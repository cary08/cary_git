#ifndef _BSP_ULTRA_H_
#define _BSP_ULTRA_H_
#include "../bsp.h"

// 障碍距离，最大值20厘米，小于此距离表示有障碍了
#define B_ULTRA_CM_MAX 200

// 一共多少个超声波
#define B_ULTRA_NUM 8

// 微秒值除以 此值 为厘米
#define B_ULTRA_CM_NUM 58

// 微秒值除以 此值 为毫米
#define B_ULTRA_MM_NUM 5.8

#define B_ULTRA_A0_ON HAL_GPIO_WritePin(ultra_out_echo2_a0_GPIO_Port, ultra_out_echo2_a0_Pin, GPIO_PIN_SET)
#define B_ULTRA_A0_OFF HAL_GPIO_WritePin(ultra_out_echo2_a0_GPIO_Port, ultra_out_echo2_a0_Pin, GPIO_PIN_RESET)

#define B_ULTRA_A1_ON HAL_GPIO_WritePin(ultra_out_echo2_a1_GPIO_Port, ultra_out_echo2_a1_Pin, GPIO_PIN_SET)
#define B_ULTRA_A1_OFF HAL_GPIO_WritePin(ultra_out_echo2_a1_GPIO_Port, ultra_out_echo2_a1_Pin, GPIO_PIN_RESET)

#define B_ULTRA_A2_ON HAL_GPIO_WritePin(ultra_out_echo2_a2_GPIO_Port, ultra_out_echo2_a2_Pin, GPIO_PIN_SET)
#define B_ULTRA_A2_OFF HAL_GPIO_WritePin(ultra_out_echo2_a2_GPIO_Port, ultra_out_echo2_a2_Pin, GPIO_PIN_RESET)

// 每个超声波的枚举
typedef enum _B_ULTRA_ENUM
{
    // 超声波 1~8 的枚举常量
    B_ULTRA_1 = 0,
    B_ULTRA_2,
    B_ULTRA_3,
    B_ULTRA_4,
    B_ULTRA_5,
    B_ULTRA_6,
    B_ULTRA_7,
    B_ULTRA_8,

    // 这个始终在最下面，用来判断总共有多少个超声波的
    B_ULTRA_SIZE,
} B_ULTRA_ENUM;

// 超声波结构体
typedef struct
{
    // 8个超声波的距离值
    int Distance1;
    int Distance2;
    int Distance3;
    int Distance4;
    int Distance5;
    int Distance6;
    int Distance7;
    int Distance8;

    // 超声波的状态
    vu8 State;

    // 当前轮询测距的是哪个超声波，对应枚举
    B_ULTRA_ENUM Num;

    // 定时器值，用来计算距离值 此值us / 5.8 = 距离mm
    int CountUs;

} B_UltraDef;

extern B_UltraDef B_UltraObj;

void bsp_UltraQueryDistance(void);
void bsp_UltraOpen(int Ultrax);
void bsp_UltraOut(int Ultrax, vu8 flag);
void bsp_UltraDistance(int Ultrax);
void bsp_UltraCallback(void);

#endif
