#include "bsp_ultra.h"

B_UltraDef B_UltraObj = {0};

//*******************************************************************
// 函  数：void bsp_UltraQueryDistance(void)
// 功  能：查看超声波 的距离
// 参  数：无
//
// 返回值：无
// 备  注：每个超声波之间间隔60ms，不然会有干扰
//*******************************************************************
void bsp_UltraQueryDistance(void)
{

    // 出错延时
    static vu32 errMs = 0;
    static vu32 errMsNum = 0;
    static vu32 errWait = 0;
    // static vu32 us = 0;

    switch (B_UltraObj.State)
    {
    case 0:
    {
        // 每个超声波对应的开关值不同
        bsp_UltraOpen(B_UltraObj.Num);

        // 每个超声波对应的发射脚不同,1为打开
        bsp_UltraOut(B_UltraObj.Num, B_TRUE);

        // 清空定时器值，并重新打开定时器
        __HAL_TIM_SetCounter(&htim5, 0);
        HAL_TIM_Base_Start(&htim5);

        // 发射时间大于10微秒
        while (__HAL_TIM_GetCounter(&htim5) < 20)
            ;

        // 关闭并清空定时器
        __HAL_TIM_SetCounter(&htim5, 0);
        HAL_TIM_Base_Stop(&htim5);

        B_UltraObj.State = 1;
        break;
    }
    case 1:
    {
        // 关闭发射脚
        bsp_UltraOut(B_UltraObj.Num, B_FALSE);

        // 进行延时错误判断
        errMs = HAL_TickGetMs();
        errMsNum = HAL_TickGetMsNum();
        errWait = 50;

        B_UltraObj.State = 2;
        break;
    }
    case 2:
    {
        // 等待中断 判断接收引脚为高电平，打开计时器，然后低电平获取定时器值

        // 如果某个超声波出现问题，那么这里就会出不去，加个判断，超时就直接查看下一个超声波
        if (HAL_TickIsTimeOut(errMsNum, errMs, errWait) == B_TRUE)
        {
            // 到这里说明，超时了，这个超声波还没检测到数据
            __HAL_TIM_SetCounter(&htim5, 0);
            HAL_TIM_Base_Start(&htim5);

            // printf("ultr_not:%d\n", B_UltraObj.Num);

            B_UltraObj.State = 4;
        }
        break;
    }
    case 3:
    {
        // 中断已经获取到计时器毫秒us值，去计算当前超声波的距离值
        bsp_UltraDistance(B_UltraObj.Num);

        __HAL_TIM_SetCounter(&htim5, 0);
        HAL_TIM_Base_Start(&htim5);

        B_UltraObj.State = 4;
        break;
    }
    case 4: // 继续检测下一个超声波
    {
        // 延时20ms，不要让上一个超声波干扰到下一个要检测的超声波
        if (__HAL_TIM_GetCounter(&htim5) > 50000)
        {
            // 延时完毕，继续检测下一个超声波
            __HAL_TIM_SetCounter(&htim5, 0);
            HAL_TIM_Base_Stop(&htim5);

            B_UltraObj.Num++;
            if (B_UltraObj.Num >= B_ULTRA_SIZE)
            {
                B_UltraObj.Num = 0;
            }

            B_UltraObj.CountUs = 0;
            B_UltraObj.State = 0;
            errMs = 0;
            errMsNum = 0;
            errWait = 0;
        }
        break;
    }
    }
}

//*******************************************************************
// 函  数：void bsp_UltraOpen(B_ULTRA_ENUM Ultrax)
// 功  能：打开要发射的超声波开关
// 参  数：Ultrax 要打开超声波几
//
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_UltraOpen(int Ultrax)
{

    switch (Ultrax)
    {
    case B_ULTRA_1:
    {
        B_ULTRA_A0_ON;
        B_ULTRA_A1_OFF;
        B_ULTRA_A2_ON;
        break;
    }
    case B_ULTRA_2:
    {
        B_ULTRA_A0_ON;
        B_ULTRA_A1_ON;
        B_ULTRA_A2_ON;
        break;
    }
    case B_ULTRA_3:
    {
        B_ULTRA_A0_OFF;
        B_ULTRA_A1_ON;
        B_ULTRA_A2_ON;
        break;
    }
    case B_ULTRA_4:
    {
        B_ULTRA_A0_OFF;
        B_ULTRA_A1_OFF;
        B_ULTRA_A2_ON;
        break;
    }
    case B_ULTRA_5:
    {
        B_ULTRA_A0_ON;
        B_ULTRA_A1_ON;
        B_ULTRA_A2_OFF;
        break;
    }
    case B_ULTRA_6:
    {
        B_ULTRA_A0_OFF;
        B_ULTRA_A1_OFF;
        B_ULTRA_A2_OFF;
        break;
    }
    case B_ULTRA_7:
    {
        B_ULTRA_A0_ON;
        B_ULTRA_A1_OFF;
        B_ULTRA_A2_OFF;
        break;
    }
    case B_ULTRA_8:
    {
        B_ULTRA_A0_OFF;
        B_ULTRA_A1_ON;
        B_ULTRA_A2_OFF;
        break;
    }
    }
}

//*******************************************************************
// 函  数：void bsp_UltraOut(int Ultrax, vu8 flag)
// 功  能：发射超声波io，置为高电平
// 参  数：Ultrax 要发射的超声波几
//
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_UltraOut(int Ultrax, vu8 flag)
{

    switch (Ultrax)
    {
    case B_ULTRA_1:
    {
        if (flag)
            HAL_GPIO_WritePin(ultra_out_trig1_GPIO_Port, ultra_out_trig1_Pin, GPIO_PIN_SET);
        else
            HAL_GPIO_WritePin(ultra_out_trig1_GPIO_Port, ultra_out_trig1_Pin, GPIO_PIN_RESET);
        break;
    }
    case B_ULTRA_2:
    {
        if (flag)
            HAL_GPIO_WritePin(ultra_out_trig2_GPIO_Port, ultra_out_trig2_Pin, GPIO_PIN_SET);
        else
            HAL_GPIO_WritePin(ultra_out_trig2_GPIO_Port, ultra_out_trig2_Pin, GPIO_PIN_RESET);
        break;
    }
    case B_ULTRA_3:
    {
        if (flag)
            HAL_GPIO_WritePin(ultra_out_trig3_GPIO_Port, ultra_out_trig3_Pin, GPIO_PIN_SET);
        else
            HAL_GPIO_WritePin(ultra_out_trig3_GPIO_Port, ultra_out_trig3_Pin, GPIO_PIN_RESET);
        break;
    }
    case B_ULTRA_4:
    {
        if (flag)
            HAL_GPIO_WritePin(ultra_out_trig4_GPIO_Port, ultra_out_trig4_Pin, GPIO_PIN_SET);
        else
            HAL_GPIO_WritePin(ultra_out_trig4_GPIO_Port, ultra_out_trig4_Pin, GPIO_PIN_RESET);
        break;
    }
    case B_ULTRA_5:
    {
        if (flag)
            HAL_GPIO_WritePin(ultra_out_trig5_GPIO_Port, ultra_out_trig5_Pin, GPIO_PIN_SET);
        else
            HAL_GPIO_WritePin(ultra_out_trig5_GPIO_Port, ultra_out_trig5_Pin, GPIO_PIN_RESET);
        break;
    }
    case B_ULTRA_6:
    {
        if (flag)
            HAL_GPIO_WritePin(ultra_out_trig6_GPIO_Port, ultra_out_trig6_Pin, GPIO_PIN_SET);
        else
            HAL_GPIO_WritePin(ultra_out_trig6_GPIO_Port, ultra_out_trig6_Pin, GPIO_PIN_RESET);
        break;
    }
    case B_ULTRA_7:
    {
        if (flag)
            HAL_GPIO_WritePin(ultra_out_trig7_GPIO_Port, ultra_out_trig7_Pin, GPIO_PIN_SET);
        else
            HAL_GPIO_WritePin(ultra_out_trig7_GPIO_Port, ultra_out_trig7_Pin, GPIO_PIN_RESET);
        break;
    }
    case B_ULTRA_8:
    {
        if (flag)
            HAL_GPIO_WritePin(ultra_out_trig8_GPIO_Port, ultra_out_trig8_Pin, GPIO_PIN_SET);
        else
            HAL_GPIO_WritePin(ultra_out_trig8_GPIO_Port, ultra_out_trig8_Pin, GPIO_PIN_RESET);
        break;
    }
    }
}

//*******************************************************************
// 函  数：void bsp_UltraDistance(int Ultrax)
// 功  能：计算出超声波的距离
// 参  数：Ultrax 要计算的超声波几
//
// 返回值：无
// 备  注：距离值单位：B_ULTRA_MM_NUM 单位毫米  B_ULTRA_MM_NUM 单位厘米
//*******************************************************************
void bsp_UltraDistance(int Ultrax)
{

    switch (Ultrax)
    {
    case B_ULTRA_1:
    {
        B_UltraObj.Distance1 = (int)(B_UltraObj.CountUs / B_ULTRA_MM_NUM);
        break;
    }
    case B_ULTRA_2:
    {
        B_UltraObj.Distance2 = (int)(B_UltraObj.CountUs / B_ULTRA_MM_NUM);
        break;
    }
    case B_ULTRA_3:
    {
        B_UltraObj.Distance3 = (int)(B_UltraObj.CountUs / B_ULTRA_MM_NUM);
        break;
    }
    case B_ULTRA_4:
    {
        B_UltraObj.Distance4 = (int)(B_UltraObj.CountUs / B_ULTRA_MM_NUM);
        break;
    }
    case B_ULTRA_5:
    {
        B_UltraObj.Distance5 = (int)(B_UltraObj.CountUs / B_ULTRA_MM_NUM);
        break;
    }
    case B_ULTRA_6:
    {
        B_UltraObj.Distance6 = (int)(B_UltraObj.CountUs / B_ULTRA_MM_NUM);
        break;
    }
    case B_ULTRA_7:
    {
        B_UltraObj.Distance7 = (int)(B_UltraObj.CountUs / B_ULTRA_MM_NUM);
        break;
    }
    case B_ULTRA_8:
    {
        B_UltraObj.Distance8 = (int)(B_UltraObj.CountUs / B_ULTRA_MM_NUM);
        break;
    }
    }
    printf("ult1:%d,_2_:%d,_3_:%d,_4_:%d,_5_:%d,_6_:%d,_7_:%d,_8_:%d,\n",
           (int)(B_UltraObj.Distance1), (int)(B_UltraObj.Distance2), (int)(B_UltraObj.Distance3), (int)(B_UltraObj.Distance4),
           (int)(B_UltraObj.Distance5), (int)(B_UltraObj.Distance6), (int)(B_UltraObj.Distance7), (int)(B_UltraObj.Distance8));
}

//*******************************************************************
// 函  数：void bsp_UltraCallback(void)
// 功  能：中断的回调函数的自定义的超声波的回调函数
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_UltraCallback(void)
{
    if (HAL_GPIO_ReadPin(ultra_read_GPIO_Port, ultra_read_Pin) == GPIO_PIN_SET)
    {
        // 高电平开启定时器
        HAL_TIM_Base_Start(&htim5);
    }
    else if (HAL_GPIO_ReadPin(ultra_read_GPIO_Port, ultra_read_Pin) == GPIO_PIN_RESET)
    {
        // 低电平拿出定时器值，并进入下一流程状态
        B_UltraObj.CountUs = __HAL_TIM_GetCounter(&htim5);
        B_UltraObj.State = 3;
    }
}
