#include "bsp_encode.h"

//*******************************************************************
// ��  ����short bsp_EncoderGet(vu8 Tim)
// ��  �ܣ���ȡ���ӵı�����ֵ
// ��  ����Tim ���ֱ������������ֱ����� �� ��
//
// ����ֵ����/�ұ�������ֵ
// ��  ע����
//*******************************************************************
short bsp_EncoderGet(vu8 Tim)
{
    switch (Tim)
    {
    case B_ENCODER_RIGHT:
    {
        return __HAL_TIM_GET_COUNTER(&htim2);
        break;
    }
    case B_ENCODER_LEFT:
    {
        return __HAL_TIM_GET_COUNTER(&htim3);
        break;
    }
    }
    return B_NULL;
}

//*******************************************************************
// ��  ����void bsp_EncoderSet(vu8 Tim, short Val)
// ��  �ܣ����ñ�������ֵ��һ������д���㣬��ձ�����
// ��  ����Tim ���ֱ������������ֱ����� �� ��
//
// ����ֵ����
// ��  ע����
//*******************************************************************
void bsp_EncoderSet(vu8 Tim, short Val)
{
    switch (Tim)
    {
    case B_ENCODER_RIGHT:
    {
        __HAL_TIM_SET_COUNTER(&htim2, Val);
        break;
    }

    case B_ENCODER_LEFT:
    {
        __HAL_TIM_SET_COUNTER(&htim3, Val);
        break;
    }
    }
}
