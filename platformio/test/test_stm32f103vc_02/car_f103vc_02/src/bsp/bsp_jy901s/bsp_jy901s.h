#ifndef _BSP_JY901S_H_
#define _BSP_JY901S_H_
#include "../bsp.h"

// 加速度单位，m/s2 ，地球默认加速度为 9.8m/s2
#define B_GRAVITATIONAL_GM_S2 9.8

// 模块的重力加速度 设置的值
#define B_GRAVITATIONAL_ACCEL 16.0

// 加速度计 和 角速度 和 角度 和 四元数 的精度范围
#define B_ACCURACY_RANGE 32768.0

// 角速度范围 设置的值
#define B_ANGULAR_VELOCITY_RANGE 2000.0

// 角度范围  度
#define B_ANGULAR_RANGE 180.0

// 温度精度 获取到的温度值 要 除以 这个精度值，才是真正的温度
#define B_TEMPERATURE_ACCURACY 100

// 设置回传命令的值
#define B_JY901S_RETURN_L_VAL 0X1E
#define B_JY901S_RETURN_H_VAL 0X02 // 将第二位置一,开启回传四元数

// 注释在pdf中找对应的详细解释 E:\companyMaterials\JunYiShiJue\台球桌项目相关\硬件驱动函数\硬件资料\jy901s\维特智能文档 - 维特智能.pdf
typedef enum
{
    B_JY901S_RECV_TIME_CMD = 0X50,                // 时间输出
    B_JY901S_RECV_ACCELERATION_CMD = 0X51,        // 加速度输出
    B_JY901S_RECV_ANGULAR_VELOCITY_CMD = 0X52,    // 角速度输出
    B_JY901S_RECV_ANGLE_CMD = 0X53,               // 角度输出
    B_JY901S_RECV_MAGNETIC_FIELD_CMD = 0X54,      // 磁场输出
    B_JY901S_RECV_PORT_STATUS_CMD = 0X55,         // 端口状态数据输出
    B_JY901S_RECV_PRESSURE_CMD = 0X56,            // 气压、高度输出
    B_JY901S_RECV_LONGITUDE_DIMENSION_CMD = 0X57, // 经纬度输出
    B_JY901S_RECV_GPS_CMD = 0X58,                 // 地速，GPS输出
    B_JY901S_RECV_QUATERNION_CMD = 0X59,          // 四元数输出
    B_JY901S_RECV_SATELLITE_CMD = 0X5A,           // 卫星定位精度输出

} B_JY901S_RECV_CMD_ENUM;

// 每个命令对应的长度
typedef enum
{
    B_JY901S_RECV_TIME_SIZE = 11,                // 时间输出命令的帧长度
    B_JY901S_RECV_ACCELERATION_SIZE = 11,        // 加速度输出命令的帧长度
    B_JY901S_RECV_ANGULAR_VELOCITY_SIZE = 11,    // 角速度输出命令的帧长度
    B_JY901S_RECV_ANGLE_SIZE = 11,               // 角度输出命令的帧长度
    B_JY901S_RECV_MAGNETIC_FIELD_SIZE = 11,      // 磁场输出命令的帧长度
    B_JY901S_RECV_PORT_STATUS_SIZE = 11,         // 端口状态数据输出命令的帧长度
    B_JY901S_RECV_PRESSURE_SIZE = 11,            // 气压、高度输出命令的帧长度
    B_JY901S_RECV_LONGITUDE_DIMENSION_SIZE = 11, // 经纬度输出命令的帧长度
    B_JY901S_RECV_GPS_SIZE = 6,                  // 地速，GPS输出命令的帧长度
    B_JY901S_RECV_QUATERNION_SIZE = 11,          // 四元数输出命令的帧长度
    B_JY901S_RECV_SATELLITE_SIZE = 11,           // 卫星定位精度输出命令的帧长度

} B_JY901S_RECV_SIZE_ENUM;

// 设置配置 对应的命令
typedef enum
{
    // 长度1; data为 0退出校准 1加速度计校准 3高度置零 4z轴角度归零(只在六轴下适用) 7磁场校准
    B_JY901S_SET_CALIBRATION_CMD = 0X01, // 设置校准的命令

    // 长度1; data为 0水平安装 1垂直安装
    B_JY901S_SET_DIRECTION_CMD = 0X23, // 设置安装方向的命令

    // 长度1; 值固定为1,发一次改变当前状态
    B_JY901S_SET_DORMANCY_CMD = 0X22, // 设置休眠与解休眠的命令

    // 长度1; data为 0设置9轴算法 1设置6轴算法
    B_JY901S_SET_SIX_NINE_CMD = 0X24, // 设置六轴和九轴的算法转换的命令

    // 长度1; data为 0开启陀螺仪自动校准 1关闭陀螺仪自动校准
    B_JY901S_SET_GYRO_CAL_CMD = 0X63, // 设置陀螺仪自动校准的命令

    // 长度2; data为 第一个data为低位,第二个为高位, 每个位代表1开启或0关闭一个回传命令码,命令码在上边的枚举有定义
    // 低位:0-7位,0x50 0x51 0x52 0x53 0x54 0x55 0x56 0x57
    // 高位:0-7位,0x58 0x59 0x5A xx xx xx xx xx 高位只有三个位起作用
    B_JY901S_SET_RETURN_CMD = 0X02, // 设置回传的命令

    // 长度1; data为 0x01为0.1Hz. 0x02为0.5Hz. 0x03为1Hz. 0x04为2Hz. 0x05为5Hz. 0x06为10Hz(默认). 0x07为20Hz.
    // 0x08为50Hz. 0x09为100Hz. 0x0A为125Hz. 0x0B为200Hz. 0x0C为单次输出
    B_JY901S_SET_RETURN_RATE_CMD = 0X03, // 设置回传速率的命令

    // 长度1; data为 0x00为保留. 0x01 4800. 0x02 9600. 0x03 19200. 0x04 38400. 0x05 57600. 0x06 115200.
    // 0x07 230400. 0x08 460800. 0x09 921600（默认.手册应该写错了,默认应该是9600）
    B_JY901S_SET_BAUTRATE_CMD = 0X04, // 设置串口波特率的命令

    // 长度2; data为先低位后高位  设置加速度零偏以后，加速度的输出值为传感器测量值减去零偏值。
    B_JY901S_SET_ACCEL_OFFSET_X_CMD = 0X05,   // 设置加速度x轴零偏值的命令
    B_JY901S_SET_ACCEL_OFFSET_Y_CMD = 0X06,   // 设置加速度y轴零偏值的命令
    B_JY901S_SET_ACCEL_OFFSET_Z_CMD = 0X07,   // 设置加速度z轴零偏值的命令
    B_JY901S_SET_ANG_VEL_OFFSET_X_CMD = 0X08, // 设置角速度x轴零偏值的命令
    B_JY901S_SET_ANG_VEL_OFFSET_Y_CMD = 0X09, // 设置角速度y轴零偏值的命令
    B_JY901S_SET_ANG_VEL_OFFSET_Z_CMD = 0X0A, // 设置角速度z轴零偏值的命令
    B_JY901S_SET_MAG_OFFSET_X_CMD = 0X0B,     // 设置磁场x轴零偏值的命令
    B_JY901S_SET_MAG_OFFSET_Y_CMD = 0X0C,     // 设置磁场y轴零偏值的命令
    B_JY901S_SET_MAG_OFFSET_Z_CMD = 0X0D,     // 设置磁场z轴零偏值的命令

    // 长度1; data为 0x00：模拟输入（默认）. 0x01：数字输入. 0x02：输出数字高电平. 0x03：输出数字低电平. 0x04：输出PWM
    B_JY901S_SET_PORT_D0_CMD = 0X0E, // 设置端口D0模式的命令
    B_JY901S_SET_PORT_D1_CMD = 0X0F, // 设置端口D1模式的命令   多一个 0x05：CLR相对姿态 也不知道啥意思,没有解释
    B_JY901S_SET_PORT_D2_CMD = 0X10, // 设置端口D2模式的命令
    B_JY901S_SET_PORT_D3_CMD = 0X11, // 设置端口D3模式的命令

    // 长度2; data先低后高.
    // PWM的高电平宽度和周期都以us为单位，例如高电平宽度1500us，周期20000us的舵机控制信号,只需要将D0PWMH设置为1500，D0PWMT设置为20000即可。
    B_JY901S_SET_PORT_D0_PWM_H_CMD = 0X12, // 设置端口D0PWM高电平宽度的命令
    B_JY901S_SET_PORT_D1_PWM_H_CMD = 0X13, // 设置端口D1PWM高电平宽度的命令
    B_JY901S_SET_PORT_D2_PWM_H_CMD = 0X14, // 设置端口D2PWM高电平宽度的命令
    B_JY901S_SET_PORT_D3_PWM_H_CMD = 0X15, // 设置端口D3PWM高电平宽度的命令
    B_JY901S_SET_PORT_D0_PWM_C_CMD = 0X16, // 设置端口D0PWM周期的命令
    B_JY901S_SET_PORT_D1_PWM_C_CMD = 0X17, // 设置端口D1PWM周期的命令
    B_JY901S_SET_PORT_D2_PWM_C_CMD = 0X18, // 设置端口D2PWM周期的命令
    B_JY901S_SET_PORT_D3_PWM_C_CMD = 0X19, // 设置端口D3PWM周期的命令

    // 长度1; data为 默认是0x50。IIC地址采用7bit地址，最大不能超过0x7f 保存,重新上电后生效
    B_JY901S_SET_IIC_ADDR_CMD = 0X1A, // 设置IIC地址的命令

    // 长度1; data为 0x01：关闭LED指示灯   0x00：开启LED指示灯
    B_JY901S_SET_LED_CMD = 0X1B, // 设置LED指示灯的命令

    // 长度1; data为 0x00：2400  0x01：4800  0x02：9600（默认）    0x03：19200  0x04：38400
    // 0x05：57600  0x06：115200  0x07：230400  0x08：460800  0x09：921600
    B_JY901S_SET_GPS_RATE_CMD = 0X1C, // 设置GPS通信速率的命令

    // 长度2; data为先低后高
    // 设置模块报警,这个就是相当于水平仪,当小车发生倾斜了,就会被检测到
    // FF AA 5B 1C 07 表示x轴角度最大值10度, 071C 转为十进制 1820, 1820*180/32768 = 9.997 为啥还要这样呢,直接传入度数值不行吗?
    B_JY901S_SET_ALERT_ANG_MIN_X_CMD = 0X5A, // 设置X轴角度最小值的命令
    B_JY901S_SET_ALERT_ANG_MAX_X_CMD = 0X5B, // 设置X轴角度最大值的命令
    B_JY901S_SET_ALERT_ANG_MIN_Y_CMD = 0X5E, // 设置Y轴角度最小值的命令
    B_JY901S_SET_ALERT_ANG_MAX_Y_CMD = 0X5F, // 设置Y轴角度最大值的命令

    // 长度2; data为先低后高
    // 确认时间一般为0,一旦超过设置角度值,那么就触发报警信号,但是,还要在持续时间内一直都是在触发才行
    B_JY901S_SET_ALERT_CONFIRM_MS_CMD = 0X68, // 设置报警的确认时间的命令
                                              // 比如FF AA 59 64 00设置的保持时间是100ms
    B_JY901S_SET_ALERT_TIMEOUT_MS_CMD = 0X59, // 设置报警的超时时间的命令
                                              // 比如FF AA 62 00 00设置的报警电平是0
    B_JY901S_SET_ALERT_LEVEL_CMD = 0X62,      // 设置报警的电平的命令

} B_JY901S_SET_CMD_ENUM;

typedef struct B_JY901SStr
{

    // 接收数据缓存区,将读取到的数据，先全部放入此buff中
    vu8 buff[100];
    vu32 buffSize;

    // 时间输出
    vu8 DateYY;  // 20YY年
    vu8 DateMM;  // 月
    vu8 DateDD;  // 日
    vu8 TimeHH;  // 时
    vu8 TimeMM;  // 分
    vu8 TimeSS;  // 秒
    vu16 TimeMS; // 毫秒

    // 温度，t/100 = 多少°C
    float Temperature;

    // 加速度输出,ax/32768*16g(g为重力加速度，可取9.8m/s2)
    float AccelAX;
    float AccelAy;
    float AccelAz;

    // 角速度，公式是 欧米噶，像W所以就用W字母代替。Wx/32768*2000°/s
    float AngVelWx;
    float AngVelWy;
    float AngVelWz;

    // 角度，直接获取角度值
    float Roll;  // 翻滚角 x轴
    float Pitch; // 俯仰角 y轴
    float Yaw;   // 偏航角 z轴
    // 版本，模块的版本吗？应该是吧。
    vu32 Version;

    // 磁场，单位正电磁荷在磁场中所受的力被称为磁场强度H，这个H代表了磁场强度
    short MagneticHX;
    short MagneticHY;
    short MagneticHZ;

    // 四元数
    float QuaternionicQ0;
    float QuaternionicQ1;
    float QuaternionicQ2;
    float QuaternionicQ3;

    // 加速度原始值
    short AccelOriX;
    short AccelOriY;
    short AccelOriZ;

    // 角速度原始值
    short GyroOriX;
    short GyroOriY;
    short GyroOriZ;

} B_JY901SDef;

extern B_JY901SDef B_JY901SObj;

void bsp_JY901SRecv(void);
void bsp_JY901SRecvControl(vu8 *Data);

void bsp_JY901SHandleTIME(vu8 *HandleCmd);
void bsp_JY901SHandleACCELERATION(vu8 *HandleCmd);
void bsp_JY901SHandleANGULAR_VELOCITY(vu8 *HandleCmd);
void bsp_JY901SHandleANGLE(vu8 *HandleCmd);
void bsp_JY901SHandleMAGNETIC_FIELD(vu8 *HandleCmd);
void bsp_JY901SHandlePORT_STATUS(vu8 *HandleCmd);
void bsp_JY901SHandlePRESSURE(vu8 *HandleCmd);
void bsp_JY901SHandleLONGITUDE_DIMENSION(vu8 *HandleCmd);
void bsp_JY901SHandleGPS(vu8 *HandleCmd);
void bsp_JY901SHandleQUATERNION(vu8 *HandleCmd);
void bsp_JY901SHandleSATELLITE(vu8 *HandleCmd);

void bsp_JY901SSendUnlock(void);
void bsp_JY901SSendSave(vu8 data);
void bsp_JY901SSendSetConfiguration(vu8 cmd, vu8 *data, vu32 dataSize);

vu8 bsp_JY901SRecvCheckCmd(B_JY901S_RECV_CMD_ENUM cmd, int *size);
vu8 bsp_JY901SRecvCheckSum(vu8 *cmd, vu32 cmdSize);

#endif
