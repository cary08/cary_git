#include "bsp_jy901s.h"

B_JY901SDef B_JY901SObj = {0};

//*******************************************************************
// 函  数：void bsp_JY901SRecv(void)
// 功  能：接收jy901s 模块发来的数据
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SRecv(void)
{

    // 一帧数据接收完毕，一帧数据包含所有一组十条协议数据，50 ~ 5A ，（如果模块没开启哪一条数据协议，就没有那一条）
    if (HAL_Uart.uart_jy901s_idle_flag == B_TRUE)
    {
        // 将这一帧的各条数据解析出来
        int index = 0;
        int state = 0;
        int size = 0;
        for (index = 0; index < HAL_Uart.uart_jy901s_buff_len; index++)
        {

            switch (state)
            {
            case 0: // 处理协议的第一位，帧头
            {
                if (HAL_Uart.uart_jy901s_buff[index] == 0x55)
                {
                    B_JY901SObj.buff[B_JY901SObj.buffSize] = 0x55;
                    B_JY901SObj.buffSize++;
                    // printf("i0:%02X ", HAL_Uart.uart_jy901s_buff[index]);
                    state = 1;
                    break;
                }
                // printf("i1ERR\n");
                // 如果第一个不是帧头，不做处理，继续判断下一个
                break;
            }
            case 1: // 处理协议的第二位，命令码
            {
                // 校验命令码 并 获取协议总长度
                if (bsp_JY901SRecvCheckCmd(HAL_Uart.uart_jy901s_buff[index], &size) == B_TRUE)
                {
                    B_JY901SObj.buff[B_JY901SObj.buffSize] = HAL_Uart.uart_jy901s_buff[index];
                    B_JY901SObj.buffSize++;
                    state = 2;
                    break;
                }
                else
                {
                    // 错误,去置为初始态，重新从头校验
                    // 初始化变量 校验下一条数据
                    memset((uint8_t *)B_JY901SObj.buff, 0, sizeof(B_JY901SObj.buff));
                    B_JY901SObj.buffSize = 0;
                    size = 0;
                    state = 0;
                    break;
                }
                break;
            }
            case 2: // 处理协议的第三位 至 数据位最后一位，数据位，由每个宏定义的数据长度控制
            {
                // 最后一位是校验位
                if (B_JY901SObj.buffSize < (size - 1))
                {
                    B_JY901SObj.buff[B_JY901SObj.buffSize] = HAL_Uart.uart_jy901s_buff[index];
                    B_JY901SObj.buffSize++;

                    if (B_JY901SObj.buffSize >= (size - 1))
                    {
                        // 数据位赋值完毕
                        state = 3;
                        break;
                    }
                }
                break;
            }
            case 3:
            {
                // 校验和
                // 先把校验和放入数组中，校验函数会用到进行对比
                B_JY901SObj.buff[B_JY901SObj.buffSize] = HAL_Uart.uart_jy901s_buff[index];

                // 数组中已经将校验和的值存入了，开始校验
                if (bsp_JY901SRecvCheckSum(B_JY901SObj.buff, size) == B_TRUE)
                {
                    // 到这里说明已经拿到一个完整的协议数据了，进行处理
                    bsp_JY901SRecvControl(B_JY901SObj.buff);
                }

                // 初始化变量 校验下一条数据
                memset((uint8_t *)B_JY901SObj.buff, 0, sizeof(B_JY901SObj.buff));
                B_JY901SObj.buffSize = 0;
                size = 0;
                state = 0;
                break;
            }
            }
        }
        // 读取完数据，然后将其清空，等待下一次数据的到来
        memset(HAL_Uart.uart_jy901s_buff, 0, sizeof(HAL_Uart.uart_jy901s_buff));
        HAL_Uart.uart_jy901s_buff_len = B_NULL;
        HAL_Uart.uart_jy901s_idle_flag = B_NULL;
        // printf("read_end....\n");
    }
}

//*******************************************************************
// 函  数：void bsp_JY901SRecvControl(vu8 *Data)
// 功  能：处理接收到jy901s发来的数据
// 参  数：Data 协议数据
//
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SRecvControl(vu8 *Data)
{
    switch (Data[1])
    {
    case B_JY901S_RECV_TIME_CMD: // 0X50     时间输出
    {
        bsp_JY901SHandleTIME(Data);
        break;
    }

    case B_JY901S_RECV_ACCELERATION_CMD: // 0X51     加速度输出
    {
        bsp_JY901SHandleACCELERATION(Data);
        break;
    }

    case B_JY901S_RECV_ANGULAR_VELOCITY_CMD: // 0X52     角速度输出
    {
        bsp_JY901SHandleANGULAR_VELOCITY(Data);
        break;
    }

    case B_JY901S_RECV_ANGLE_CMD: // 0X53     角度输出
    {
        bsp_JY901SHandleANGLE(Data);
        break;
    }

    case B_JY901S_RECV_MAGNETIC_FIELD_CMD: // 0X54     磁场输出
    {
        bsp_JY901SHandleMAGNETIC_FIELD(Data);
        break;
    }

    case B_JY901S_RECV_PORT_STATUS_CMD: // 0X55     端口状态数据输出
    {
        bsp_JY901SHandlePORT_STATUS(Data);
        break;
    }

    case B_JY901S_RECV_PRESSURE_CMD: // 0X56     气压、高度输出
    {
        bsp_JY901SHandlePRESSURE(Data);
        break;
    }

    case B_JY901S_RECV_LONGITUDE_DIMENSION_CMD: // 0X57     经纬度输出
    {
        bsp_JY901SHandleLONGITUDE_DIMENSION(Data);
        break;
    }

    case B_JY901S_RECV_GPS_CMD: // 0X58     地速，GPS输出
    {
        bsp_JY901SHandleGPS(Data);
        break;
    }

    case B_JY901S_RECV_QUATERNION_CMD: // 0X59     四元数输出
    {
        bsp_JY901SHandleQUATERNION(Data);
        break;
    }

    case B_JY901S_RECV_SATELLITE_CMD: // 0X5A     卫星定位精度输出
    {
        bsp_JY901SHandleSATELLITE(Data);
        break;
    }
    }
}

//*******************************************************************
// 函  数：void bsp_JY901SHandleTIME(vu8 *HandleCmd,vu32 HandleSize)
// 功  能：处理接收到jy901s发来的 时间 协议
// 参  数：HandleCmd 完整的协议数据
//         HandleSize 协议长度
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SHandleTIME(vu8 *HandleCmd)
{
}

//*******************************************************************
// 函  数：void bsp_JY901SHandleACCELERATION(vu8 *HandleCmd,vu32 HandleSize)
// 功  能：处理接收到jy901s发来的 加速度 协议
// 参  数：HandleCmd 完整的协议数据
//         HandleSize 协议长度
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SHandleACCELERATION(vu8 *HandleCmd)
{

    // 加速度计 原始值
    B_JY901SObj.AccelOriX = ((short)HandleCmd[3] << 8) | HandleCmd[2];
    B_JY901SObj.AccelOriY = ((short)HandleCmd[5] << 8) | HandleCmd[4];
    B_JY901SObj.AccelOriZ = ((short)HandleCmd[7] << 8) | HandleCmd[6];

    // 将数据转换，并放入结构体中
    B_JY901SObj.AccelAX = (((short)HandleCmd[3] << 8) | HandleCmd[2]) / B_ACCURACY_RANGE * B_GRAVITATIONAL_ACCEL;
    B_JY901SObj.AccelAy = (((short)HandleCmd[5] << 8) | HandleCmd[4]) / B_ACCURACY_RANGE * B_GRAVITATIONAL_ACCEL;
    B_JY901SObj.AccelAz = (((short)HandleCmd[7] << 8) | HandleCmd[6]) / B_ACCURACY_RANGE * B_GRAVITATIONAL_ACCEL;

    // 温度
    B_JY901SObj.Temperature = (((short)HandleCmd[9] << 8) | HandleCmd[8]) / B_TEMPERATURE_ACCURACY;
}

//*******************************************************************
// 函  数：void bsp_JY901SHandleANGULAR_VELOCITY(vu8 *HandleCmd,vu32 HandleSize)
// 功  能：处理接收到jy901s发来的 角速度 协议
// 参  数：HandleCmd 完整的协议数据
//         HandleSize 协议长度
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SHandleANGULAR_VELOCITY(vu8 *HandleCmd)
{

    // 陀螺仪 原始值
    B_JY901SObj.GyroOriX = ((short)HandleCmd[3] << 8) | HandleCmd[2];
    B_JY901SObj.GyroOriY = ((short)HandleCmd[5] << 8) | HandleCmd[4];
    B_JY901SObj.GyroOriZ = ((short)HandleCmd[7] << 8) | HandleCmd[6];

    B_JY901SObj.AngVelWx = (((short)HandleCmd[3] << 8) | HandleCmd[2]) / B_ACCURACY_RANGE * B_ANGULAR_VELOCITY_RANGE;
    B_JY901SObj.AngVelWy = (((short)HandleCmd[5] << 8) | HandleCmd[4]) / B_ACCURACY_RANGE * B_ANGULAR_VELOCITY_RANGE;
    B_JY901SObj.AngVelWz = (((short)HandleCmd[7] << 8) | HandleCmd[6]) / B_ACCURACY_RANGE * B_ANGULAR_VELOCITY_RANGE;

    // printf("GyroX:%hd,GyroY:%hd,GyroZ:%hd \n", B_JY901SObj.GyroOriX, B_JY901SObj.GyroOriY, B_JY901SObj.GyroOriZ);

    // 温度
    B_JY901SObj.Temperature = (((short)HandleCmd[9] << 8) | HandleCmd[8]) / B_TEMPERATURE_ACCURACY;
}

//*******************************************************************
// 函  数：void bsp_JY901SHandleANGLE(vu8 *HandleCmd,vu32 HandleSize)
// 功  能：处理接收到jy901s发来的 角度 协议
// 参  数：HandleCmd 完整的协议数据
//         HandleSize 协议长度
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SHandleANGLE(vu8 *HandleCmd)
{
    B_JY901SObj.Roll = (((short)HandleCmd[3] << 8) | HandleCmd[2]) / B_ACCURACY_RANGE * B_ANGULAR_RANGE;
    B_JY901SObj.Pitch = (((short)HandleCmd[5] << 8) | HandleCmd[4]) / B_ACCURACY_RANGE * B_ANGULAR_RANGE;
    B_JY901SObj.Yaw = (((short)HandleCmd[7] << 8) | HandleCmd[6]) / B_ACCURACY_RANGE * B_ANGULAR_RANGE;

    // printf("Oritestyaw:%X,%X,%hd,%.2f \n", HandleCmd[7],HandleCmd[6],(((short)HandleCmd[7]<<8)|HandleCmd[6]),
    // (((short)HandleCmd[7]<<8)|HandleCmd[6])/B_ACCURACY_RANGE*B_ANGULAR_RANGE);

    // printf("Roll:%.2f,Pitch:%.2f,Yaw:%.2f\n", B_JY901SObj.Roll, B_JY901SObj.Pitch, B_JY901SObj.Yaw);

    // 版本
    B_JY901SObj.Version = (HandleCmd[9] << 8) | HandleCmd[8];
}

//*******************************************************************
// 函  数：void bsp_JY901SHandleMAGNETIC_FIELD(vu8 *HandleCmd,vu32 HandleSize)
// 功  能：处理接收到jy901s发来的 磁场 协议
// 参  数：HandleCmd 完整的协议数据
//         HandleSize 协议长度
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SHandleMAGNETIC_FIELD(vu8 *HandleCmd)
{
    B_JY901SObj.MagneticHX = ((short)HandleCmd[3] << 8) | HandleCmd[2];
    B_JY901SObj.MagneticHY = ((short)HandleCmd[5] << 8) | HandleCmd[4];
    B_JY901SObj.MagneticHZ = ((short)HandleCmd[7] << 8) | HandleCmd[6];

    // 温度
    B_JY901SObj.Temperature = (((short)HandleCmd[9] << 8) | HandleCmd[8]) / B_TEMPERATURE_ACCURACY;
}

//*******************************************************************
// 函  数：void bsp_JY901SHandlePORT_STATUS(vu8 *HandleCmd,vu32 HandleSize)
// 功  能：处理接收到jy901s发来的 端口状态 协议
// 参  数：HandleCmd 完整的协议数据
//         HandleSize 协议长度
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SHandlePORT_STATUS(vu8 *HandleCmd)
{
}

//*******************************************************************
// 函  数：void bsp_JY901SHandlePRESSURE(vu8 *HandleCmd,vu32 HandleSize)
// 功  能：处理接收到jy901s发来的 气压、高度 协议
// 参  数：HandleCmd 完整的协议数据
//         HandleSize 协议长度
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SHandlePRESSURE(vu8 *HandleCmd)
{
}

//*******************************************************************
// 函  数：void bsp_JY901SHandleLONGITUDE_DIMENSION(vu8 *HandleCmd,vu32 HandleSize)
// 功  能：处理接收到jy901s发来的 经纬度 协议
// 参  数：HandleCmd 完整的协议数据
//         HandleSize 协议长度
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SHandleLONGITUDE_DIMENSION(vu8 *HandleCmd)
{
}

//*******************************************************************
// 函  数：void bsp_JY901SHandleGPS(vu8 *HandleCmd,vu32 HandleSize)
// 功  能：处理接收到jy901s发来的 地速GPS 协议
// 参  数：HandleCmd 完整的协议数据
//         HandleSize 协议长度
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SHandleGPS(vu8 *HandleCmd)
{
}

//*******************************************************************
// 函  数：void bsp_JY901SHandleQUATERNION(vu32 *HandleCmd,vu32 HandleSize)
// 功  能：处理接收到jy901s发来的 四元数 协议
// 参  数：HandleCmd 完整的协议数据
//         HandleSize 协议长度
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SHandleQUATERNION(vu8 *HandleCmd)
{

    B_JY901SObj.QuaternionicQ0 = (((short)HandleCmd[3] << 8) | HandleCmd[2]) / B_ACCURACY_RANGE;
    B_JY901SObj.QuaternionicQ1 = (((short)HandleCmd[5] << 8) | HandleCmd[4]) / B_ACCURACY_RANGE;
    B_JY901SObj.QuaternionicQ2 = (((short)HandleCmd[7] << 8) | HandleCmd[6]) / B_ACCURACY_RANGE;
    B_JY901SObj.QuaternionicQ3 = (((short)HandleCmd[9] << 8) | HandleCmd[8]) / B_ACCURACY_RANGE;
}

//*******************************************************************
// 函  数：void bsp_JY901SHandleSATELLITE(vu8 *HandleCmd,vu32 HandleSize)
// 功  能：处理接收到jy901s发来的 卫星定位精度 协议
// 参  数：HandleCmd 完整的协议数据
//         HandleSize 协议长度
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SHandleSATELLITE(vu8 *HandleCmd)
{
}

// ****************************** 设置。 控制板 向 jy901s 模块 发送 ****************************

//*******************************************************************
// 函  数：void bsp_JY901SSendUnlock(void)
// 功  能：解锁指令。对模块进行指令控制时，必须先发送解锁指令，然后隔10ms再发送其它配置指令。
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SSendUnlock(void)
{
    vu8 unlock[5] = {0XFF, 0XAA, 0X69, 0X88, 0XB5};
    HAL_UART_Transmit(&huart3, (uint8_t *)unlock, sizeof(unlock), 1000);
}

//*******************************************************************
// 函  数：void bsp_JY901SSendSave(vu8 data)
// 功  能：保存指令。对模块配置完命令时，还要发送保存命令，不然会断电丢失。
// 参  数：data 0保存当前配置，1恢复默认（出厂）配置并保存
//
// 返回值：无
// 备  注：发送完此指令，要等待100ms再进行其它操作，模块内部保存需要一定时间。
//*******************************************************************
void bsp_JY901SSendSave(vu8 data)
{
    vu8 save[5] = {0XFF, 0XAA, 0X00, data, 0X00};
    HAL_UART_Transmit(&huart3, (uint8_t *)save, sizeof(save), 1000);
}

//*******************************************************************
// 函  数：void bsp_JY901SSendSetConfiguration(vu8 cmd,vu8 *data, vu32 dataSize)
// 功  能：设置配置指令。
// 参  数：cmd 每个配置对应的命令
//        data 每个命令对应的数据
//        dataSize 数据对应的长度，每个命令对应的数据长度是不一样的
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_JY901SSendSetConfiguration(vu8 cmd, vu8 *data, vu32 dataSize)
{
    vu8 config[5] = {0XFF, 0XAA, cmd, 0X00, 0X00};

    // 数据 有两个，默认为0，来几个就用传入的数据替换默认的0值
    int i = 0;
    for (i = 0; i < dataSize; i++)
    {
        config[3 + i] = data[i];
    }
    HAL_UART_Transmit(&huart3, (uint8_t *)config, sizeof(config), 1000);
}

//*******************************************************************
// 函  数：vu8 bsp_JY901SRecvCheckCmd(B_JY901S_RECV_CMD_ENUM cmd, vu32 *size)
// 功  能：接收jy901s 校验查看接收到的命令码是否正确
// 参  数：cmd  校验的命令码
//         *size  对应命令码的总长度，包括帧头和校验和
// 返回值：无
// 备  注：无
//*******************************************************************
vu8 bsp_JY901SRecvCheckCmd(B_JY901S_RECV_CMD_ENUM cmd, int *size)
{

    switch (cmd)
    {
    case B_JY901S_RECV_TIME_CMD: // 时间输出
    {
        *size = B_JY901S_RECV_TIME_SIZE;
        return B_TRUE;
        break;
    }
    case B_JY901S_RECV_ACCELERATION_CMD: // 加速度输出
    {
        *size = B_JY901S_RECV_ACCELERATION_SIZE;
        return B_TRUE;
        break;
    }
    case B_JY901S_RECV_ANGULAR_VELOCITY_CMD: // 角速度输出
    {
        *size = B_JY901S_RECV_ANGULAR_VELOCITY_SIZE;
        return B_TRUE;
        break;
    }
    case B_JY901S_RECV_ANGLE_CMD: // 角度输出
    {
        *size = B_JY901S_RECV_ANGLE_SIZE;
        return B_TRUE;
        break;
    }
    case B_JY901S_RECV_MAGNETIC_FIELD_CMD: // 磁场输出
    {
        *size = B_JY901S_RECV_MAGNETIC_FIELD_SIZE;
        return B_TRUE;
        break;
    }
    case B_JY901S_RECV_PORT_STATUS_CMD: // 端口状态数据输出
    {
        *size = B_JY901S_RECV_PORT_STATUS_SIZE;
        return B_TRUE;
        break;
    }
    case B_JY901S_RECV_PRESSURE_CMD: // 气压、高度输出
    {
        *size = B_JY901S_RECV_PRESSURE_SIZE;
        return B_TRUE;
        break;
    }
    case B_JY901S_RECV_LONGITUDE_DIMENSION_CMD: // 经纬度输出
    {
        *size = B_JY901S_RECV_LONGITUDE_DIMENSION_SIZE;
        return B_TRUE;
        break;
    }
    case B_JY901S_RECV_GPS_CMD: // 地速，GPS输出
    {
        *size = B_JY901S_RECV_GPS_SIZE;
        return B_TRUE;
        break;
    }
    case B_JY901S_RECV_QUATERNION_CMD: // 四元数输出
    {
        *size = B_JY901S_RECV_QUATERNION_SIZE;
        return B_TRUE;
        break;
    }
    case B_JY901S_RECV_SATELLITE_CMD: // 卫星定位精度输出
    {
        *size = B_JY901S_RECV_SATELLITE_SIZE;
        return B_TRUE;
        break;
    }
    }
    return B_FALSE;
}

//*******************************************************************
// 函  数：vu8 bsp_JY901SRecvCheckSum(vu8 *cmd,vu32 cmdSize)
// 功  能：判断接收jy901s 模块发来的命令是否正确
// 参  数：*cmd 是接收到的协议数组
//         cmdSize 是接收到的协议数组长度
// 返回值：正确1，错误0
// 备  注：无
//*******************************************************************
vu8 bsp_JY901SRecvCheckSum(vu8 *cmd, vu32 cmdSize)
{
    int i = 0;
    vu8 sum = 0;
    // 做校验和的长度是从第一个到倒数第二个，最后一个是模块的校验和的值
    for (i = 0; i < cmdSize - 1; i++)
    {
        sum += cmd[i];
    }

    // 1到倒数第二个值相加的校验和跟 接收的最后一个值比较，一致表示校验正确，不一致表示错误
    if (sum == cmd[cmdSize - 1])
    {
        return B_TRUE;
    }

    return B_FALSE;
}
