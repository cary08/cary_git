#include "bsp_pid.h"

B_PositionPID B_MotorAPPid; // 电机A位置式 PID
B_PositionPID B_MotorBPPid; // 电机B位置式 PID

B_AddPID B_MotorAAPid; // 电机A增量式 PID
B_AddPID B_MotorBAPid; // 电机B增量式 PID

//*******************************************************************
// 函  数：void bsp_PIDPositionMotorAParamInit(void)
// 功  能：初始化电机A位置式pid相关的参数
// 参  数：无
//
// 返回值：无
// 备  注：主要是初始化 Kp Ki Kd  左轮
//*******************************************************************
void bsp_PIDPositionMotorAParamInit(void)
{
    B_MotorAPPid.target_val = 0.0; // 这个是 m/s 的速度
    B_MotorAPPid.actual_val = 0.0;
    B_MotorAPPid.err = 0.0;
    B_MotorAPPid.err_last = 0.0;
    B_MotorAPPid.integral = 0.0;
    B_MotorAPPid.Kp = 1190.00; // PI 稳定值   1700 * 0.7 = 1190
    B_MotorAPPid.Ki = 4000.00; // PI 稳定值	 4000
    B_MotorAPPid.Kd = 300.00;  // 1000 * 0.3 = 300
}

//*******************************************************************
// 函  数：void app_PIDPositionMotorBParamInit(void)
// 功  能：初始化电机B位置式pid相关的参数
// 参  数：无
//
// 返回值：无
// 备  注：主要是初始化 Kp Ki Kd  右轮
//*******************************************************************
void bsp_PIDPositionMotorBParamInit(void)
{
    B_MotorBPPid.target_val = 0.0; // 这个是 m/s 的速度
    B_MotorBPPid.actual_val = 0.0;
    B_MotorBPPid.err = 0.0;
    B_MotorBPPid.err_last = 0.0;
    B_MotorBPPid.integral = 0.0;
    B_MotorBPPid.Kp = 1190.00; // PI 稳定值   1700 * 0.7 = 1190
    B_MotorBPPid.Ki = 4000.00; // PI 稳定值	 4000
    B_MotorBPPid.Kd = 300.00;  // 1000 * 0.3 = 300
}

//*******************************************************************
// 函  数：void bsp_PIDAddMotorAParamInit(void)
// 功  能：初始化电机A增量式pid相关的参数
// 参  数：无
//
// 返回值：无
// 备  注：主要是初始化 Kp Ki Kd
//*******************************************************************
void bsp_PIDAddMotorAParamInit(void)
{
    B_MotorAAPid.target_val = 0.0;
    B_MotorAAPid.actual_val = 0.0;
    B_MotorAAPid.err = 0.0;
    B_MotorAAPid.err_last = 0.0;
    B_MotorAAPid.err_next = 0.0;

    B_MotorAAPid.Kp = 0.05;
    B_MotorAAPid.Ki = 0.10;
    B_MotorAAPid.Kd = 0.01;
}

//*******************************************************************
// 函  数：void bsp_PIDAddMotorBParamInit(void)
// 功  能：初始化电机B增量式pid相关的参数
// 参  数：无
//
// 返回值：无
// 备  注：主要是初始化 Kp Ki Kd
//*******************************************************************
void bsp_PIDAddMotorBParamInit(void)
{
    B_MotorBAPid.target_val = 0.0;
    B_MotorBAPid.actual_val = 0.0;
    B_MotorBAPid.err = 0.0;
    B_MotorBAPid.err_last = 0.0;
    B_MotorBAPid.err_next = 0.0;

    B_MotorBAPid.Kp = 0.05;
    B_MotorBAPid.Ki = 0.10;
    B_MotorBAPid.Kd = 0.01;
}

//*******************************************************************
// 函  数：float bsp_PIDPositionMotorARealize(float temp_val)
// 功  能：通过位置式PID算法，得出电机A实际值
// 参  数：temp_val		当前实际速度值
//
// 返回值：目标pwm值
// 备  注：疑问：为什么给一个目标速度 和 实际速度 就能算出 PWM 值呢？
// 					其实pid也不会一下就计算出pwm值，也是逐步计算出pwm值的，计算出的pwm值不对时，
//					相应的实际速度值也不对，然后再给pid计算，直到 目标速度值 和 实际速度值 相等，
//					那么，此时的pwm值就是正确的了。
//*******************************************************************
float bsp_PIDPositionMotorARealize(float temp_val)
{
    // 计算目标值与实际值的误差
    B_MotorAPPid.err = B_MotorAPPid.target_val - temp_val;
    // 误差累计
    B_MotorAPPid.integral += B_MotorAPPid.err;
    // PID算法实现
    B_MotorAPPid.actual_val = B_MotorAPPid.Kp * B_MotorAPPid.err + B_MotorAPPid.Ki * B_MotorAPPid.integral + B_MotorAPPid.Kd * (B_MotorAPPid.err - B_MotorAPPid.err_last);
    // 误差传递
    B_MotorAPPid.err_last = B_MotorAPPid.err;
    // 返回当前实际值
    return B_MotorAPPid.actual_val;
}

//*******************************************************************
// 函  数：float bsp_PIDPositionMotorBRealize(float temp_val)
// 功  能：通过位置式PID算法，得出电机B实际值
// 参  数：temp_val		当前值
//
// 返回值：目标pwm值
// 备  注：无
//*******************************************************************
float bsp_PIDPositionMotorBRealize(float temp_val)
{
    // 计算目标值与实际值的误差
    B_MotorBPPid.err = B_MotorBPPid.target_val - temp_val;
    // 误差累计
    B_MotorBPPid.integral += B_MotorBPPid.err;
    // PID算法实现
    B_MotorBPPid.actual_val = B_MotorBPPid.Kp * B_MotorBPPid.err + B_MotorBPPid.Ki * B_MotorBPPid.integral + B_MotorBPPid.Kd * (B_MotorBPPid.err - B_MotorBPPid.err_last);
    // 误差传递
    B_MotorBPPid.err_last = B_MotorBPPid.err;
    // 返回当前实际值
    return B_MotorBPPid.actual_val;
}

//*******************************************************************
// 函  数：float bsp_PIDAddMotorARealize(float temp_val)
// 功  能：通过增量式PID算法，得出电机A实际值
// 参  数：temp_val		当前值
//
// 返回值：无
// 备  注：无
//*******************************************************************
float bsp_PIDAddMotorARealize(float temp_val)
{
    // 计算目标值与实际值的误差
    B_MotorAAPid.err = B_MotorAAPid.target_val - temp_val;

    // PID 算法实现
    // 比例项*当前误差-上一次误差   +  积分项*当前误差  +  微分项*(当前误差 - 二倍的上一次误差 + 最后一次误差)
    float increment_val = B_MotorAAPid.Kp * (B_MotorAAPid.err - B_MotorAAPid.err_next) + B_MotorAAPid.Ki * B_MotorAAPid.err + B_MotorAAPid.Kd * (B_MotorAAPid.err - 2 * B_MotorAAPid.err_next + B_MotorAAPid.err_last);

    // 累加，是增量算法，当前算出的结果要加上上一次计算出来的结果
    B_MotorAAPid.actual_val += increment_val;

    // 传递误差
    // 将上一次的误差 赋值 给 最后一次的误差
    B_MotorAAPid.err_last = B_MotorAAPid.err_next;
    // 将当前误差 赋值 给 上一次的误差
    B_MotorAAPid.err_next = B_MotorAAPid.err;

    // 返回当前实际值
    return B_MotorAAPid.actual_val;
}

//*******************************************************************
// 函  数：float bsp_PIDAddMotorBRealize(float temp_val)
// 功  能：通过增量式PID算法，得出电机B实际值
// 参  数：temp_val		当前值
//
// 返回值：无
// 备  注：无
//*******************************************************************
float bsp_PIDAddMotorBRealize(float temp_val)
{
    // 计算目标值与实际值的误差
    B_MotorBAPid.err = B_MotorBAPid.target_val - temp_val;

    // PID 算法实现
    // 比例项*当前误差-上一次误差   +  积分项*当前误差  +  微分项*(当前误差 - 二倍的上一次误差 + 最后一次误差)
    float increment_val = B_MotorBAPid.Kp * (B_MotorBAPid.err - B_MotorBAPid.err_next) + B_MotorBAPid.Ki * B_MotorBAPid.err + B_MotorBAPid.Kd * (B_MotorBAPid.err - 2 * B_MotorBAPid.err_next + B_MotorBAPid.err_last);

    // 累加，是增量算法，当前算出的结果要加上上一次计算出来的结果
    B_MotorBAPid.actual_val += increment_val;

    // 传递误差
    // 将上一次的误差 赋值 给 最后一次的误差
    B_MotorBAPid.err_last = B_MotorBAPid.err_next;
    // 将当前误差 赋值 给 上一次的误差
    B_MotorBAPid.err_next = B_MotorBAPid.err;

    // 返回当前实际值
    return B_MotorBAPid.actual_val;
}

//*******************************************************************
// 函  数：void bsp_PIDMotorASetTarget(float targetVal)
// 功  能：设置电机A当前目标值
// 参  数：targetVal		要修改的目标值 速度 m/s
//
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_PIDMotorASetTarget(float targetVal)
{
    B_MotorAPPid.target_val = targetVal;
}

//*******************************************************************
// 函  数：void bsp_PIDMotorBSetTarget(float targetVal)
// 功  能：设置电机B当前目标值
// 参  数：targetVal		要修改的目标值 速度 m/s
//
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_PIDMotorBSetTarget(float targetVal)
{
    B_MotorBPPid.target_val = targetVal;
}
