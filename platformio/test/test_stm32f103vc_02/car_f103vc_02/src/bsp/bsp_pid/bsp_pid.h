#ifndef _BSP_PID_H_
#define _BSP_PID_H_
#include "../bsp.h"

// 位置式 PID
typedef struct
{
    float target_val; // 目标值
    float actual_val; // 实际值
    float err;        // 定义偏差值
    float err_last;   // 定义上一个偏差值
    float Kp, Ki, Kd; // 定义比例、积分、微分系数
    float integral;   // 定义积分值
} B_PositionPID;

extern B_PositionPID B_MotorAPPid; // 电机A位置式 PID
extern B_PositionPID B_MotorBPPid; // 电机B位置式 PID

// 增量式 PID
typedef struct
{
    float target_val; // 目标值
    float actual_val; // 实际值
    float err;        // 定义偏差值
    float err_next;   // 定义下一个偏差值
    float err_last;   // 定义最后一个偏差值
    float Kp, Ki, Kd; // 定义比例、积分、微分系数（积分都没了，怎么还定义ki？）
} B_AddPID;

extern B_AddPID B_MotorAAPid; // 电机A增量式 PID
extern B_AddPID B_MotorBAPid; // 电机B增量式 PID

void bsp_PIDPositionMotorAParamInit(void);
void bsp_PIDPositionMotorBParamInit(void);
void bsp_PIDAddMotorAParamInit(void);
void bsp_PIDAddMotorBParamInit(void);
float bsp_PIDPositionMotorARealize(float temp_val);
float bsp_PIDPositionMotorBRealize(float temp_val);
float bsp_PIDAddMotorARealize(float temp_val);
float bsp_PIDAddMotorBRealize(float temp_val);
void bsp_PIDMotorASetTarget(float targetVal);
void bsp_PIDMotorBSetTarget(float targetVal);

#endif
