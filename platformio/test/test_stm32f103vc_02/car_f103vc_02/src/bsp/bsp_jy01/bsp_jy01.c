#include "bsp_jy01.h"

//*******************************************************************
// 函  数：void bsp_JY01SetLeftSpeed(vu16 pwm)
// 功  能：设置左轮 pwm 速度
// 参  数：pwm	速度的快慢，占空比越高速度越快
//
// 返回值：无
// 备  注：定时器8通道1
//*******************************************************************
void bsp_JY01SetLeftSpeed(vu16 pwm)
{
    if (pwm > B_JY01_BRAKE_SPEED_PWM_MAX)
    {
        pwm = B_JY01_BRAKE_SPEED_PWM_MAX; // 最大值限制
    }

    // PWM不能是负值，小于0时就是置为 0
    if (pwm <= B_NULL)
    {
        pwm = B_NULL;
    }

    __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_1, pwm);
}

//*******************************************************************
// 函  数：void bsp_JY01SetRightSpeed(vu16 pwm)
// 功  能：设置右轮 pwm 速度
// 参  数：pwm	速度的快慢，占空比越高速度越快
//
// 返回值：无
// 备  注：定时器8通道2
//*******************************************************************
void bsp_JY01SetRightSpeed(vu16 pwm)
{
    if (pwm > B_JY01_BRAKE_SPEED_PWM_MAX)
    {
        pwm = B_JY01_BRAKE_SPEED_PWM_MAX; // 最大值限制
    }

    // PWM不能是负值，小于0时就是置为 0
    if (pwm <= B_NULL)
    {
        pwm = B_NULL;
    }

    __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_2, pwm);
}

//*******************************************************************
// 函  数：void bsp_JY01SetLeftBrake(vu16 pwm)
// 功  能：设置左轮 pwm 刹车
// 参  数：pwm	刹车的强度，占空比越高刹车越狠
//
// 返回值：无
// 备  注：定时器8通道3
//*******************************************************************
void bsp_JY01SetLeftBrake(vu16 pwm)
{
    if (pwm > B_JY01_BRAKE_SPEED_PWM_MAX)
    {
        pwm = B_JY01_BRAKE_SPEED_PWM_MAX; // 最大值限制
    }

    // PWM不能是负值，小于0时就是置为 0
    if (pwm <= B_NULL)
    {
        pwm = B_NULL;
    }

    __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_3, pwm);
}

//*******************************************************************
// 函  数：void bsp_JY01SetRightBrake(vu16 pwm)
// 功  能：设置右轮 pwm 刹车
// 参  数：pwm	刹车的强度，占空比越高刹车越狠
//
// 返回值：无
// 备  注：定时器8通道4
//*******************************************************************
void bsp_JY01SetRightBrake(vu16 pwm)
{
    if (pwm > B_JY01_BRAKE_SPEED_PWM_MAX)
    {
        pwm = B_JY01_BRAKE_SPEED_PWM_MAX; // 最大值限制
    }

    // PWM不能是负值，小于0时就是置为 0
    if (pwm <= B_NULL)
    {
        pwm = B_NULL;
    }

    __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_4, pwm);
}
