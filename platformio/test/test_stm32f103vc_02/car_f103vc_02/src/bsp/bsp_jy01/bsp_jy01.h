#ifndef _BSP_JY01_H_
#define _BSP_JY01_H_
#include "../bsp.h"

// 轮子刹车 和 速度 pwm 的最大值 占空比为 100%
#define B_JY01_BRAKE_SPEED_PWM_MAX 7200

#define B_JY01_RIGHT_HIGH HAL_GPIO_WritePin(jy01_right_GPIO_Port, jy01_right_Pin, GPIO_PIN_SET)  // 设置 右轮 高电平
#define B_JY01_RIGHT_LOW HAL_GPIO_WritePin(jy01_right_GPIO_Port, jy01_right_Pin, GPIO_PIN_RESET) // 设置 右轮 低电平

#define B_JY01_LEFT_LOW HAL_GPIO_WritePin(jy01_left_GPIO_Port, jy01_left_Pin, GPIO_PIN_SET)    // 设置 左轮 高电平
#define B_JY01_LEFT_HIGH HAL_GPIO_WritePin(jy01_left_GPIO_Port, jy01_left_Pin, GPIO_PIN_RESET) // 设置 左轮 低电平

void bsp_JY01SetLeftSpeed(vu16 pwm);
void bsp_JY01SetRightSpeed(vu16 pwm);
void bsp_JY01SetLeftBrake(vu16 pwm);
void bsp_JY01SetRightBrake(vu16 pwm);

#endif
