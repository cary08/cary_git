#include "bsp_infrared.h"

B_InfraredDef B_InfraredObj = {0};

//*******************************************************************
// 函  数：void bsp_ReadInfrared(void)
// 功  能：查询红外状态
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_ReadInfrared(void)
{
    if (B_READ_INFRA1 == 0) // 红外1有遮挡物
    {
        B_InfraredObj.En1 = 1;
    }
    if (B_READ_INFRA1 == 1)
    {
        B_InfraredObj.En1 = 0;
    }

    if (B_READ_INFRA2 == 0) // 红外2有遮挡物
    {
        B_InfraredObj.En2 = 1;
    }
    if (B_READ_INFRA2 == 1)
    {
        B_InfraredObj.En2 = 0;
    }

    if (B_READ_INFRA3 == 0) // 红外3有遮挡物
    {
        B_InfraredObj.En3 = 1;
    }
    if (B_READ_INFRA3 == 1)
    {
        B_InfraredObj.En3 = 0;
    }
}
