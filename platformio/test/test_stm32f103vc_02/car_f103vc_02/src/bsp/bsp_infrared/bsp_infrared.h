#ifndef _BSP_INFRARED_H_
#define _BSP_INFRARED_H_
#include "../bsp.h"

#define B_READ_INFRA1 HAL_GPIO_ReadPin(Infra1_GPIO_Port, Infra1_Pin)
#define B_READ_INFRA2 HAL_GPIO_ReadPin(Infra2_GPIO_Port, Infra2_Pin)
#define B_READ_INFRA3 HAL_GPIO_ReadPin(Infra3_GPIO_Port, Infra3_Pin)
#define B_READ_INFRA4 HAL_GPIO_ReadPin(Infra4_GPIO_Port, Infra4_Pin)

typedef struct
{
    vu8 En1; // 0为无障碍 1为有障碍
    vu8 En2;
    vu8 En3;
    vu8 En4;
} B_InfraredDef; /*Decice_Infrared结构体*/

extern B_InfraredDef B_InfraredObj;

void bsp_ReadInfrared(void);

#endif
