#ifndef _BSP_H_
#define _BSP_H_
#include "../cubemx_hal/cubemx_hal.h"
#include "bsp_encode/bsp_encode.h"
#include "bsp_infrared/bsp_infrared.h"
#include "bsp_jy01/bsp_jy01.h"
#include "bsp_jy901s/bsp_jy901s.h"
#include "bsp_pid/bsp_pid.h"
#include "bsp_speak/bsp_speak.h"
#include "bsp_ultra/bsp_ultra.h"
#include "bsp_wheel/bsp_wheel.h"

#define B_NULL 0
#define B_TRUE 1
#define B_FALSE 0

void bsp_Init(void);

void bsp_IWDGFeel(void);

#endif
