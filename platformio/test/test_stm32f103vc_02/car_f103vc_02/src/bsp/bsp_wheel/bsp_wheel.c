#include "bsp_wheel.h"

B_WheelDef B_WheelObj = {0};

//*******************************************************************
// 函  数：short bsp_EncoderRead(vu8 TIMX)
// 功  能：左右电机的 当前的编码器值，读取一次并清零
// 参  数：TIMX  B_ENCODER_LEFT左编码器值 B_ENCODER_RIGHT右编码器值
//
// 返回值：无
// 备  注：无
//*******************************************************************
short bsp_WheelEncoderRead(vu8 TIMX)
{
    short encoderVal = B_NULL;
    switch (TIMX)
    {
    case B_ENCODER_RIGHT:
    {
        encoderVal = (short)bsp_EncoderGet(TIMX); // 右电机
        bsp_EncoderSet(TIMX, B_NULL);
        B_WheelObj.RightEncodeAllVal += encoderVal > 0 ? encoderVal : -encoderVal; // 记录右轮编码器的总值
        B_WheelObj.RightEncodeVal = encoderVal;
        break;
    }

    case B_ENCODER_LEFT:
    {
        encoderVal = (short)bsp_EncoderGet(TIMX); // 左电机
        bsp_EncoderSet(TIMX, B_NULL);
        B_WheelObj.LeftEncodeAllVal += encoderVal > 0 ? encoderVal : -encoderVal; // 记录左轮编码器的总值
        B_WheelObj.LeftEncodeVal = encoderVal;
        break;
    }
    }

    return encoderVal;
}

//*******************************************************************
// 函  数：void bsp_WheelLeftSetTargetSpeed(float speed)
// 功  能：设置左轮目标速度值，pid计算时会用到
// 参  数：speed		设置的左轮目标速度值
//
// 返回值：无
// 备  注：位置式PID
//*******************************************************************
void bsp_WheelLeftSetTargetSpeed(float speed)
{
    B_MotorAPPid.target_val = speed;
}

//*******************************************************************
// 函  数：void bsp_WheelRightSetTargetSpeed(float speed)
// 功  能：设置右轮目标速度值，pid计算时会用到
// 参  数：speed		设置的右轮目标速度值
//
// 返回值：无
// 备  注：位置式PID
//*******************************************************************
void bsp_WheelRightSetTargetSpeed(float speed)
{
    B_MotorBPPid.target_val = speed;
}

//*******************************************************************
// 函  数：int bsp_WheelLeftPidGetPwm(float temp_val)
// 功  能：通过pid获取 左轮 的pwm值
// 参  数：temp_val		当前左轮实际速度值
//
// 返回值：目标pwm值
// 备  注：位置式PID
//*******************************************************************
int bsp_WheelLeftPidGetPwm(float temp_val)
{
    return bsp_PIDPositionMotorARealize(temp_val);
}

//*******************************************************************
// 函  数：int bsp_WheelRightPidGetPwm(float temp_val)
// 功  能：通过pid获取 右轮 的pwm值
// 参  数：temp_val		当前右轮实际速度值
//
// 返回值：目标pwm值
// 备  注：位置式PID
//*******************************************************************
int bsp_WheelRightPidGetPwm(float temp_val)
{
    return bsp_PIDPositionMotorBRealize(temp_val);
}

//*******************************************************************
// 函  数：void bsp_WheelSetDirectionPwm(int leftPwm,int rightPwm)
// 功  能：设置两个轮子的转向 和 pwm值
// 参  数：leftPwm		设置左轮 负数为后退，正数为前进，绝对值为 pwm值
//         rightPwm	    设置右轮 负数为后退，正数为前进，绝对值为 pwm值
// 返回值：无
// 备  注：如果速度值为0，将会刹车
//*******************************************************************
void bsp_WheelSetDirectionPwm(int leftPwm, int rightPwm)
{
    // 左轮
    if (leftPwm >= 0)
    {
        B_JY01_LEFT_HIGH; // 正转 左轮方向io脚高电平
    }
    else
    {
        B_JY01_LEFT_LOW; // 反转 左轮方向io脚低电平
    }

    // 右轮
    if (rightPwm >= 0)
    {
        B_JY01_RIGHT_HIGH; // 正转 右轮方向io脚高电平
    }
    else
    {
        B_JY01_RIGHT_LOW; // 反转 右轮方向io脚低电平
    }

    leftPwm = leftPwm > 0 ? leftPwm : -leftPwm;
    rightPwm = rightPwm > 0 ? rightPwm : -rightPwm;

    if (leftPwm > B_NULL && rightPwm > B_NULL)
    {
        // 取消刹车
        bsp_JY01SetLeftBrake(B_NULL);
        bsp_JY01SetRightBrake(B_NULL);
    }
    else if (leftPwm == B_NULL && rightPwm > B_NULL)
    {
        // 只有 左轮刹车
        bsp_JY01SetLeftBrake(B_JY01_BRAKE_SPEED_PWM_MAX);
    }
    else if (leftPwm > B_NULL && rightPwm == B_NULL)
    {
        // 只有 右轮刹车
        bsp_JY01SetRightBrake(B_JY01_BRAKE_SPEED_PWM_MAX);
    }
    else if (leftPwm == B_NULL && rightPwm == B_NULL)
    {
        // 双轮刹车
        bsp_JY01SetLeftBrake(B_JY01_BRAKE_SPEED_PWM_MAX);
        bsp_JY01SetRightBrake(B_JY01_BRAKE_SPEED_PWM_MAX);
    }

    // 设置左右轮的pwm
    bsp_JY01SetLeftSpeed(leftPwm);
    bsp_JY01SetRightSpeed(rightPwm);
}
