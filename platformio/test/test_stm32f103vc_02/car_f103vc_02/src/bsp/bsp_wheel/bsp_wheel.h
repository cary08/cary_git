#ifndef _BSP_WHEEL_H_
#define _BSP_WHEEL_H_
#include "../bsp.h"

// 左右轮 获取的编码器值符号是否正确，如果不正确就去 乘以 这个方向值 使其符号正确
// 正数向前，负数向后   暂时都为1 表示获取的值是正确值 正数向前，负数向后
// 如果值的符号不正确改为 -1 即可
#define B_WHEEL_LEFT_ENCODE_DIRECTION 1
#define B_WHEEL_RIGHT_ENCODE_DIRECTION 1

// 轮子刹车的pwm值
#define B_WHEEL_BRAKE_PWM_VAL 5000

// 轮间距，两个轮子中心 的间距。单位米
#define B_WHEEL_SPACING 0.415

// 圆周率
#define B_WHEEL_PI 3.1415927

// 轮子周长 单位米
#define B_WHEEL_PERIMETER 0.478

// 轮子转一圈编码器的值
#define B_WHEEL_ENCODE_VAL 208

// 轮子电机减速比 1为1：1就是没有减速比
#define B_WHEEL_REDUCTION_RATIO 1

// 每100ms 获取一次速度值
#define B_WHEEL_GET_SPEED_MS 100

// 时间系数： 秒/获取速度间隔时间 就是单位时间，计算 转/秒 时用到。
#define B_WHEEL_DETECTION_TIME (1000.0 / B_WHEEL_GET_SPEED_MS)

typedef struct _B_WheelStr
{
    // 左右轮从开机到当前 总编码器值
    int RightEncodeAllVal;
    int LeftEncodeAllVal;

    // 左右轮实时 单位时间内的值
    int RightEncodeVal;
    int LeftEncodeVal;

    // 左右轮实时 转/秒
    float LeftTurnSecond;
    float RightTurnSecond;

    // 左右轮实时 转速m/s
    float LeftSpeed;
    float RightSpeed;

    // 实时的 线速度 和 角速度
    float SpeedX;
    float SpeedZ;

    // 设置的左右轮速度
    float SetLeftSpeed;
    float SetRightSpeed;

    // 是否刹车，避障、掉线 会置一，其他情况是0表示不刹车
    vu8 Obstacle;

} B_WheelDef;

extern B_WheelDef B_WheelObj;

void bsp_WheelInit(void);
short bsp_WheelEncoderRead(vu8 TIMX);
void bsp_WheelLeftSetTargetSpeed(float speed);
void bsp_WheelRightSetTargetSpeed(float speed);
int bsp_WheelLeftPidGetPwm(float temp_val);
int bsp_WheelRightPidGetPwm(float temp_val);
void bsp_WheelSetDirectionPwm(int leftPwm, int rightPwm);

#endif
