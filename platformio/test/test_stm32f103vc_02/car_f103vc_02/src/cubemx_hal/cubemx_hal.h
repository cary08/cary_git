#ifndef _CUBEMX_HAL_H_
#define _CUBEMX_HAL_H_
#include "stm32f1xx_hal.h"
#include "stdio.h"
#include "string.h"
#include "math.h"
#include "stdlib.h"
#include "sys_clock/sys_clock.h"
#include "sys_tick/sysTick.h"
#include "iwdg/iwdg.h"

#include "dma/dma.h"
#include "gpio/gpio.h"
#include "tim/tim.h"
#include "uart/usart.h"

typedef __IO uint32_t vu32;
typedef __IO uint16_t vu16;
typedef __IO uint8_t vu8;

typedef __I uint32_t vuc32;
typedef __I uint16_t vuc16;
typedef __I uint8_t vuc8;

void hal_BspInit(void);
void Error_Handler(void);
void HAL_MspInit(void);
#endif
