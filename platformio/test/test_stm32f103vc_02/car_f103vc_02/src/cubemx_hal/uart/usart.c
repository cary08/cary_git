/**
 ******************************************************************************
 * @file    usart.c
 * @brief   This file provides code for the configuration
 *          of the USART instances.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "usart.h"

/* USER CODE BEGIN 0 */
HAL_UartDef HAL_Uart = {0};
/* USER CODE END 0 */

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart3_rx;

/* USART1 init function */

void MX_USART1_UART_Init(void)
{

    huart1.Instance = USART1;
    huart1.Init.BaudRate = 115200;
    huart1.Init.WordLength = UART_WORDLENGTH_8B;
    huart1.Init.StopBits = UART_STOPBITS_1;
    huart1.Init.Parity = UART_PARITY_NONE;
    huart1.Init.Mode = UART_MODE_TX_RX;
    huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart1.Init.OverSampling = UART_OVERSAMPLING_16;
    if (HAL_UART_Init(&huart1) != HAL_OK)
    {
        Error_Handler();
    }
}
/* USART2 init function */

void MX_USART2_UART_Init(void)
{

    huart2.Instance = USART2;
    huart2.Init.BaudRate = 115200;
    huart2.Init.WordLength = UART_WORDLENGTH_8B;
    huart2.Init.StopBits = UART_STOPBITS_1;
    huart2.Init.Parity = UART_PARITY_NONE;
    huart2.Init.Mode = UART_MODE_TX_RX;
    huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart2.Init.OverSampling = UART_OVERSAMPLING_16;
    if (HAL_UART_Init(&huart2) != HAL_OK)
    {
        Error_Handler();
    }
}
/* USART3 init function */

void MX_USART3_UART_Init(void)
{
    huart3.Instance = USART3;
    huart3.Init.BaudRate = 9600;
    huart3.Init.WordLength = UART_WORDLENGTH_8B;
    huart3.Init.StopBits = UART_STOPBITS_1;
    huart3.Init.Parity = UART_PARITY_NONE;
    huart3.Init.Mode = UART_MODE_TX_RX;
    huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart3.Init.OverSampling = UART_OVERSAMPLING_16;
    if (HAL_UART_Init(&huart3) != HAL_OK)
    {
        Error_Handler();
    }
}

void HAL_UART_MspInit(UART_HandleTypeDef *uartHandle)
{

    GPIO_InitTypeDef GPIO_InitStruct = {0};
    if (uartHandle->Instance == USART1)
    {
        /* USER CODE BEGIN USART1_MspInit 0 */

        /* USER CODE END USART1_MspInit 0 */
        /* USART1 clock enable */
        __HAL_RCC_USART1_CLK_ENABLE();

        __HAL_RCC_GPIOA_CLK_ENABLE();
        /**USART1 GPIO Configuration
        PA9     ------> USART1_TX
        PA10     ------> USART1_RX
        */
        GPIO_InitStruct.Pin = ros_usart_tx_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
        HAL_GPIO_Init(ros_usart_tx_GPIO_Port, &GPIO_InitStruct);

        GPIO_InitStruct.Pin = ros_usart_rx_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(ros_usart_rx_GPIO_Port, &GPIO_InitStruct);

        /* USART1 interrupt Init */
        HAL_NVIC_SetPriority(USART1_IRQn, 1, 0);
        HAL_NVIC_EnableIRQ(USART1_IRQn);
        /* USER CODE BEGIN USART1_MspInit 1 */

        /* USER CODE END USART1_MspInit 1 */
    }
    else if (uartHandle->Instance == USART2)
    {
        /* USER CODE BEGIN USART2_MspInit 0 */

        /* USER CODE END USART2_MspInit 0 */
        /* USART2 clock enable */
        __HAL_RCC_USART2_CLK_ENABLE();

        __HAL_RCC_GPIOA_CLK_ENABLE();
        /**USART2 GPIO Configuration
        PA2     ------> USART2_TX
        PA3     ------> USART2_RX
        */
        GPIO_InitStruct.Pin = jy901s_usart_tx_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
        HAL_GPIO_Init(jy901s_usart_tx_GPIO_Port, &GPIO_InitStruct);

        GPIO_InitStruct.Pin = jy901s_usart_rx_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(jy901s_usart_rx_GPIO_Port, &GPIO_InitStruct);

        /* USART2 interrupt Init */
        HAL_NVIC_SetPriority(USART2_IRQn, 2, 0);
        HAL_NVIC_EnableIRQ(USART2_IRQn);
        /* USER CODE BEGIN USART2_MspInit 1 */

        /* USER CODE END USART2_MspInit 1 */
    }
    else if (uartHandle->Instance == USART3)
    {
        /* USER CODE BEGIN USART3_MspInit 0 */

        /* USER CODE END USART3_MspInit 0 */
        /* USART3 clock enable */
        __HAL_RCC_USART3_CLK_ENABLE();

        __HAL_RCC_GPIOD_CLK_ENABLE();
        /**USART3 GPIO Configuration
        PD8     ------> USART3_TX
        PD9     ------> USART3_RX
        */
        GPIO_InitStruct.Pin = pc_usart_tx_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
        HAL_GPIO_Init(pc_usart_tx_GPIO_Port, &GPIO_InitStruct);

        GPIO_InitStruct.Pin = pc_usart_rx_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(pc_usart_rx_GPIO_Port, &GPIO_InitStruct);

        __HAL_AFIO_REMAP_USART3_ENABLE();

        /* USART3 DMA Init */
        /* USART3_RX Init */
        hdma_usart3_rx.Instance = DMA1_Channel3;
        hdma_usart3_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
        hdma_usart3_rx.Init.PeriphInc = DMA_PINC_DISABLE;
        hdma_usart3_rx.Init.MemInc = DMA_MINC_ENABLE;
        hdma_usart3_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_usart3_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
        hdma_usart3_rx.Init.Mode = DMA_NORMAL;
        hdma_usart3_rx.Init.Priority = DMA_PRIORITY_LOW;
        if (HAL_DMA_Init(&hdma_usart3_rx) != HAL_OK)
        {
            Error_Handler();
        }

        __HAL_LINKDMA(uartHandle, hdmarx, hdma_usart3_rx);

        /* USART3 interrupt Init */
        HAL_NVIC_SetPriority(USART3_IRQn, 3, 0);
        HAL_NVIC_EnableIRQ(USART3_IRQn);
        /* USER CODE BEGIN USART3_MspInit 1 */

        /* USER CODE END USART3_MspInit 1 */
    }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef *uartHandle)
{

    if (uartHandle->Instance == USART1)
    {
        /* USER CODE BEGIN USART1_MspDeInit 0 */

        /* USER CODE END USART1_MspDeInit 0 */
        /* Peripheral clock disable */
        __HAL_RCC_USART1_CLK_DISABLE();

        /**USART1 GPIO Configuration
        PA9     ------> USART1_TX
        PA10     ------> USART1_RX
        */
        HAL_GPIO_DeInit(GPIOA, ros_usart_tx_Pin | ros_usart_rx_Pin);

        /* USART1 interrupt Deinit */
        HAL_NVIC_DisableIRQ(USART1_IRQn);
        /* USER CODE BEGIN USART1_MspDeInit 1 */

        /* USER CODE END USART1_MspDeInit 1 */
    }
    else if (uartHandle->Instance == USART2)
    {
        /* USER CODE BEGIN USART2_MspDeInit 0 */

        /* USER CODE END USART2_MspDeInit 0 */
        /* Peripheral clock disable */
        __HAL_RCC_USART2_CLK_DISABLE();

        /**USART2 GPIO Configuration
        PA2     ------> USART2_TX
        PA3     ------> USART2_RX
        */
        HAL_GPIO_DeInit(GPIOA, jy901s_usart_tx_Pin | jy901s_usart_rx_Pin);

        /* USART2 interrupt Deinit */
        HAL_NVIC_DisableIRQ(USART2_IRQn);
        /* USER CODE BEGIN USART2_MspDeInit 1 */

        /* USER CODE END USART2_MspDeInit 1 */
    }
    else if (uartHandle->Instance == USART3)
    {
        /* USER CODE BEGIN USART3_MspDeInit 0 */

        /* USER CODE END USART3_MspDeInit 0 */
        /* Peripheral clock disable */
        __HAL_RCC_USART3_CLK_DISABLE();

        /**USART3 GPIO Configuration
        PD8     ------> USART3_TX
        PD9     ------> USART3_RX
        */
        HAL_GPIO_DeInit(GPIOD, pc_usart_tx_Pin | pc_usart_rx_Pin);

        /* USART3 DMA DeInit */
        HAL_DMA_DeInit(uartHandle->hdmarx);

        /* USART3 interrupt Deinit */
        HAL_NVIC_DisableIRQ(USART3_IRQn);
        /* USER CODE BEGIN USART3_MspDeInit 1 */

        /* USER CODE END USART3_MspDeInit 1 */
    }
}

/* USER CODE BEGIN 1 */
int test_uart_3;
void HAL_UART_USER_IRQCallback(UART_HandleTypeDef *huart)
{
    if (huart->Instance == USART1)
    {

        if (__HAL_UART_GET_FLAG(huart, UART_FLAG_RXNE) != RESET)
        {

            switch (HAL_Uart.uart_ros_state)
            {
            case 0:
            {
                // 初始化各变量

                // 开始接收数据，空闲标志 置零
                HAL_Uart.uart_ros_idle_flag = 0;
                // 开始接收数据，数据长度 置零
                HAL_Uart.uart_ros_buff_len = 0;

                HAL_Uart.uart_ros_state = 1;
                break;
            }

            case 1:
            {
                // 除了第一次进入，后边就不需要初始化变量了，直接去下边直行赋值操作
                break;
            }
            }

            // 大于接收最大值清空数组
            if (HAL_Uart.uart_ros_buff_len >= HAL_UART_BUFF_SIZE_MAX)
            {
                memset(HAL_Uart.uart_ros_buff, 0, sizeof(HAL_Uart.uart_ros_buff));
                HAL_Uart.uart_ros_buff_len = 0;
            }

            HAL_UART_Receive(huart, &HAL_Uart.uart_ros_buff[HAL_Uart.uart_ros_buff_len], 1, 100);
            HAL_Uart.uart_ros_buff_len++;

            __HAL_UART_CLEAR_FLAG(huart, UART_FLAG_RXNE);
        }

        if (__HAL_UART_GET_FLAG(huart, UART_FLAG_IDLE) != RESET)
        {
            // 一帧数据接收完毕，进入空闲中断，空闲标志置一
            HAL_Uart.uart_ros_idle_flag = 1;
            // 状态置零，接收数据时初始化
            HAL_Uart.uart_ros_state = 0;

            // 测试用
            // HAL_UART_Transmit(huart, HAL_Uart.uart_ros_buff, HAL_Uart.uart_ros_buff_len, 100);
            // HAL_Uart.uart_ros_buff_len = 0;

            __HAL_UART_CLEAR_IDLEFLAG(huart);
        }
    }
    if (huart->Instance == USART2)
    {

        if (__HAL_UART_GET_FLAG(huart, UART_FLAG_RXNE) != RESET)
        {

            switch (HAL_Uart.uart_pc_state)
            {
            case 0:
            {
                // 初始化各变量

                // 开始接收数据，空闲标志 置零，主要初始化这个变量，目的是让主程序知道此时正在接收数据，不可读取
                // 接收完毕进入空闲中断，会置一，主循环中就去读取数据
                HAL_Uart.uart_pc_idle_flag = 0;
                // 开始接收数据，数据长度 置零
                HAL_Uart.uart_pc_buff_len = 0;

                HAL_Uart.uart_pc_state = 1;
                break;
            }

            case 1:
            {
                // 除了第一次进入，后边就不需要初始化变量了，直接去下边直行赋值操作
                break;
            }
            }

            if (HAL_Uart.uart_pc_buff_len >= HAL_UART_BUFF_SIZE_MAX)
            {
                memset(HAL_Uart.uart_pc_buff, 0, sizeof(HAL_Uart.uart_pc_buff));
                HAL_Uart.uart_pc_buff_len = 0;
            }

            HAL_UART_Receive(huart, &HAL_Uart.uart_pc_buff[HAL_Uart.uart_pc_buff_len], 1, 100);
            HAL_Uart.uart_pc_buff_len++;

            __HAL_UART_CLEAR_FLAG(huart, UART_FLAG_RXNE);
        }

        if (__HAL_UART_GET_FLAG(huart, UART_FLAG_IDLE) != RESET)
        {
            // 一帧数据接收完毕，进入空闲中断，空闲标志置一
            HAL_Uart.uart_pc_idle_flag = 1;
            // 状态置零，接收数据时初始化
            HAL_Uart.uart_pc_state = 0;

            // 测试用
            // HAL_UART_Transmit(huart, HAL_Uart.uart_pc_buff, HAL_Uart.uart_pc_buff_len, 100);
            // HAL_Uart.uart_pc_buff_len = 0;

            __HAL_UART_CLEAR_IDLEFLAG(huart);
        }
    }

    // 这个用的 DMA 外设-》内存 方式，姿态模块数据量太大，不占用cpu资源进行赋值操作
    if (huart->Instance == USART3)
    {

        uint16_t temp_len = 0;

        if (__HAL_UART_GET_FLAG(huart, UART_FLAG_IDLE) != RESET)
        {
            __HAL_UART_CLEAR_FLAG(huart, UART_FLAG_IDLE);

            // 进入此中断，说明数据已经传入DMA中了。
            HAL_UART_DMAStop(huart);

            // 获取还没有接收到的字节长度值
            temp_len = __HAL_DMA_GET_COUNTER(&hdma_usart3_rx);

            // 可接收的总长度 减去 还没有接收的长度 等于 已接收到的字节长度
            HAL_Uart.uart_jy901s_buff_len = HAL_UART_BUFF_SIZE_MAX - temp_len;

            // 此时，数据已被传入到 HAL_Uart.uart_jy901s_buff 中了
            HAL_Uart.uart_jy901s_idle_flag = 1;
        }

        // 重新打开接收中断，不然下次就进不来了
        __HAL_UART_ENABLE_IT(huart, UART_IT_RXNE);
        // 重新开启 DMA 接收中断
        HAL_UART_Receive_DMA(huart, HAL_Uart.uart_jy901s_buff, HAL_UART_BUFF_SIZE_MAX);

        // 标志位置一，要处理数据了，这个if的代码，写在业务代码中判断，这里是个例子，注释掉，测试的时候在放开测测
        // if (HAL_Uart.uart_jy901s_idle_flag == 1)
        // {
        //     // printf("idle_start...\n");
        //     // HAL_Uart.uart_jy901s_idle_flag = 0;
        //     // HAL_UART_Transmit(&huart1, HAL_Uart.uart_jy901s_buff, HAL_Uart.uart_jy901s_buff_len, 1000);
        // }
    }
}

#if defined(__GNUC__)
int _write(int fd, char *ptr, int len)
{
    HAL_UART_Transmit(&huart2, (uint8_t *)ptr, len, 100);
    return len;
}
#elif defined(__ICCARM__)
size_t __write(int handle, const unsigned char *buffer, size_t size)
{
    HAL_UART_Transmit(&huart1, (uint8_t *)buffer, size, HAL_MAX_DELAY);
    return size;
}
#elif defined(__CC_ARM)
int fputc(int ch, FILE *f)
{
    HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, HAL_MAX_DELAY);
    return ch;
}
#endif
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
