#ifndef _SYS_CLOCK_H_
#define _SYS_CLOCK_H_
#include "../cubemx_hal.h"

void SystemClock_Config(void);
void _Error_Handler(char *file, int line);
#endif
