#ifndef _SYS_TICK_H_
#define _SYS_TICK_H_
#include "../cubemx_hal.h"

void HAL_TickInit(uint32_t TickPriority);
void HAL_Delay(uint32_t Delay);
__IO uint32_t HAL_TickGetMs(void);
__IO uint32_t HAL_TickGetMsNum(void);
__IO uint8_t HAL_TickIsTimeOut(__IO uint32_t oldNum, __IO uint32_t oldMs, __IO uint32_t outMs);
void HAL_IncTick(void);

#endif
