#include "sysTick.h"

static __IO uint32_t TimeMs = 0;
static __IO uint32_t TimeMsNum = 0;
static __IO uint32_t WaitMs = 0;

//*******************************************************************
// 函  数：void HAL_TickInit(uint32_t TickPriority)
// 功  能：滴答定时器初始化函数，覆盖了hal库的这个初始化函数
// 参  数：TickPriority  延时系数，SystemCoreClock / TickPriority
//              SystemCoreClock / 1000    = 72000    1ms中断一次
//              SystemCoreClock / 100000  = 720      10us中断一次
//              SystemCoreClock / 1000000 = 72       1us中断一次
// 返回值：无
// 备  注：延时1ms 参数填 1000
//*******************************************************************
void HAL_TickInit(uint32_t TickPriority)
{
    // 滴答定时器递减的数值，减到0就中断
    // SystemCoreClock / 1000    = 72000    1ms中断一次
    // SystemCoreClock / 100000  = 720      10us中断一次
    // SystemCoreClock / 1000000 = 72       1us中断一次
    // 设置装载值、开启中断、设置时钟源为内部时钟源AHB
    if (HAL_SYSTICK_Config(SystemCoreClock / TickPriority))
    {
        // error
        while (1)
            ;
    }

    // 设置中断优先级，不设置就会有问题，我知道了，不设置中断优先级会被其他外设中断打断，影响精准度
    // 果然，是串口3接收 jy901s 的中断导致的
    HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

//*******************************************************************
// 函  数：void HAL_Delay(uint32_t Delay)
// 功  能：阻塞延时，覆盖了hal库的这个函数
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void HAL_Delay(uint32_t Delay)
{
    WaitMs = Delay;
    while (WaitMs != 0)
        ;
}

//*******************************************************************
// 函  数：__IO uint32_t HAL_TickGetMs(void)
// 功  能：获取从开机到当前滴答计时器的总毫秒值
// 参  数：无
//
// 返回值：当前计时的毫秒值
// 备  注：无
//*******************************************************************
__IO uint32_t HAL_TickGetMs(void)
{
    return TimeMs;
}

//*******************************************************************
// 函  数：__IO uint32_t HAL_TickGetMsNum(void)
// 功  能：获取从开机到当前滴答计时器的总毫秒值 的个数
// 参  数：无
//
// 返回值：当前计时的毫秒值 的个数
// 备  注：无
//*******************************************************************
__IO uint32_t HAL_TickGetMsNum(void)
{
    return TimeMsNum;
}

//*******************************************************************
// 函  数：__IO uint8_t HAL_TickIsTimeOut(__IO uint32_t oldNum, __IO uint32_t oldMs, __IO uint32_t outMs)
// 功  能：查看是否延时完毕
// 参  数：oldNum   延时前的延时毫秒个数
//          oldMs   延时前的延时毫秒值
//          outMs   延时的毫秒值
// 返回值：0延时没有完成，1延时结束
// 备  注：无
//*******************************************************************
__IO uint8_t HAL_TickIsTimeOut(__IO uint32_t oldNum, __IO uint32_t oldMs, __IO uint32_t outMs)
{
    // 计数值相等的情况下
    if (oldNum == TimeMsNum)
    {
        // 新的毫秒值，减去 旧的毫秒值，大于超时的毫秒值，那么就是已经超时了
        if ((TimeMs - oldMs) > outMs)
        {
            return 1; // 已超时
        }
    }

    // 旧的计数值小于新的计数值，没有大于的情况
    if (oldNum < TimeMsNum)
    {
        // 用最大值减去旧的毫秒值再加上新的毫秒值，就是已经延时了的毫秒值，然后对比超时毫秒值，如果大于说明超时
        if (0xFFFFFFFF - oldMs + TimeMs > outMs)
        {
            return 1; // 已超时
        }
    }

    return 0; // 未超时
}

//*******************************************************************
// 函  数：void HAL_IncTick(void)
// 功  能：系统滴答定时器的中断 回调函数，覆盖了hal库这个函数
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void HAL_IncTick(void)
{
    TimeMs++;
    if (TimeMs == 0xFFFFFFFF)
    {
        TimeMsNum++;
    }

    // 用于阻塞延时
    if (WaitMs > 0)
    {
        WaitMs--;
    }
}
