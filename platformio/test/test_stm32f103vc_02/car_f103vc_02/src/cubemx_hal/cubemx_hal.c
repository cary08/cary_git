#include "cubemx_hal.h"

//*******************************************************************
// 函  数：void hal_BspInit(void)
// 功  能：调用cubemx生成的代码，进行初始化硬件
// 参  数：无
//
// 返回值：无
// 备  注：初始化所有硬件
//*******************************************************************
void hal_BspInit(void)
{
    HAL_Init(); // hal库初始化，优先级分组，开启时钟，设置swd模式 等

    SystemClock_Config(); // 系统时钟外部 8M    // 这个初始化时钟要在自己实现的滴答定时器前边

    HAL_TickInit(1000); // 系统滴答定时器 1ms

    MX_IWDG_Init(); // 初始化看门狗 1ms
    MX_DMA_Init();
    MX_GPIO_Init();        // 初始化所有的gpio
    MX_TIM2_Init();        // 初始化 右轮编码器 定时器
    MX_TIM3_Init();        // 初始化 左轮编码器 定时器
    MX_TIM5_Init();        // 初始化 超声波 定时器
    MX_TIM8_Init();        // 初始化 pwm 两个轮子的速度 和 刹车 定时器
    MX_USART1_UART_Init(); // 初始化 ros 串口
    MX_USART2_UART_Init(); // 初始化 pc 串口
    MX_USART3_UART_Init(); // 初始化 jy901s 姿态模块 串口

    // 开启 右轮 和 左轮编码器 定时器
    HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);
    HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);

    // 开启 两个轮子 pwm 速度 和 刹车 定时器
    HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_1); // 左轮 速度 pwm
    HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_2); // 右轮 速度 pwm
    HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_3); // 左轮 刹车 pwm
    HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_4); // 右轮 刹车 pwm

    // 开启 ros串口 接收和空闲中断
    __HAL_UART_ENABLE_IT(&huart1, UART_IT_RXNE);
    __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);

    // 开启 pc串口
    __HAL_UART_ENABLE_IT(&huart2, UART_IT_RXNE);
    __HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);

    // 开启 jy901s 姿态模块 串口
    HAL_UART_DMAStop(&huart3);
    __HAL_UART_ENABLE_IT(&huart3, UART_IT_RXNE);
    __HAL_UART_ENABLE_IT(&huart3, UART_IT_IDLE);
    HAL_UART_Receive_DMA(&huart3, HAL_Uart.uart_jy901s_buff, HAL_UART_BUFF_SIZE_MAX);
}

void Error_Handler(void)
{
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    __disable_irq();
    while (1)
    {
    }
    /* USER CODE END Error_Handler_Debug */
}

// hal初始化函数，需要手动实现，包含禁用 JTAG 这个很重要，一定要禁用
// 反正添加这三句就对了，不然会有问题
void HAL_MspInit(void)
{
    __HAL_RCC_AFIO_CLK_ENABLE();
    __HAL_RCC_PWR_CLK_ENABLE();
    __HAL_AFIO_REMAP_SWJ_NOJTAG();
}
