#include "app.h"

A_AppDef A_AppObj = {0};

int main(void)
{
    // 初始化 硬件驱动 和 板级支持包
    bsp_Init();

    // 应用层初始化
    app_CaryTaskInit();  // 初始化任务
    app_TimerTaskInit(); // 初始化定时器

    // 测试
    vu32 testMs = HAL_TickGetMs();
    vu32 testMsNum = HAL_TickGetMsNum();
    vu32 testWait = 1000;

    uint8_t buff[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    HAL_UART_Transmit(&huart1, buff, 10, 1000);

    printf("start\n");

    // 这个设置pwm的函数没问题
    bsp_WheelSetDirectionPwm(1000, 1000);

    // 主循环
    while (1)
    {
        // 业务代码

        // 执行就绪的任务
        app_CaryTaskRun();

        // 执行定时器任 务
        app_TimerTaskRun();

        // 下边是测试，先不删

        if (HAL_TickIsTimeOut(testMsNum, testMs, testWait) == B_TRUE)
        {
            testMs = HAL_TickGetMs();
            testMsNum = HAL_TickGetMsNum();

            // 超声波
            // printf("ult1:%d,_2_:%d,_3_:%d,_4_:%d,_5_:%d,_6_:%d,_7_:%d,_8_:%d,\n",
            //        (int)(B_UltraObj.Distance1), (int)(B_UltraObj.Distance2), (int)(B_UltraObj.Distance3), (int)(B_UltraObj.Distance4),
            //        (int)(B_UltraObj.Distance5), (int)(B_UltraObj.Distance6), (int)(B_UltraObj.Distance7), (int)(B_UltraObj.Distance8));

            // 地检 红外
            // printf("Infra1:%d,_2_:%d,_3_:%d,_4_:%d\n", B_InfraredObj.En1, B_InfraredObj.En2, B_InfraredObj.En3, B_InfraredObj.En4);

            // 读取编码器值
            // printf("encoder_right:%ld\n", __HAL_TIM_GET_COUNTER(&htim2));
            // printf("encoder_left:%ld\n", __HAL_TIM_GET_COUNTER(&htim3));
            // __HAL_TIM_SET_COUNTER(&htim2, 0);
            // __HAL_TIM_SET_COUNTER(&htim3, 0);
        }

        // HAL_Delay(1000);

        // 设置pwm 和 读取编码器值
        // 正方向 传入带符号的 pwm值
        // bsp_WheelSetDirectionPwm(1000, 1000);

        // if (HAL_TickIsTimeOut(testMsNum, testMs, testWait) == B_TRUE)
        // {
        //     // printf("Wait1000____\n");

        //     // printf("encode_right:%d\n", B_WheelObj.RightEncodeVal);
        //     // printf("encode_left:%d\n", B_WheelObj.LeftEncodeVal);
        //     printf("encoder_right:%ld\n", __HAL_TIM_GET_COUNTER(&htim2));
        //     printf("encoder_left:%ld\n", __HAL_TIM_GET_COUNTER(&htim3));
        //     __HAL_TIM_SET_COUNTER(&htim2, 0);
        //     __HAL_TIM_SET_COUNTER(&htim3, 0);
        //     // bsp_WheelEncoderRead(B_ENCODER_RIGHT);
        //     // bsp_WheelEncoderRead(B_ENCODER_LEFT);
        //     testMs = HAL_TickGetMs();
        //     testMsNum = HAL_TickGetMsNum();
        // }

        // 超声波 OK
        // bsp_UltraQueryDistance();
        // // printf("Ult_1:%dmm\n", B_UltraObj.Distance1);
        // if (HAL_TickIsTimeOut(testMsNum, testMs, testWait) == B_TRUE)
        // {
        //     testMs = HAL_TickGetMs();
        //     testMsNum = HAL_TickGetMsNum();

        //     printf("Ult_1:%d, 2:%d mm\n", B_UltraObj.Distance1, B_UltraObj.Distance2);
        // }

        // 获取jy901s 数据 没问题

        // 到此，cubemx_hal 中的硬件驱动没问题了。

        // 解析jy901s 数据 没问题
        // bsp_JY901SRecv();
        // if (HAL_TickIsTimeOut(testMsNum, testMs, testWait) == B_TRUE)
        // {
        //     testMs = HAL_TickGetMs();
        //     testMsNum = HAL_TickGetMsNum();
        //     printf("Acc_Ori_x:%hd\n", B_JY901SObj.AccelOriX);
        //     printf("Acc_Ori_y:%hd\n", B_JY901SObj.AccelOriY);
        //     printf("Acc_Ori_z:%hd\n", B_JY901SObj.AccelOriZ);

        //     printf("Gyro_Ori_x:%hd\n", B_JY901SObj.GyroOriX);
        //     printf("Gyro_Ori_y:%hd\n", B_JY901SObj.GyroOriY);
        //     printf("Gyro_Ori_z:%hd\n", B_JY901SObj.GyroOriZ);

        //     printf("Roll:%.2f\n", B_JY901SObj.Roll);
        //     printf("Pitch:%.2f\n", B_JY901SObj.Pitch);
        //     printf("Yaw:%.2f\n", B_JY901SObj.Yaw);
        // }

        // 喂狗
        bsp_IWDGFeel();
    }
}
