#ifndef _CARYTASK_H_
#define _CARYTASK_H_
#include "../app.h"

// ros存储的最大数据行数
#define A_CARY_TASK_MAX 100

// 任务中的状态
enum A_CARY_TASK_STATE_ENUM
{
    A_CARY_TASK_INIT = 0, // 任务处理的初始态
    A_CARY_TASK_START,    // 任务处理的起始态
    A_CARY_TASK_WAIT,     // 任务处理的等待态
    A_CARY_TASK_WAIT_END, // 任务处理的结束等待态
    A_CARY_TASK_STOP,     // 任务处理的结束态
};

// ros 的任务优先级，一个任务对应一个优先级 值越小，优先级越高
typedef enum _A_CARY_TASK_PRIORITY
{
    // 0优先级保留。

    // 测试任务，从50开始
    A_CARY_TASK_ODOM_NAV_PRI = 50, // 手动导航，直行里程的任务优先级
    A_CARY_TASK_ROTATE_NAV_PRI,    // 手动导航，旋转角度的任务优先级

    // 最小优先级，空闲优先级，一直为就绪状态，一直查看是否有任务就绪
    A_CARY_TASK_IDLE_PRI = 99,
} A_CARY_TASK_PRIORITY;

// 任务相关的全局变量
typedef struct A_CaryTaskStr
{
    // 最多100个任务，下标0列的值为0表示此任务没有就绪，1为就绪，
    // 下标1列，表示此任务的状态
    vu8 ReadyTable[100][2];
    vu8 run; // 当前在运行的任务，是ReadyTable行下标的值

} A_CaryTaskDef;

extern A_CaryTaskDef A_CaryTaskObj;

void app_CaryTaskInit(void);
void app_CaryTaskRun(void);

void app_CaryTaskIDLE(void);
void app_CaryTaskSetRun(void);
vu8 app_CaryTaskMutuallyReady(vu32 pri);
void app_CaryTaskStop(A_CARY_TASK_PRIORITY Pri);

#endif
