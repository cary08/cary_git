#include "rotateNav.h"

A_RotateNavDef A_RotateNavObj = {0};

//*******************************************************************
// 函  数：void app_CaryTaskRotateNav(void)
// 功  能：手动导航，旋转 任务
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_CaryTaskRotateNav(void)
{

    switch (A_RotateNavObj.State)
    {
    case A_CARY_TASK_INIT:
    {
        app_CaryTaskRotateNavInit();
        break;
    }

    case A_CARY_TASK_START:
    {
        app_CaryTaskRotateNavStart();
        break;
    }

    case A_CARY_TASK_STOP:
    {
        app_CaryTaskRotateNavStop();
        break;
    }
    }
}

//*******************************************************************
// 函  数：void app_CaryTaskRotateNavInit(void)
// 功  能：手动导航，旋转角度的初始态，初始化变量
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_CaryTaskRotateNavInit(void)
{
    A_CaryTaskObj.ReadyTable[A_CARY_TASK_ODOM_NAV_PRI][1] = A_CARY_TASK_INIT;

    A_RotateNavObj.State = A_CARY_TASK_START;

    A_RotateNavObj.NewAngle = B_NULL;
}

//*******************************************************************
// 函  数：void app_CaryTaskRotateNavStart(void)
// 功  能：手动导航，直行里程的结束态，清空所有的标志位
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_CaryTaskRotateNavStart(void)
{

    if (A_RotateNavObj.AngleDirection > 0)
    {
        A_RotateNavObj.NewAngle = (B_JY901SObj.Yaw - A_RotateNavObj.OldAngle) < -10 ? B_JY901SObj.Yaw - A_RotateNavObj.OldAngle + 360 : B_JY901SObj.Yaw - A_RotateNavObj.OldAngle;
        if (A_RotateNavObj.NewAngle >= A_RotateNavObj.SetAngle)
        {
            // 小车停止
            B_WheelObj.SetLeftSpeed = B_NULL;
            B_WheelObj.SetRightSpeed = B_NULL;

            // 结束此任务
            A_RotateNavObj.State = A_CARY_TASK_STOP;

            printf("rotate_stop_newAngle:%.2f,setAngle:%.2f,yaw:%.2f,oldAngle:%.2f......\n",
                   A_RotateNavObj.NewAngle, A_RotateNavObj.SetAngle, B_JY901SObj.Yaw, A_RotateNavObj.OldAngle);
            return;
        }
    }

    if (A_RotateNavObj.AngleDirection < 0)
    {
        A_RotateNavObj.NewAngle = (B_JY901SObj.Yaw - A_RotateNavObj.OldAngle) > 10 ? B_JY901SObj.Yaw - A_RotateNavObj.OldAngle - 360 : B_JY901SObj.Yaw - A_RotateNavObj.OldAngle;
        if (A_RotateNavObj.NewAngle <= A_RotateNavObj.SetAngle)
        {
            // 小车停止
            B_WheelObj.SetLeftSpeed = B_NULL;
            B_WheelObj.SetRightSpeed = B_NULL;

            // 结束此任务
            A_RotateNavObj.State = A_CARY_TASK_STOP;

            printf("rotate_stop_newAngle:%.2f,setAngle:%.2f,yaw:%.2f,oldAngle:%.2f......\n",
                   A_RotateNavObj.NewAngle, A_RotateNavObj.SetAngle, B_JY901SObj.Yaw, A_RotateNavObj.OldAngle);
            return;
        }
    }

    // 开启延时定时器
    app_TimerTaskWait(A_TIMER_TASK_USART_RECV_PC_ROTATE, A_TIMER_TASK_USART_RECV_PC_ROTATE_MS, A_CARY_TASK_ROTATE_NAV_PRI);
}

//*******************************************************************
// 函  数：void app_CaryTaskRotateNavStop(void)
// 功  能：手动导航，直行里程的结束态，清空所有的标志位
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_CaryTaskRotateNavStop(void)
{
    A_RotateNavObj.State = B_NULL; // 任务的状态标志位

    A_RotateNavObj.SetAngle = B_NULL;
    A_RotateNavObj.NewAngle = B_NULL;
    A_RotateNavObj.OldAngle = B_NULL;
    A_RotateNavObj.AngleDirection = B_NULL;

    // 任务数减一，并且将状态置为 初始态
    app_CaryTaskStop(A_CARY_TASK_ROTATE_NAV_PRI);
}
