#include "odomNav.h"

A_OdomNavDef A_OdomNavObj = {0};

//*******************************************************************
// 函  数：void app_CaryTaskOdomNav(void)
// 功  能：手动导航，直行里程 任务
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_CaryTaskOdomNav(void)
{

    switch (A_OdomNavObj.State)
    {
    case A_CARY_TASK_INIT:
    {
        app_CaryTaskOdomNavInit();
        break;
    }

    case A_CARY_TASK_START:
    {
        app_CaryTaskOdomNavStart();
        break;
    }

    case A_CARY_TASK_STOP:
    {
        app_CaryTaskOdomNavStop();
        break;
    }
    }
}

//*******************************************************************
// 函  数：void app_CaryTaskOdomNavInit(void)
// 功  能：手动导航，直行里程的初始态，初始化变量
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_CaryTaskOdomNavInit(void)
{
    A_CaryTaskObj.ReadyTable[A_CARY_TASK_ODOM_NAV_PRI][1] = A_CARY_TASK_INIT;

    A_OdomNavObj.State = A_CARY_TASK_START;

    // 清空此次里程计算
    A_OdomNavObj.DistanceEncode = B_NULL;
}

//*******************************************************************
// 函  数：void app_CaryTaskOdomNavStart(void)
// 功  能：手动导航，直行里程的结束态，清空所有的标志位
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_CaryTaskOdomNavStart(void)
{

    // 计算当前走了多少编码器值，
    A_OdomNavObj.DistanceEncode = (abs(B_WheelObj.LeftEncodeAllVal) + abs(B_WheelObj.RightEncodeAllVal)) / 2;

    // 当路程走到大于等于 设置的路程时 停止行走
    if (A_OdomNavObj.DistanceEncode >= A_OdomNavObj.SetDistanceEncode)
    {
        // 小车停止
        B_WheelObj.SetLeftSpeed = B_NULL;
        B_WheelObj.SetRightSpeed = B_NULL;

        // 结束此任务
        A_OdomNavObj.State = A_CARY_TASK_STOP;

        return;
    }

    // 开启延时定时器
    app_TimerTaskWait(A_TIMER_TASK_USART_RECV_PC_ODOM, A_TIMER_TASK_USART_RECV_PC_ODOM_MS, A_CARY_TASK_ODOM_NAV_PRI);
}

//*******************************************************************
// 函  数：void app_CaryTaskOdomNavStop(void)
// 功  能：手动导航，直行里程的结束态，清空所有的标志位
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_CaryTaskOdomNavStop(void)
{
    A_OdomNavObj.State = B_NULL; // 任务的状态标志位

    // 清空此次里程计算
    A_OdomNavObj.DistanceEncode = B_NULL;

    // 清空此次设置的里程值
    A_OdomNavObj.SetDistanceEncode = B_NULL;
    printf("odom_stop......\n");
    // 任务数减一，并且将状态置为 初始态
    app_CaryTaskStop(A_CARY_TASK_ODOM_NAV_PRI);
}
