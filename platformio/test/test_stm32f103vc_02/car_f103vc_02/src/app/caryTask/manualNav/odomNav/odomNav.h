#ifndef _ODOM_NAV_H_
#define _ODOM_NAV_H_
#include "../../../app.h"

typedef struct A_OdomNavStr
{
    vu8 State; // 任务的状态标

    // 手动导航的 直行里程 距离 米
    float SetDistanceEncode;
    float DistanceEncode;

} A_OdomNavDef;

extern A_OdomNavDef A_OdomNavObj;

void app_CaryTaskOdomNav(void);
void app_CaryTaskOdomNavInit(void);
void app_CaryTaskOdomNavStart(void);
void app_CaryTaskOdomNavStop(void);

#endif
