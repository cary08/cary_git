#ifndef _ROTATE_NAV_H_
#define _ROTATE_NAV_H_
#include "../../../app.h"

typedef struct A_RotateNavStr
{
    vu8 State; // 任务的状态标

    float SetAngle;       // 设置的角度值
    float NewAngle;       // 当前位姿模块的角度值
    float OldAngle;       // 旋转前的位姿模块的角度值
    float AngleDirection; // 旋转方向

} A_RotateNavDef;

extern A_RotateNavDef A_RotateNavObj;

void app_CaryTaskRotateNav(void);
void app_CaryTaskRotateNavInit(void);
void app_CaryTaskRotateNavStart(void);
void app_CaryTaskRotateNavStop(void);

#endif
