#include "idle.h"










//*******************************************************************
// 函  数：void app_CaryTaskIdle(void)
// 功  能：空闲处理任务，一直在查看是否有就绪的任务，使其置为运行态
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_CaryTaskIdle(void)
{
    static vu8 flag = A_CARY_TASK_INIT;

    switch (flag)
    {
    case A_CARY_TASK_INIT:
    {
        // 一直在查看是否有就绪任务，使其置为运行态
        app_CaryTaskSetRun();
        break;
    }
    }
}









