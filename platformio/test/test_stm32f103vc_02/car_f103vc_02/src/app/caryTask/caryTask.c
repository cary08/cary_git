#include "caryTask.h"

A_CaryTaskDef A_CaryTaskObj = {0};

//*******************************************************************
// 函  数：void app_CaryTaskInit(void)
// 功  能：执行 运行态的 ros任务
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_CaryTaskInit(void)
{
    // 就绪 空闲 任务
    A_CaryTaskObj.ReadyTable[A_CARY_TASK_IDLE_PRI][0]++;
    A_CaryTaskObj.ReadyTable[A_CARY_TASK_IDLE_PRI][1] = A_CARY_TASK_INIT;
    A_CaryTaskObj.run = A_CARY_TASK_IDLE_PRI;
}

//*******************************************************************
// 函  数：void app_CaryTaskRun(void)
// 功  能：执行 运行态的 ros任务
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_CaryTaskRun(void)
{

    switch (A_CaryTaskObj.run) // 当前该运行的优先级
    {

    case A_CARY_TASK_ODOM_NAV_PRI: // 50 手动导航，直行里程任务优先级
    {
        app_CaryTaskOdomNav();
        break;
    }

    case A_CARY_TASK_ROTATE_NAV_PRI: // 50 手动导航，旋转角度任务优先级
    {
        app_CaryTaskRotateNav();
        break;
    }

    case A_CARY_TASK_IDLE_PRI: // 99 最小优先级，空闲优先级，一直为就绪状态，一直查看是否有任务就绪
    {
        app_CaryTaskIdle();
        break;
    }
    }
}

//*******************************************************************
// 函  数：void app_CaryTaskSetRun(void)
// 功  能：将ros的优先级最高的已就绪的任务置为运行态
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_CaryTaskSetRun(void)
{
    vu32 i = 0;

    for (i = 0; i < A_CARY_TASK_MAX; i++)
    {
        // 执行下一个任务的条件，任务数必须大于0，并且任务处于初始态也就是可执行态，这个态什么时候才会是呢？
        // 当第一次发来时；或者延时中，触发事件后要执行此任务了，就置位初始态；或延时后就是超时了，置位初始态
        if (A_CaryTaskObj.ReadyTable[i][0] > B_NULL &&
            (A_CaryTaskObj.ReadyTable[i][1] == A_CARY_TASK_INIT || A_CaryTaskObj.ReadyTable[i][1] == A_CARY_TASK_WAIT_END))
        {
            // 然后再判断，将i传入，查看是否与当前延时的任务有冲突，如果有冲突那么跳过此任务不去执行，直到找到与当前延时任务不冲突的任务去执行
            if (!app_CaryTaskMutuallyReady(i))
            {
                // 表示没有互斥任务，将运行当前任务
                A_CaryTaskObj.run = i;
            }
            // 表示有与当前任务互斥的任务，不运行此任务，继续寻找需要运行的任务
            return;
        }
    }
}

//*******************************************************************
// 函  数：void app_CaryTaskMutuallyReady(vu32 pri)
// 功  能：互斥就绪任务，查看传入的要运行的任务，如果当前有互斥的任务在延时，那么此任务不可就绪
// 参  数：pri  任务列表下标，每个下标代表一个任务
//
// 返回值：0为没有互斥任务，1为有互斥任务
// 备  注：无
//*******************************************************************
vu8 app_CaryTaskMutuallyReady(vu32 pri)
{
    switch (pri)
    {

    case A_CARY_TASK_ODOM_NAV_PRI: // 50    手动导航，直行里程
    {
        if (A_CaryTaskObj.ReadyTable[A_CARY_TASK_ROTATE_NAV_PRI][0] > B_NULL)
        {
            return B_TRUE;
        }
        break;
    }

    case A_CARY_TASK_ROTATE_NAV_PRI: // 51    手动导航，旋转
    {
        if (A_CaryTaskObj.ReadyTable[A_CARY_TASK_ODOM_NAV_PRI][0] > B_NULL)
        {
            return B_TRUE;
        }
        break;
    }

    case A_CARY_TASK_IDLE_PRI: // 99    空闲任务
    {
        // 没有互斥任务
        break;
    }
    }

    return 0;
}

//*******************************************************************
// 函  数：void app_CaryTaskStop(A_CARY_TASK_PRIORITY Pri)
// 功  能：任务结束调用的函数，功能是任务数减一，然后执行当前就绪的任务
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_CaryTaskStop(A_CARY_TASK_PRIORITY Pri)
{
    if (A_CaryTaskObj.ReadyTable[Pri][0] > B_NULL)
    {
        A_CaryTaskObj.ReadyTable[Pri][0]--;
        A_CaryTaskObj.ReadyTable[Pri][1] = A_CARY_TASK_INIT;
        app_CaryTaskSetRun();
    }
}
