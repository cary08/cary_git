#ifndef _MEMORYTASK_H_
#define _MEMORYTASK_H_
#include "../app.h"

#define A_MEMORY_SIZE_MAX 1024 // 每个缓存区都是固定最大值

typedef enum _A_MEMORY_TASK_ENUM
{
    pass, // 占位
    // 暂时用不到了，不需要接收到数据放到缓存区，接收到数据直接处理了
    // A_MEMORY_RECV_ROS_INDEX = 0, // 接收ros数据的缓存区数组下标
    // A_MEMORY_RECV_PC_TEST_INDEX, // 接收PC测试数据的缓存区数组下标，用于手动导航
} A_MEMORY_TASK_ENUM;

typedef struct _A_MemoryTaskStr
{
    vu8 Queue[1024]; // 每个缓存区固定为1k
    vu32 TopIndex;
    vu32 EndIndex;
} A_MemoryTaskDef;

// 最多5个缓存区 那就是5K了
extern A_MemoryTaskDef A_MemoryObj[5];

void app_MemorySaveDataToQueue(A_MEMORY_TASK_ENUM memoryIndex, vu8 *data, vu32 len);
void app_MemoryQueueToBuf(A_MEMORY_TASK_ENUM memoryIndex, vu8 *data);

#endif
