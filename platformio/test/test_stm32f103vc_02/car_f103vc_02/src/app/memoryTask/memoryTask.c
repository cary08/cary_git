#include "memoryTask.h"

// 存储数据的总队列
A_MemoryTaskDef A_MemoryObj[5] = {0};

//*******************************************************************
// 函  数：void app_MemorySaveDataToQueue(A_MEMORY_TASK_ENUM memoryIndex, vu8 *data, vu32 len)
// 功  能：将接收到的协议数据，存储到缓存区中
// 参  数：memoryIndex  代表的是哪个缓存区
//        data  要存储的协议数据
// 		  len   要存储的协议数据的长度
// 返回值：无
// 备  注：无
//*******************************************************************
void app_MemorySaveDataToQueue(A_MEMORY_TASK_ENUM memoryIndex, vu8 *data, vu32 len)
{
    vu32 newTopIndex = (A_MemoryObj[memoryIndex].EndIndex + 1) % A_MEMORY_SIZE_MAX; // 添加此数据的第一个位的下标

    int i = 0;
    for (i = 0; i < len; i++)
    {
        A_MemoryObj[memoryIndex].Queue[(newTopIndex + i) % A_MEMORY_SIZE_MAX] = data[i];
    }

    A_MemoryObj[memoryIndex].TopIndex = newTopIndex;
    A_MemoryObj[memoryIndex].EndIndex = (newTopIndex + i - 1) % A_MEMORY_SIZE_MAX;
}

//*******************************************************************
// 函  数：void app_MemoryQueueToBuf(A_MEMORY_TASK_ENUM memoryIndex, vu8 *data)
// 功  能：将缓存区中的命令放入buf中
// 参  数：memoryIndex  哪个缓存区
//        data          要将数据保存在的buf
// 返回值：无
// 备  注：无
//*******************************************************************
void app_MemoryQueueToBuf(A_MEMORY_TASK_ENUM memoryIndex, vu8 *data)
{
    int topIndex = A_MemoryObj[memoryIndex].TopIndex;
    int endIndex = A_MemoryObj[memoryIndex].EndIndex;

    int len = endIndex - topIndex + 1;
    len = len < 0 ? (endIndex + A_MEMORY_SIZE_MAX) - topIndex + 1 : len;

    int i = 0;
    for (i = 0; i < len; i++)
    {
        data[i] = A_MemoryObj[memoryIndex].Queue[(topIndex + i) % A_MEMORY_SIZE_MAX];
    }
}
