#ifndef _APP_H_
#define _APP_H_
#include "../bsp/bsp.h"

#include "stm32f1xx_it.h"
#include "appTool/appTool.h"
#include "caryTask/caryTask.h"
#include "caryTask/caryTaskTool.h"
#include "caryTask/idle/idle.h"
#include "caryTask/manualNav/odomNav/odomNav.h"
#include "caryTask/manualNav/rotateNav/rotateNav.h"
#include "memoryTask/memoryTask.h"
#include "timerTask/timerTask.h"
#include "timerTask/timerTaskTool.h"
#include "timerTask/recvPc/recvPc.h"
#include "timerTask/recvPc/recvPcHandle.h"
#include "timerTask/recvRos/recvRos.h"
#include "timerTask/recvRos/recvRosHandle.h"
#include "timerTask/recvRos/recvRosTool.h"

typedef struct A_AppStr
{
    vu8 UltrasonicJustFlag; // ǰ������״̬
    vu8 UltrasonicBackFlag; // ������״̬
    vu8 Infra1Flag;         // ����1״̬
    vu8 Infra2Flag;         // ����2״̬
    vu8 Infra3Flag;         // ����3״̬
    vu8 OnLineFlag;         // 0Ϊδ���ӣ�1Ϊ������
    int testPrintfWheel;    // �����ã���ʱɾ

} A_AppDef;

extern A_AppDef A_AppObj;

#endif
