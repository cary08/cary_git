#include "timerTask.h"

A_TimerTaskDef A_TimerTaskObj[100] = {0};

//*******************************************************************
// 函  数：void app_TimerTaskInit(void)
// 功  能：初始化延时变量
// 参  数：无
//
// 返回值：无
// 备  注：这个初始化一般是在死循环前边最后一个进行调用初始化
//*******************************************************************
void app_TimerTaskInit(void)
{
    // 开启接收上位机指令定时器
    app_TimerTaskWait(A_TIMER_TASK_USART_RECV_ROS_COMMAND, A_TIMER_TASK_USART_RECV_ROS_COMMAND_MS, B_NULL);
    // 开启接收IMU数据定时器
    app_TimerTaskWait(A_TIMER_TASK_USART_RECV_IMU_JY901S, A_TIMER_TASK_USART_RECV_IMU_JY901S_MS, B_NULL);
    // 开启接收pc测试数据定时器
    app_TimerTaskWait(A_TIMER_TASK_USART_RECV_PC_TEST, A_TIMER_TASK_USART_RECV_PC_TEST_MS, B_NULL);

    // 开启连接定时器
    app_TimerTaskWait(A_TIMER_TASK_CONNECT, A_TIMER_TASK_CONNECT_MS, B_NULL);
    // 开启轮子定时器
    app_TimerTaskWait(A_TIMER_TASK_WHEEL, A_TIMER_TASK_WHEEL_MS, B_NULL);
    // 避障定时器
    app_TimerTaskWait(A_TIMER_TASK_OBSTACLE, A_TIMER_TASK_OBSTACLE_MS, B_NULL);
}

//*******************************************************************
// 函  数：void app_TimerTaskRun(void)
// 功  能：里面都是定时要执行的操作
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskRun(void)
{
    app_TimerTaskOnLine();       // 连接
    app_TimerTaskUsartRecvRos(); // 串口上位机
    app_TimerTaskUsartIMU();     // 串口imu
    app_TimerTaskUsartRecvPc();  // 串口pc测试
    app_TimerTaskWheel();        // 轮子
    app_TimerTaskObstacle();     // 避障

    app_TimerTaskPcOdom();   // 手动导航，直行里程 计算
    app_TimerTaskPcRotate(); // 手动导航，旋转角度 计算
}

//*******************************************************************
// 函  数：void app_TimerTaskOnLine(void)
// 功  能：是否上下线的定时器
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskOnLine(void)
{
    // 延时结束表示上位机掉线
    if (app_TimerTaskIsDelayEnd(A_TIMER_TASK_CONNECT) == B_TRUE)
    {
        // 掉线了
        A_AppObj.OnLineFlag = B_FALSE;
    }
    else
    {
        // 没掉线
        A_AppObj.OnLineFlag = B_TRUE;
    }
}

//*******************************************************************
// 函  数：void app_TimerTaskUsartIMU(void)
// 功  能：接收imu数据定时器
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskUsartIMU(void)
{
    if (app_TimerTaskIsDelayEnd(A_TIMER_TASK_USART_RECV_IMU_JY901S) == B_TRUE)
    {
        bsp_JY901SRecv();
        // 继续延时
        app_TimerTaskWait(A_TIMER_TASK_USART_RECV_IMU_JY901S, A_TIMER_TASK_USART_RECV_IMU_JY901S_MS, B_NULL);
    }
}

//*******************************************************************
// 函  数：void app_TimerTaskObstacle(void)
// 功  能：避障定时器
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskObstacle(void)
{
    if (app_TimerTaskIsDelayEnd(A_TIMER_TASK_OBSTACLE) == B_TRUE)
    {
        // 查看超声波
        bsp_UltraQueryDistance();

        // 查看红外
        bsp_ReadInfrared();

        // 避障 写个函数，检测是否要刹车
        app_TimerTaskToolObstacleFlag();

        // printf("obstacle.....\n");

        // 继续开启延时
        app_TimerTaskWait(A_TIMER_TASK_OBSTACLE, A_TIMER_TASK_OBSTACLE_MS, B_NULL);
    }
}

//*******************************************************************
// 函  数：vu8 app_TimerTaskIsDelayEnd(A_TIMER_TASK_ENUM StateIndex)
// 功  能：查看定时器是否延时结束
// 参  数：StateIndex 定时器类型的结构体数组的下标，每个下标都对应一个任务定时器
//
// 返回值：定时器开启并延时完毕 或 定时器没有开启 返回1，延时开启，并且在延时中返回0
// 备  注：无
//*******************************************************************
vu8 app_TimerTaskIsDelayEnd(A_TIMER_TASK_ENUM StateIndex)
{
    if (A_TimerTaskObj[StateIndex].State == A_TIMER_READY_STATE &&
        HAL_TickIsTimeOut(A_TimerTaskObj[StateIndex].TimeOutMsNum,
                          A_TimerTaskObj[StateIndex].TimeOutMs,
                          A_TimerTaskObj[StateIndex].WaitTimeMs) == B_TRUE)

    {
        // 延时结束
        return B_TRUE;
    }
    else
    {
        // 延时开启，并且在延时中
        return B_FALSE;
    }
}

//*******************************************************************
// 函  数：void app_TimerTaskWait(A_TIMER_TASK_ENUM StateIndex,vu32 WaitTimeMs,vu8 TaskPriority)
// 功  能：任务中的延时函数，取消了任务中的延时态，直接调用此函数就行
// 参  数：StateIndex 定时器类型的结构体数组的下标，每个下标都对应一个任务定时器
// 	      WaitTimeMs 延时的时长
//        TaskPriority  任务优先级，每个任务对应一个优先级，也就是具体的某个任务，没有0值的优先级
//          如果是0，那么说明：外边调用时，不想置位延时态，并且外边已经置位了其它相应的状态，所以这里就不置态
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskWait(A_TIMER_TASK_ENUM StateIndex, vu32 WaitTimeMs, vu8 TaskPriority)
{
    // 外边调用时，不想置位延时态，并且外边已经置位了其它相应的状态，所以这里就不置态
    if (TaskPriority != B_NULL)
    {
        A_CaryTaskObj.ReadyTable[TaskPriority][1] = A_CARY_TASK_WAIT;
        app_CaryTaskSetRun();
    }

    A_TimerTaskObj[StateIndex].State = A_TIMER_READY_STATE;
    A_TimerTaskObj[StateIndex].TimeOutMs = HAL_TickGetMs();
    A_TimerTaskObj[StateIndex].TimeOutMsNum = HAL_TickGetMsNum();
    A_TimerTaskObj[StateIndex].WaitTimeMs = WaitTimeMs;
}

//*******************************************************************
// 函  数：void app_TimerTaskWaitCancel(A_TIMER_TASK_ENUM StateIndex,vu8 TaskPriority)
// 功  能：取消延时函数
// 参  数：StateIndex 定时器类型的结构体数组的下标，每个下标都对应一个任务定时器
// 	      WaitTimeMs 延时的时长
//        TaskPriority  任务优先级，每个任务对应一个优先级，也就是具体的某个任务
//          如果是0，那么说明：外边调用时，不想置位延时态，并且外边已经置位了其它相应的状态，所以这里就不置态
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskWaitCancel(A_TIMER_TASK_ENUM StateIndex, vu8 TaskPriority)
{
    // 外边调用时，不想就绪此任务，并且外边已经置位了其它相应的状态，所以这里就不置态
    if (TaskPriority != B_NULL)
    {
        A_CaryTaskObj.ReadyTable[TaskPriority][1] = A_CARY_TASK_WAIT_END; // 就绪   任务
    }
    A_TimerTaskObj[StateIndex].State = A_TIMER_PENDING_STATE; // 关闭此定时器
    A_TimerTaskObj[StateIndex].TimeOutMsNum = B_NULL;
    A_TimerTaskObj[StateIndex].TimeOutMs = B_NULL;
    A_TimerTaskObj[StateIndex].WaitTimeMs = B_NULL;
}
