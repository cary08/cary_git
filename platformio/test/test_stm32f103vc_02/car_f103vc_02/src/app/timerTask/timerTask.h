#ifndef _TIMERTASK_H_
#define _TIMERTASK_H_
#include "../app.h"

// 定时器任务
typedef enum _A_TIMER_TASK_ENUM
{

    // 串口接收上位机命令的定时器任务
    A_TIMER_TASK_USART_RECV_ROS_COMMAND = 0,

    // 串口imu数据接收定时器
    A_TIMER_TASK_USART_RECV_IMU_JY901S,

    // 串口接收pc测试数据，手动导航
    A_TIMER_TASK_USART_RECV_PC_TEST,
    A_TIMER_TASK_USART_RECV_PC_ODOM,   // 直行里程操作的定时器
    A_TIMER_TASK_USART_RECV_PC_ROTATE, // 旋转角度操作的定时器

    // 轮子速度控制定时器
    A_TIMER_TASK_WHEEL,

    // 连接定时器
    A_TIMER_TASK_CONNECT,

    // 避障定时器
    A_TIMER_TASK_OBSTACLE,

    // ----------------------------------------
    // 任务中的延时任务

    // ----------------------------------------
    // 定时器任务的延时间隔时间
    A_TIMER_TASK_USART_RECV_ROS_COMMAND_MS = 0, // 接收上位机命令的延时时间

    A_TIMER_TASK_USART_RECV_IMU_JY901S_MS = 0, // 接收IMU数据的延时时间

    A_TIMER_TASK_USART_RECV_PC_TEST_MS = 0,    // 接收PC测试数据的延时时间
    A_TIMER_TASK_USART_RECV_PC_ODOM_MS = 10,   // 直行里程的延时时间
    A_TIMER_TASK_USART_RECV_PC_ROTATE_MS = 10, // 旋转角度的延时时间

    A_TIMER_TASK_WHEEL_MS = 100, // 发送和获取轮子数据的延时时间

    A_TIMER_TASK_CONNECT_MS = 1000, // 连接的延时时间

    A_TIMER_TASK_OBSTACLE_MS = 0, // 避障的检测延时时间

} A_TIMER_TASK_ENUM;

// 定时器状态
enum A_TIMER_STATE_ENUM
{
    // 定时器挂起
    A_TIMER_PENDING_STATE = 0,

    // 定时器运行
    A_TIMER_READY_STATE,
};

typedef struct A_TimerTaskStr
{
    vu8 State;         // 定时器任务状态
    vu32 WaitTimeMs;   // 定时器任务延时时长
    vu32 TimeOutMs;    // 定时器任务延时开始ms
    vu32 TimeOutMsNum; // 定时器任务延时开始ms计数
} A_TimerTaskDef;

// 最多100个定时器
extern A_TimerTaskDef A_TimerTaskObj[100];

void app_TimerTaskOnLine(void);
void app_TimerTaskUsartIMU(void);
void app_TimerTaskWheel(void);
void app_TimerTaskObstacle(void);

void app_TimerTaskInit(void);
void app_TimerTaskRun(void);
vu8 app_TimerTaskIsDelayEnd(A_TIMER_TASK_ENUM StateIndex);
void app_TimerTaskWait(A_TIMER_TASK_ENUM StateIndex, vu32 WaitTimeMs, vu8 TaskPriority);
void app_TimerTaskWaitCancel(A_TIMER_TASK_ENUM StateIndex, vu8 TaskPriority);

#endif
