#ifndef _TIMER_TASK_TOOL_H_
#define _TIMER_TASK_TOOL_H_
#include "../app.h"

void app_TimerTaskToolObstacleFlag(void);
vu8 app_TimerRecvCRC8MAXIM(vu8 *data, vu32 len);

#endif
