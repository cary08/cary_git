#ifndef _RECV_PC_H_
#define _RECV_PC_H_
#include "../../app.h"

#define A_PC_PROTOCOL_TOP 0X5A // 协议头
#define A_PC_PROTOCOL_ID 0X00  // 协议ID
#define A_PC_BACK_0X00 0X00    // 回复的固定值0x00
#define A_PC_BACK_0X01 0X01    // 回复的固定值0x01
#define A_PC_BACK_0X02 0X02    // 回复的固定值0x02

// 协议数据的下标值
enum _A_PC_PROTOCOL_INDEX_ENUM
{
    A_PC_TOP_INDEX = 0,  // 协议头的下标值
    A_PC_SIZE_INDEX = 1, // 协议长度的下标值
    A_PC_ID_INDEX = 2,   // 协议id的下标值
    A_PC_CMD_INDEX = 3,  // 协议命令的下标值
    A_PC_DATA_INDEX = 4, // 协议第一个数据位的下标值
};

typedef enum _A_PC_PROTOCOL_SIZE_ENUM
{
    // 协议长度的最大值
    A_PC_MAX_SIZE = 100,

    // 除了数据位的协议长度，为固定值 6
    A_PC_NO_DATA_SIZE = 6,

    // 底盘的命令的协议总长度
    A_PC_DOWN_ODOM_SIZE = 14, // pc下发 		直行里程		协议总长度
    A_PC_UP_ODOM_SIZE = 7,    // 回复pc 		直行里程		协议总长度

    A_PC_DOWN_ROTATE_ANGLE_SIZE = 14, // pc下发 		旋转角度		协议总长度
    A_PC_UP_ROTATE_ANGLE_SIZE = 7,    // 回复pc 		旋转角度		协议总长度

} A_PC_PROTOCOL_SIZE_ENUM;

// 手动导航 的命令码
typedef enum _A_PC_CMD
{

    A_PC_DOWN_ODOM_CMD = 0X01, // 0X01 1		pc下发 		直行里程 			指令
    A_PC_UP_ODOM_CMD = 0X2,    // 0X02 2		回复pc 	    直行里程 	        指令

    A_PC_DOWN_ROTATE_ANGLE_CMD = 0X03, // 0X03 3		pc下发 		旋转角度 			指令
    A_PC_UP_ROTATE_ANGLE_CMD = 0X04,   // 0X04 4		回复pc 	    旋转角度 	        指令

} A_PC_CMD;

// 接收的数据
typedef struct _A_PcRecv
{
    vu8 buff[200];
    vu32 buffSize;
} A_PcRecvDef;

extern A_PcRecvDef A_PcRecvObj;

void app_TimerTaskUsartRecvPc(void);
void app_TimerTaskRecvPcDataHandle(void);
void app_TimerTaskRecvPcDataControl(vu8 cmd);

void app_TimerTaskPcOdom(void);
void app_TimerTaskPcRotate(void);

#endif
