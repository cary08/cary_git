#include "recvPc.h"

A_PcRecvDef A_PcRecvObj = {0};

//*******************************************************************
// 函  数：void app_TimerTaskUsartRecvPc(void)
// 功  能：串口接收pc发来的测试数据
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskUsartRecvPc(void)
{
    // 无间隔时间，一直接收上位机发来的数据
    if (app_TimerTaskIsDelayEnd(A_TIMER_TASK_USART_RECV_PC_TEST) == B_TRUE)
    {
        // 接收到上位机数据进行校验和处理
        app_TimerTaskRecvPcDataHandle();
        // 继续开启定时器
        app_TimerTaskWait(A_TIMER_TASK_USART_RECV_PC_TEST, A_TIMER_TASK_USART_RECV_PC_TEST_MS, B_NULL);
    }
}

//*******************************************************************
// 函  数：void app_TimerTaskRecvPcDataHandle(void)
// 功  能：接收pc发来的测试命令，进行校验处理
// 参  数：无
//
// 返回值：无
// 备  注：将接收到的数据放入队列中
//*******************************************************************
void app_TimerTaskRecvPcDataHandle(void)
{

    // ----------------------- 新的 解析接收到的协议，这种方式是用的空闲中断，然后判断空闲标志位，解析并处理数据

    // 帧头   帧长度   ID(为下位机编号，为级联设计预留，暂时固定1)   功能码   数据   预留位   CRC校验

    // 一帧数据接收完毕
    if (HAL_Uart.uart_pc_idle_flag == B_TRUE)
    {

        // 将这一帧的各条数据解析出来
        int index = 0;
        int state = 0;
        int size = 0;
        for (index = 0; index < HAL_Uart.uart_pc_buff_len; index++)
        {
            switch (state)
            {
            case 0: // 处理协议的第一位，帧头
            {
                if (HAL_Uart.uart_pc_buff[index] == 0x5A)
                {
                    A_PcRecvObj.buff[A_PcRecvObj.buffSize] = 0x5A;
                    A_PcRecvObj.buffSize++;
                    state = 1;
                    break;
                }
                // 如果第一个不是帧头，不做处理，继续判断下一个
                break;
            }
            case 1: // 处理协议的第二位，帧长度
            {
                // 这一步先接收，因为还无法进行校验，只有拿到了命令码才能进行校验
                A_PcRecvObj.buff[A_PcRecvObj.buffSize] = HAL_Uart.uart_pc_buff[index];
                A_PcRecvObj.buffSize++;
                state = 2;
                break;
            }
            case 2: // 处理协议的第三位，id,默认为0
            {
                // 直接接收就好了，没什么
                A_PcRecvObj.buff[A_PcRecvObj.buffSize] = HAL_Uart.uart_pc_buff[index];
                A_PcRecvObj.buffSize++;
                state = 3;
                break;
            }
            case 3: // 处理协议的第四位，命令码
            {
                // 校验命令码 和 校验协议总长度 -2为上边接收到的协议总长度
                if (bsp_PcRecvCheckCmd(HAL_Uart.uart_pc_buff[index], A_PcRecvObj.buff[A_PcRecvObj.buffSize - 2]) == B_TRUE)
                {
                    // 协议总长度 校验成功了
                    size = A_PcRecvObj.buff[A_PcRecvObj.buffSize - 2];

                    A_PcRecvObj.buff[A_PcRecvObj.buffSize] = HAL_Uart.uart_pc_buff[index];
                    A_PcRecvObj.buffSize++;

                    // 如果长度等于没有数据的长度，直接跳过接收数据位
                    if (A_PcRecvObj.buff[A_PcRecvObj.buffSize - 3] == A_PC_NO_DATA_SIZE)
                    {
                        state = 5;
                        break;
                    }

                    // 去接收数据位
                    state = 4;
                    break;
                }
                else
                {
                    // 错误,去置为初始态，重新从头校验
                    memset((uint8_t *)A_PcRecvObj.buff, 0, sizeof(A_PcRecvObj.buff));
                    A_PcRecvObj.buffSize = 0;
                    size = 0;
                    state = 0;
                    break;
                }
                break;
            }
            case 4: // 处理协议的第五位数据位开始位 至 数据位最后一位，数据位
            {

                // 协议最后两个是 预留位 和 校验位
                if (A_PcRecvObj.buffSize < (size - 2))
                {

                    A_PcRecvObj.buff[A_PcRecvObj.buffSize] = HAL_Uart.uart_pc_buff[index];
                    A_PcRecvObj.buffSize++;
                    if (A_PcRecvObj.buffSize >= (size - 2))
                    {

                        // 数据位赋值完毕
                        state = 5;
                        break;
                    }
                }
                break;
            }
            case 5: // 处理 协议的倒数第二位，预留位
            {
                // 没有操作，直接赋值
                A_PcRecvObj.buff[A_PcRecvObj.buffSize] = HAL_Uart.uart_pc_buff[index];
                A_PcRecvObj.buffSize++;
                state = 6;
                break;
            }
            case 6: // 处理 协议的最后一位，校验位
            {
                // 先赋值，再去校验
                A_PcRecvObj.buff[A_PcRecvObj.buffSize] = HAL_Uart.uart_pc_buff[index];

                // 最后一位校验，FF不需要校验
                if ((A_PcRecvObj.buff[A_PcRecvObj.buffSize] == 0xFF) || (app_TimerRecvCRC8MAXIM(A_PcRecvObj.buff, A_PcRecvObj.buffSize) == A_PcRecvObj.buff[A_PcRecvObj.buffSize]))
                {
                    printf("check_ok_\n");
                    // 校验成功 进行处理数据
                    app_TimerTaskRecvPcDataControl(A_PcRecvObj.buff[A_PC_CMD_INDEX]);
                }

                // 初始化变量 校验下一条数据
                memset((uint8_t *)A_PcRecvObj.buff, 0, sizeof(A_PcRecvObj.buff));
                A_PcRecvObj.buffSize = 0;
                size = 0;
                state = 0;
                break;
            }
            } // switch
        }     // for

        // 读取完数据，然后将其清空，等待下一次数据的到来
        memset(HAL_Uart.uart_pc_buff, 0, sizeof(HAL_Uart.uart_pc_buff));
        HAL_Uart.uart_pc_buff_len = B_NULL;
        HAL_Uart.uart_pc_idle_flag = B_NULL; // 这个是接收到数据状态，必须手动清零
    }
}

//*******************************************************************
// 函  数：void app_TimerTaskRecvPcDataControl(vu8 cmd)
// 功  能：将接收到ros下发的正确的数据 进行分配 就绪该任务
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskRecvPcDataControl(vu8 cmd)
{

    switch (cmd)
    {

    case A_PC_DOWN_ODOM_CMD: // 0x01  1   ros下发 直行里程 命令
    {
        app_PcHandleOdom();
        break;
    }

    case A_PC_DOWN_ROTATE_ANGLE_CMD: // 0x03  3   ros下发 旋转角度 命令
    {
        app_PcHandleRotateAngle();
        break;
    }
    }
}

//*******************************************************************
// 函  数：void app_TimerTaskPcOdom(void)
// 功  能：手动导航直行里程计，行驶多少米 任务的定时器
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskPcOdom(void)
{
    if (app_TimerTaskIsDelayEnd(A_TIMER_TASK_USART_RECV_PC_ODOM) == B_TRUE)
    {
        // 取消此任务定时器，回到任务
        app_TimerTaskWaitCancel(A_TIMER_TASK_USART_RECV_PC_ODOM, A_CARY_TASK_ODOM_NAV_PRI);
    }
}

//*******************************************************************
// 函  数：void app_TimerTaskPcRotate(void)
// 功  能：手动导航旋转角度，旋转多少度 任务的定时器
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskPcRotate(void)
{
    if (app_TimerTaskIsDelayEnd(A_TIMER_TASK_USART_RECV_PC_ROTATE) == B_TRUE)
    {
        // 取消此任务定时器，回到任务
        app_TimerTaskWaitCancel(A_TIMER_TASK_USART_RECV_PC_ROTATE, A_CARY_TASK_ROTATE_NAV_PRI);
    }
}
