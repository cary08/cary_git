#include "recvPcHandle.h"

//*******************************************************************
// 函  数：void app_PcHandleOdom(void)
// 功  能：处理接收到的直行里程命令
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_PcHandleOdom(void)
{
    // 查看直行里程任务是否开启，如果开启，那么就去关闭
    if (A_CaryTaskObj.ReadyTable[A_CARY_TASK_ODOM_NAV_PRI][0] > 0)
    {
        app_CaryTaskOdomNavStop();
    }

    // 查看旋转角度任务是否开启，如果开启，那么就去关闭
    if (A_CaryTaskObj.ReadyTable[A_CARY_TASK_ROTATE_NAV_PRI][0] > 0)
    {
        app_CaryTaskRotateNavStop();
    }

    // 拿到运动值，然后计算
    vu8 xLineTemp[4] = {A_PcRecvObj.buff[7], A_PcRecvObj.buff[6], A_PcRecvObj.buff[5], A_PcRecvObj.buff[4]};
    vu8 Distance1[4] = {A_PcRecvObj.buff[11], A_PcRecvObj.buff[10], A_PcRecvObj.buff[9], A_PcRecvObj.buff[8]};

    int xLine1 = *(int *)xLineTemp;    // x 线速度
    int Distance2 = *(int *)Distance1; // 距离米

    // 真实的 线速度 角速度
    float xLine = (float)xLine1 / 1000.0;
    float Distance3 = (float)Distance2 / 1000.0;

    // 左轮速度 = 线速度 - 角速度 * 轮间距 / 2
    // 右轮速度 = 线速度 + 角速度 * 轮间距 / 2
    float leftSpeed = xLine - 0 * B_WHEEL_SPACING / 2;
    float rightSpeed = xLine + 0 * B_WHEEL_SPACING / 2;

    // 设置速度
    B_WheelObj.SetLeftSpeed = leftSpeed;
    B_WheelObj.SetRightSpeed = rightSpeed;

    // 计算出编码器到达的距离目标的数值，当总编码器达到此值时停止运动 ，距离米 除以 轮子周长 乘以 一圈编码器值 就是此距离的编码器值
    A_OdomNavObj.SetDistanceEncode = ((abs(B_WheelObj.LeftEncodeAllVal) + abs(B_WheelObj.RightEncodeAllVal)) / 2) + (Distance3 / B_WHEEL_PERIMETER * B_WHEEL_ENCODE_VAL);

    printf("_Nav_cmd01_leftSpeed:%.2f,rightSpeed:%.2f,SetDistanceEncode:%.2f \n", leftSpeed, rightSpeed, A_OdomNavObj.SetDistanceEncode);

    // 开启 直行里程 任务
    A_CaryTaskObj.ReadyTable[A_CARY_TASK_ODOM_NAV_PRI][0]++;
}

//*******************************************************************
// 函  数：void app_PcHandleRotateAngle(void)
// 功  能：处理接收到的旋转角度命令
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_PcHandleRotateAngle(void)
{
    // 查看旋转角度任务是否开启，如果开启，那么就去关闭
    if (A_CaryTaskObj.ReadyTable[A_CARY_TASK_ROTATE_NAV_PRI][0] > 0)
    {
        app_CaryTaskRotateNavStop();
    }

    // 查看直行里程任务是否开启，如果开启，那么就去关闭
    if (A_CaryTaskObj.ReadyTable[A_CARY_TASK_ODOM_NAV_PRI][0] > 0)
    {
        app_CaryTaskOdomNavStop();
    }

    // 拿到运动值，然后计算
    vu8 zLineTemp[4] = {A_PcRecvObj.buff[7], A_PcRecvObj.buff[6], A_PcRecvObj.buff[5], A_PcRecvObj.buff[4]};
    vu8 angle1[4] = {A_PcRecvObj.buff[11], A_PcRecvObj.buff[10], A_PcRecvObj.buff[9], A_PcRecvObj.buff[8]};

    int zLine1 = *(int *)zLineTemp; // z 角速度
    int angle2 = *(int *)angle1;    // 角度

    // 真实的 线速度 角速度
    float zLine = (float)zLine1 / 1000.0;
    float angle3 = (float)angle2 / 1000.0;

    // 左轮速度 = 线速度 - 角速度 * 轮间距 / 2
    // 右轮速度 = 线速度 + 角速度 * 轮间距 / 2
    float leftSpeed = 0 - zLine * B_WHEEL_SPACING / 2;
    float rightSpeed = 0 + zLine * B_WHEEL_SPACING / 2;

    // 设置速度
    B_WheelObj.SetLeftSpeed = leftSpeed;
    B_WheelObj.SetRightSpeed = rightSpeed;

    // 设置角度前还要看左转还是右转
    A_RotateNavObj.AngleDirection = zLine > 0 ? 1.0 : -1.0;
    A_RotateNavObj.SetAngle = angle3 * A_RotateNavObj.AngleDirection; // 设置的角度值
    A_RotateNavObj.OldAngle = B_JY901SObj.Yaw;                        // 旋转前的角度值

    printf("_Nav_cmd03_leftSpeed:%.2f,rightSpeed:%.2f,Direction:%.2f,setAngle:%.2f,oldAngle:%.2f \n",
           leftSpeed, rightSpeed, A_RotateNavObj.AngleDirection, A_RotateNavObj.SetAngle, A_RotateNavObj.OldAngle);

    // 开启 旋转 任务
    A_CaryTaskObj.ReadyTable[A_CARY_TASK_ROTATE_NAV_PRI][0]++;
}

//*******************************************************************
// 函  数：vu8 bsp_PcRecvCheckCmd(A_PC_CMD cmd, vu8 size)
// 功  能：校验命令码 和 协议总长度
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
vu8 bsp_PcRecvCheckCmd(vu32 cmd, vu8 size)
{
    switch (cmd)
    {
    case A_PC_DOWN_ODOM_CMD: // 下发里程
    {
        if (size == A_PC_DOWN_ODOM_SIZE)
        {
            // 并且长度值也一致 才能返回正确
            return B_TRUE;
        }
        break;
    }
    case A_PC_DOWN_ROTATE_ANGLE_CMD: // 下发旋转角度
    {
        if (size == A_PC_DOWN_ROTATE_ANGLE_SIZE)
        {
            // 并且长度值也一致 才能返回正确
            return B_TRUE;
        }
        break;
    }
    }

    return B_FALSE;
}
