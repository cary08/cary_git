#ifndef _RECV_PC_HANDLE_H_
#define _RECV_PC_HANDLE_H_
#include "../../app.h"

void app_PcHandleOdom(void);
void app_PcHandleRotateAngle(void);

vu8 bsp_PcRecvCheckCmd(vu32 cmd, vu8 size);

#endif
