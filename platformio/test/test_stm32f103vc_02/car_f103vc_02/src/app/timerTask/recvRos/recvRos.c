#include "recvRos.h"

//*******************************************************************
// 函  数：void app_TimerTaskUsartRecvRos(void)
// 功  能：串口接收上位机发来的数据
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskUsartRecvRos(void)
{
    // 无间隔时间，一直接收上位机发来的数据
    if (app_TimerTaskIsDelayEnd(A_TIMER_TASK_USART_RECV_ROS_COMMAND) == B_TRUE)
    {
        // 接收到上位机数据进行校验和处理
        app_TimerTaskRecvRosDataHandle();
        // 继续开启定时器
        app_TimerTaskWait(A_TIMER_TASK_USART_RECV_ROS_COMMAND, A_TIMER_TASK_USART_RECV_ROS_COMMAND_MS, B_NULL);
    }
}

//*******************************************************************
// 函  数：void app_TimerTaskRecvRosDataHandle(void)
// 功  能：接收上位机发来的命令，进行校验处理
// 参  数：无
//
// 返回值：无
// 备  注：将接收到的数据放入队列中
//*******************************************************************
void app_TimerTaskRecvRosDataHandle(void)
{
    // ----------------------- 新的 解析接收到的协议，这种方式是用的空闲中断，然后判断空闲标志位，解析并处理数据

    // 帧头   帧长度   ID(为下位机编号，为级联设计预留，暂时固定1)   功能码   数据   预留位   CRC校验

    // 一帧数据接收完毕
    if (HAL_Uart.uart_ros_idle_flag == B_TRUE)
    {

        // 将这一帧的各条数据解析出来
        int index = 0;
        int state = 0;
        int size = 0;
        for (index = 0; index < HAL_Uart.uart_ros_buff_len; index++)
        {
            switch (state)
            {
            case 0: // 处理协议的第一位，帧头
            {
                if (HAL_Uart.uart_ros_buff[index] == 0x5A)
                {

                    A_PcRecvObj.buff[A_PcRecvObj.buffSize] = 0x5A;
                    A_PcRecvObj.buffSize++;
                    state = 1;
                    break;
                }
                // 如果第一个不是帧头，不做处理，继续判断下一个
                break;
            }
            case 1: // 处理协议的第二位，帧长度
            {
                // 这一步先接收，因为还无法进行校验，只有拿到了命令码才能进行校验
                A_PcRecvObj.buff[A_PcRecvObj.buffSize] = HAL_Uart.uart_ros_buff[index];
                A_PcRecvObj.buffSize++;
                state = 2;
                break;
            }
            case 2: // 处理协议的第三位，id,默认为0
            {

                // 直接接收就好了，没什么
                A_PcRecvObj.buff[A_PcRecvObj.buffSize] = HAL_Uart.uart_ros_buff[index];
                A_PcRecvObj.buffSize++;
                state = 3;
                break;
            }
            case 3: // 处理协议的第四位，命令码
            {

                // 校验命令码 和 校验协议总长度 -2为上边接收到的协议总长度
                if (bsp_RosRecvCheckCmd(HAL_Uart.uart_ros_buff[index], A_PcRecvObj.buff[A_PcRecvObj.buffSize - 2]) == B_TRUE)
                {

                    // 长度校验已经通过
                    size = A_PcRecvObj.buff[A_PcRecvObj.buffSize - 2];

                    A_PcRecvObj.buff[A_PcRecvObj.buffSize] = HAL_Uart.uart_ros_buff[index];
                    A_PcRecvObj.buffSize++;

                    // 如果长度等于没有数据的长度，直接跳过接收数据位
                    if (A_PcRecvObj.buff[A_PcRecvObj.buffSize - 3] == A_PC_NO_DATA_SIZE)
                    {

                        state = 5;
                        break;
                    }

                    // 去接收数据位
                    state = 4;
                    break;
                }
                else
                {
                    // 错误,去置为初始态，重新从头校验
                    memset((uint8_t *)A_PcRecvObj.buff, 0, sizeof(A_PcRecvObj.buff));
                    A_PcRecvObj.buffSize = 0;
                    size = 0;
                    state = 0;
                    break;
                }
                break;
            }
            case 4: // 处理协议的第五位数据位开始位 至 数据位最后一位，数据位
            {

                // 协议最后两个是 预留位 和 校验位
                if (A_PcRecvObj.buffSize < (size - 2))
                {
                    A_PcRecvObj.buff[A_PcRecvObj.buffSize] = HAL_Uart.uart_ros_buff[index];
                    A_PcRecvObj.buffSize++;
                    if (A_PcRecvObj.buffSize >= (size - 2))
                    {
                        // 数据位赋值完毕
                        state = 5;
                        break;
                    }
                }
                break;
            }
            case 5: // 处理 协议的倒数第二位，预留位
            {

                // 没有操作，直接赋值
                A_PcRecvObj.buff[A_PcRecvObj.buffSize] = HAL_Uart.uart_ros_buff[index];
                A_PcRecvObj.buffSize++;
                state = 6;
                break;
            }
            case 6: // 处理 协议的最后一位，校验位
            {
                // 先将最后一位赋值到数组中
                A_PcRecvObj.buff[A_PcRecvObj.buffSize] = HAL_Uart.uart_ros_buff[index];

                // 最后一位校验，FF不需要校验
                if ((A_PcRecvObj.buff[A_PcRecvObj.buffSize] == 0xFF) || (app_TimerRecvCRC8MAXIM(A_PcRecvObj.buff, A_PcRecvObj.buffSize) == A_PcRecvObj.buff[A_PcRecvObj.buffSize]))
                {
                    // printf("check_ok_cmd:%d\n", A_PcRecvObj.buff[A_PC_CMD_INDEX]);
                    // 校验成功 进行处理数据
                    app_TimerTaskRecvRosDataControl(A_PcRecvObj.buff[A_PC_CMD_INDEX]);
                }

                // 初始化变量 校验下一条数据
                memset((uint8_t *)A_PcRecvObj.buff, 0, sizeof(A_PcRecvObj.buff));
                A_PcRecvObj.buffSize = 0;
                size = 0;
                state = 0;
                break;
            }
            } // switch
        }     // for

        // 读取完数据，然后将其清空，等待下一次数据的到来
        memset(HAL_Uart.uart_ros_buff, 0, sizeof(HAL_Uart.uart_ros_buff));
        HAL_Uart.uart_ros_buff_len = B_NULL;
        HAL_Uart.uart_ros_idle_flag = B_NULL; // 这个是接收到数据状态，必须手动清零
    }
}

//*******************************************************************
// 函  数：void app_TimerTaskRecvRosDataControl(vu8 cmd)
// 功  能：将接收到ros下发的正确的数据 进行分配 就绪该任务
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskRecvRosDataControl(vu8 cmd)
{
    // 接收到正确的数据钩子函数
    app_TimerTaskRecvRosDataHook();

    switch (cmd)
    {

    case A_ROS_DOWN_WHEEL_MOTION_CMD: // 0x01  1   ros下发 轮子运动 命令
    {
        app_RosHandleMotion();
        break;
    }

    case A_ROS_DOWN_QUERY_SPEED_CMD: // 0x03  3   ros下发 查询速度 命令
    {
        app_RosHandleQuerySpeed();
        break;
    }

    case A_ROS_DOWN_QUERY_IMU_CMD: // 0x05  5   ros下发 查询IMU 命令
    {
        app_RosHandleQueryIMU();
        break;
    }

    case A_ROS_DOWN_QUERY_BATTERY_CMD: // 0x07  7   ros下发 查询电池 命令
    {
        app_RosHandleQueryBattery();
        break;
    }

    case A_ROS_DOWN_QUERY_ODOM_CMD: // 0x09  9   ros下发 查询里程计 命令
    {
        app_RosHandleQueryOdom();
        break;
    }

    case A_ROS_DOWN_QUERY_IMU2_CMD: // 0x13  19   ros下发 查询IMU2原始数据 命令
    {
        app_RosHandleQueryIMU2();
        break;
    }

    case A_ROS_DOWN_QUERY_ULTRA_CMD: // 0xF5  245   ros下发 查询超声波数据 命令
    {
        app_RosHandleQueryUltra();
        break;
    }

    case A_ROS_DOWN_QUERY_INFRA_CMD: // 0xF7  247   ros下发 查询红外数据 命令
    {
        app_RosHandleQueryInfra();
        break;
    }

    case A_ROS_DOWN_RESTART_CMD: // 0xFD  253   ros下发 重启 命令
    {
        app_RosHandleRestart();
        break;
    }
    }
}

//*******************************************************************
// 函  数：void app_TimerTaskRecvRosDataHook(vu8 cmd)
// 功  能：上线的定时器，使其一直在延时，不能停止延时，否则视为下线。
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskRecvRosDataHook(void)
{
    // 开启 连接定时器 上线延时，1s
    app_TimerTaskWait(A_TIMER_TASK_CONNECT, A_TIMER_TASK_CONNECT_MS, B_NULL);
}

//*******************************************************************
// 函  数：void app_TimerRecvRosSend(A_ROS_PROTOCOL_SIZE_ENUM Size, A_ROS_CMD Cmd, vu8 *Data)
// 功  能：向ros发送协议
// 参  数：Size		协议长度
// 		  Cmd       协议的命令码
//        Data    协议的数据值
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerRecvRosSend(A_ROS_PROTOCOL_SIZE_ENUM Size, A_ROS_CMD Cmd, vu8 *Data)
{
    vu8 send[A_ROS_MAX_SIZE] = {0};

    send[A_ROS_TOP_INDEX] = A_ROS_PROTOCOL_TOP;
    send[A_ROS_SIZE_INDEX] = Size;
    send[A_ROS_ID_INDEX] = A_ROS_PROTOCOL_ID;
    send[A_ROS_CMD_INDEX] = Cmd;

    if (Data != B_NULL)
    {
        int i = 0;
        for (i = 0; i < Size - A_ROS_NO_DATA_SIZE; i++)
        {
            send[A_ROS_DATA_INDEX + i] = Data[i];
        }
    }

    send[Size - 1] = app_TimerRecvCRC8MAXIM(send, Size - 1);

    HAL_UART_Transmit(&huart1, (uint8_t *)send, Size, 1000);
}
