#include "recvRosTool.h"

//*******************************************************************
// 函  数：vu8 bsp_RosRecvCheckCmd(A_PC_CMD cmd, vu8 size)
// 功  能：校验命令码 和 协议总长度
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
vu8 bsp_RosRecvCheckCmd(vu8 cmd, vu8 size)
{

    switch (cmd)
    {
    case A_ROS_DOWN_WHEEL_MOTION_CMD: // 0X01 1		ros下发 		轮子舵机运动 			指令
    {
        if (size == A_ROS_DOWN_WHEEL_MOTION_SIZE)
        {
            // 并且长度值也一致 才能返回正确
            return B_TRUE;
        }
        break;
    }
    case A_ROS_DOWN_QUERY_SPEED_CMD: // 0X03 3		ros下发 		查询速度 			指令
    {
        if (size == A_ROS_DOWN_QUERY_SPEED_SIZE)
        {
            // 并且长度值也一致 才能返回正确
            return B_TRUE;
        }
        break;
    }

    case A_ROS_DOWN_QUERY_IMU_CMD: // 0X05 5		ros下发 		查询IMU 			指令
    {
        if (size == A_ROS_DOWN_QUERY_IMU_SIZE)
        {
            // 并且长度值也一致 才能返回正确
            return B_TRUE;
        }
        break;
    }

    case A_ROS_DOWN_QUERY_BATTERY_CMD: // 0X07 7		ros下发 		查询电池 			指令
    {
        if (size == A_ROS_DOWN_QUERY_BATTERY_SIZE)
        {
            // 并且长度值也一致 才能返回正确
            return B_TRUE;
        }
        break;
    }

    case A_ROS_DOWN_QUERY_ODOM_CMD: // 0X09 9		ros下发 		查询里程计 			指令
    {
        if (size == A_ROS_DOWN_QUERY_ODOM_SIZE)
        {
            // 并且长度值也一致 才能返回正确
            return B_TRUE;
        }
        break;
    }

    case A_ROS_DOWN_QUERY_IMU2_CMD: // 0X13 19	ros下发 		查询IMU2原始数据 			指令
    {
        if (size == A_ROS_DOWN_QUERY_IMU2_SIZE)
        {
            // 并且长度值也一致 才能返回正确
            return B_TRUE;
        }
        break;
    }

    case A_ROS_DOWN_QUERY_ULTRA_CMD: // 0XF5 245	ros下发 		查询超声波距离 			指令
    {
        if (size == A_ROS_DOWN_QUERY_ULTRA_SIZE)
        {
            // 并且长度值也一致 才能返回正确
            return B_TRUE;
        }
        break;
    }

    case A_ROS_DOWN_QUERY_INFRA_CMD: // 0XF7 247	ros下发 		查询红外数据 			指令
    {
        if (size == A_ROS_DOWN_QUERY_INFRA_SIZE)
        {
            // 并且长度值也一致 才能返回正确
            return B_TRUE;
        }
        break;
    }

    case A_ROS_DOWN_RESTART_CMD: // 0XFD 253	ros下发 		查询红外数据 			指令
    {
        if (size == A_ROS_DOWN_RESTART_SIZE)
        {
            // 并且长度值也一致 才能返回正确
            return B_TRUE;
        }
        break;
    }
    }

    return B_FALSE;
}
