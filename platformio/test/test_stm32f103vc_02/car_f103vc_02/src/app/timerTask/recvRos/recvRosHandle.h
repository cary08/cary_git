#ifndef _RECV_ROS_HANDLE_H_
#define _RECV_ROS_HANDLE_H_
#include "../../app.h"

void app_RosHandleMotion(void);
void app_RosHandleQuerySpeed(void);
void app_RosHandleQueryIMU(void);
void app_RosHandleQueryBattery(void);
void app_RosHandleQueryOdom(void);
void app_RosHandleQueryIMU2(void);
void app_RosHandleQueryUltra(void);
void app_RosHandleQueryInfra(void);
void app_RosHandleRestart(void);

#endif
