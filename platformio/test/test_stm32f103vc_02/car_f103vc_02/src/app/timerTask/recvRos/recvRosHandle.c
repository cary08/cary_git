#include "recvRosHandle.h"

//*******************************************************************
// 函  数：void app_RosHandleMotion(void)
// 功  能：处理接收到的运动命令
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_RosHandleMotion(void)
{
    vu8 data[A_ROS_UP_WHEEL_MOTION_SIZE - A_ROS_NO_DATA_SIZE] = {0};

    // 拿到运动值，然后计算
    vu8 xLineTemp[4] = {A_PcRecvObj.buff[7], A_PcRecvObj.buff[6], A_PcRecvObj.buff[5], A_PcRecvObj.buff[4]};
    // vu8 yLineTemp[4] = {A_PcRecvObj.buff[11], A_PcRecvObj.buff[10], A_PcRecvObj.buff[9], A_PcRecvObj.buff[8]};
    vu8 zAngularTemp[4] = {A_PcRecvObj.buff[15], A_PcRecvObj.buff[14], A_PcRecvObj.buff[13], A_PcRecvObj.buff[12]};

    int xLine1 = *(int *)xLineTemp;       // x 线速度
    int zAngular1 = *(int *)zAngularTemp; // z 角速度

    // 真实的 线速度 角速度
    float xLine = (float)xLine1 / 1000.0;
    float zAngular = (float)zAngular1 / 1000.0;

    // 左轮速度 = 线速度 - 角速度 * 轮间距 / 2
    // 右轮速度 = 线速度 + 角速度 * 轮间距 / 2
    float leftSpeed = xLine - zAngular * B_WHEEL_SPACING / 2;
    float rightSpeed = xLine + zAngular * B_WHEEL_SPACING / 2;

    leftSpeed = leftSpeed;
    rightSpeed = rightSpeed;

    printf("_cmd01_down_SpeedX:%.2f,SpeedZ:%.2f \n", xLine, zAngular);

    // 设置速度
    B_WheelObj.SetLeftSpeed = leftSpeed;
    B_WheelObj.SetRightSpeed = rightSpeed;

    // 查看是否要回复上位机 只要有任意障碍，就要回复
    if (A_AppObj.UltrasonicJustFlag != B_FALSE || A_AppObj.UltrasonicBackFlag != B_FALSE ||
        A_AppObj.Infra1Flag != B_FALSE || A_AppObj.Infra2Flag != B_FALSE || A_AppObj.Infra3Flag != B_FALSE)
    {

        // 超声波检测到障碍物
        if (A_AppObj.UltrasonicJustFlag != B_FALSE || A_AppObj.UltrasonicBackFlag != B_FALSE)
        {
            data[0] = 1;
        }

        // 红外检测到障碍物
        if (A_AppObj.Infra1Flag != B_FALSE || A_AppObj.Infra2Flag != B_FALSE || A_AppObj.Infra3Flag != B_FALSE)
        {
            data[0] = 2;
        }

        printf("cmd0x02_data[4]:%d\n", data[0]);

        // 设置速度失败 才回复
        app_TimerRecvRosSend(A_ROS_UP_WHEEL_MOTION_SIZE, A_ROS_UP_WHEEL_MOTION_CMD, data);
    }
}

//*******************************************************************
// 函  数：void app_RosHandleQuerySpeed(void)
// 功  能：处理接收到的查询速度命令
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_RosHandleQuerySpeed(void)
{
    // 向上位机回复
    vu8 data[A_ROS_UP_QUERY_SPEED_SIZE - A_ROS_NO_DATA_SIZE] = {0};

    vu32 speedX = (vu32)((int)(B_WheelObj.SpeedX * 1000));
    vu32 speedZ = (vu32)((int)(B_WheelObj.SpeedZ * 1000));

    data[0] = speedX >> 24; // x轴线速度 先高后低
    data[1] = speedX >> 16;
    data[2] = speedX >> 8;
    data[3] = speedX;

    data[4] = 0; // y轴线速度 先高后低
    data[5] = 0;
    data[6] = 0;
    data[7] = 0;

    data[8] = speedZ >> 24; // z轴角速度 先高后低
    data[9] = speedZ >> 16;
    data[10] = speedZ >> 8;
    data[11] = 0;

    data[12] = B_UltraObj.Distance1 >> 8; // 超声波1 先高后低
    data[13] = B_UltraObj.Distance1;
    data[14] = B_UltraObj.Distance2 >> 8; // 超声波2 先高后低
    data[15] = B_UltraObj.Distance2;

    data[16] = B_InfraredObj.En1; // 红外1
    data[17] = B_InfraredObj.En2; // 红外2
    data[18] = B_InfraredObj.En3; // 红外3
    data[19] = B_InfraredObj.En4; // 红外4

    app_TimerRecvRosSend(A_ROS_UP_QUERY_SPEED_SIZE, A_ROS_UP_QUERY_SPEED_CMD, data);
}

//*******************************************************************
// 函  数：void app_RosHandleQueryIMU(void)
// 功  能：处理接收到的查询imu命令
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_RosHandleQueryIMU(void)
{
    // 向上位机回复
    vu8 data[A_ROS_UP_QUERY_IMU_SIZE - A_ROS_NO_DATA_SIZE] = {0};

    // 获取当前的 翻滚角 俯仰角 偏航角
    vu16 roll = (vu16)((short)(B_JY901SObj.Roll * 1000));
    vu16 pitch = (vu16)((short)(B_JY901SObj.Pitch * 1000));
    vu16 yaw = (vu16)((short)(B_JY901SObj.Yaw * 1000));

    data[0] = roll >> 8; // 翻滚角 先高后低
    data[1] = roll;

    data[2] = pitch >> 8; // 俯仰角 先高后低
    data[3] = pitch;

    data[4] = yaw >> 8; // 偏航角 先高后低
    data[5] = yaw;

    app_TimerRecvRosSend(A_ROS_UP_QUERY_IMU_SIZE, A_ROS_UP_QUERY_IMU_CMD, data);
}

//*******************************************************************
// 函  数：void app_RosHandleQueryBattery(void)
// 功  能：处理接收到的查询电池命令
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_RosHandleQueryBattery(void)
{
    // 向上位机回复
    vu8 data[A_ROS_UP_QUERY_BATTERY_SIZE - A_ROS_NO_DATA_SIZE] = {0};

    // 获取 电池信息

    data[0] = 0; // 电压 先高后低
    data[1] = 0;

    data[2] = 0; // 电流 先高后低
    data[3] = 0;

    app_TimerRecvRosSend(A_ROS_UP_QUERY_BATTERY_SIZE, A_ROS_UP_QUERY_BATTERY_CMD, data);
}

//*******************************************************************
// 函  数：void app_RosHandleQueryOdom(void)
// 功  能：处理接收到的查询odom命令
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_RosHandleQueryOdom(void)
{
    // 向上位机回复
    vu8 data[A_ROS_UP_QUERY_ODOM_SIZE - A_ROS_NO_DATA_SIZE] = {0};

    // 获取 里程计 信息

    // 偏航角 方向角 单位弧度
    // vu32 yawRad   = (vu32)( (int)((B_JY901SObj.Yaw * B_JY901S_1ANG_TO_RAD) * 100) );
    vu32 yawRad = (vu32)((int)(B_JY901SObj.Yaw * 100.0));
    data[0] = yawRad >> 24;
    data[1] = yawRad >> 16;
    data[2] = yawRad >> 8;
    data[3] = yawRad;

    // 线速度 角速度
    vu32 speedx = (vu32)((int)(B_WheelObj.SpeedX * 1000.0));
    vu32 speedz = (vu32)((int)(B_WheelObj.SpeedZ * 1000.0));
    data[4] = speedx >> 24;
    data[5] = speedx >> 16;
    data[6] = speedx >> 8;
    data[7] = speedx;

    data[8] = speedz >> 24; // 角速度
    data[9] = speedz >> 16;
    data[10] = speedz >> 8;
    data[11] = speedz;

    printf("_cmd09_up_Yaw:%.2f,leftSpeed:%.2f,rightSpeed:%.2f,speedX:%.2f,speedZ:%.2f\n",
           B_JY901SObj.Yaw, B_WheelObj.LeftSpeed, B_WheelObj.RightSpeed,
           B_WheelObj.SpeedX, B_WheelObj.SpeedZ);

    data[12] = B_UltraObj.Distance1 >> 8; // 超声波1 先高后低
    data[13] = B_UltraObj.Distance1;
    data[14] = B_UltraObj.Distance2 >> 8; // 超声波2 先高后低
    data[15] = B_UltraObj.Distance2;

    data[16] = B_InfraredObj.En1; // 红外1
    data[17] = B_InfraredObj.En2; // 红外2
    data[18] = B_InfraredObj.En3; // 红外3
    data[19] = B_InfraredObj.En4; // 红外4

    app_TimerRecvRosSend(A_ROS_UP_QUERY_ODOM_SIZE, A_ROS_UP_QUERY_ODOM_CMD, data);
}

//*******************************************************************
// 函  数：void app_RosHandleQueryIMU2(void)
// 功  能：处理接收到的查询imu2命令
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_RosHandleQueryIMU2(void)
{
    // 向上位机回复
    vu8 data[A_ROS_UP_QUERY_IMU2_SIZE - A_ROS_NO_DATA_SIZE] = {0};

    // 获取 IMU2原始数据 信息
    vu32 gyrox = (vu32)((int)B_JY901SObj.GyroOriX * 100000);
    vu32 gyroy = (vu32)((int)B_JY901SObj.GyroOriY * 100000);
    vu32 gyroz = (vu32)((int)B_JY901SObj.GyroOriZ * 100000);
    vu32 accelx = (vu32)((int)B_JY901SObj.AccelOriX * 100000);
    vu32 accely = (vu32)((int)B_JY901SObj.AccelOriY * 100000);
    vu32 accelz = (vu32)((int)B_JY901SObj.AccelOriZ * 100000);
    vu16 q0 = (vu16)((short)(B_JY901SObj.QuaternionicQ0 * 10000.0));
    vu16 q1 = (vu16)((short)(B_JY901SObj.QuaternionicQ1 * 10000.0));
    vu16 q2 = (vu16)((short)(B_JY901SObj.QuaternionicQ2 * 10000.0));
    vu16 q3 = (vu16)((short)(B_JY901SObj.QuaternionicQ3 * 10000.0));

    data[0] = gyrox >> 24; // 陀螺仪x原始数据 先高后低
    data[1] = gyrox >> 16;
    data[2] = gyrox >> 8;
    data[3] = gyrox;

    data[4] = gyroy >> 24; // 陀螺仪y原始数据 先高后低
    data[5] = gyroy >> 16;
    data[6] = gyroy >> 8;
    data[7] = gyroy;

    data[8] = gyroz >> 24; // 陀螺仪z原始数据 先高后低
    data[9] = gyroz >> 16;
    data[10] = gyroz >> 8;
    data[11] = gyroz;

    data[12] = accelx >> 24; // 加速度计x原始数据 先高后低
    data[13] = accelx >> 16;
    data[14] = accelx >> 8;
    data[15] = accelx;

    data[16] = accely >> 24; // 加速度计y原始数据 先高后低
    data[17] = accely >> 16;
    data[18] = accely >> 8;
    data[19] = accely;

    data[20] = accelz >> 24; // 加速度计z原始数据 先高后低
    data[21] = accelz >> 16;
    data[22] = accelz >> 8;
    data[23] = accelz;

    data[24] = q0 >> 8; // q0 先高后低
    data[25] = q0;

    data[26] = q1 >> 8; // q1 先高后低
    data[27] = q1;

    data[28] = q2 >> 8; // q2 先高后低
    data[29] = q2;

    data[30] = q3 >> 8; // q3 先高后低
    data[31] = q3;

    app_TimerRecvRosSend(A_ROS_UP_QUERY_IMU2_SIZE, A_ROS_UP_QUERY_IMU2_CMD, data);
}

//*******************************************************************
// 函  数：void app_RosHandleQueryUltra(void)
// 功  能：处理接收到的查询超声波命令
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_RosHandleQueryUltra(void)
{
    // 向上位机回复
    vu8 data[A_ROS_UP_QUERY_ULTRA_SIZE - A_ROS_NO_DATA_SIZE] = {0};

    data[0] = B_UltraObj.Distance1 >> 8; // 超声波1 先高后低
    data[1] = B_UltraObj.Distance1;
    data[2] = B_UltraObj.Distance2 >> 8; // 超声波2 先高后低
    data[3] = B_UltraObj.Distance2;

    app_TimerRecvRosSend(A_ROS_UP_QUERY_ULTRA_SIZE, A_ROS_UP_QUERY_ULTRA_CMD, data);
}

//*******************************************************************
// 函  数：void app_RosHandleQueryInfra(void)
// 功  能：处理接收到的查询红外命令
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_RosHandleQueryInfra(void)
{
    // 向上位机回复
    vu8 data[A_ROS_UP_QUERY_INFRA_SIZE - A_ROS_NO_DATA_SIZE] = {0};

    data[0] = B_InfraredObj.En1; // 红外1
    data[1] = B_InfraredObj.En2; // 红外2
    data[2] = B_InfraredObj.En3; // 红外3
    data[3] = B_InfraredObj.En4; // 红外4

    app_TimerRecvRosSend(A_ROS_UP_QUERY_INFRA_SIZE, A_ROS_UP_QUERY_INFRA_CMD, data);
}

//*******************************************************************
// 函  数：void app_RosHandleRestart(void)
// 功  能：重启
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_RosHandleRestart(void)
{
    app_ToolsMCUReset();
}
