#ifndef _RECV_ROS_H_
#define _RECV_ROS_H_
#include "../../app.h"

#define A_ROS_PROTOCOL_TOP 0X5A // 协议头
#define A_ROS_PROTOCOL_ID 0X00  // 协议ID
#define A_ROS_BACK_0X00 0X00    // 回复的固定值0x00
#define A_ROS_BACK_0X01 0X01    // 回复的固定值0x01
#define A_ROS_BACK_0X02 0X02    // 回复的固定值0x02

// 协议数据的下标值
enum _A_ROS_PROTOCOL_INDEX_ENUM
{
    A_ROS_TOP_INDEX = 0,  // 协议头的下标值
    A_ROS_SIZE_INDEX = 1, // 协议长度的下标值
    A_ROS_ID_INDEX = 2,   // 协议id的下标值
    A_ROS_CMD_INDEX = 3,  // 协议命令的下标值
    A_ROS_DATA_INDEX = 4, // 协议第一个数据位的下标值
};

typedef enum _A_ROS_PROTOCOL_SIZE_ENUM
{
    // 协议长度的最大值
    A_ROS_MAX_SIZE = 200,

    // 除了数据位的协议长度，为固定值 6
    A_ROS_NO_DATA_SIZE = 6,

    // 底盘的命令的协议总长度
    A_ROS_DOWN_WHEEL_MOTION_SIZE = 18, // ros下发 		轮子运动		协议总长度
    A_ROS_UP_WHEEL_MOTION_SIZE = 7,    // 回复ros 		轮子运动		协议总长度

    A_ROS_DOWN_QUERY_SPEED_SIZE = 6, // ros下发 		查询速度		协议总长度
    A_ROS_UP_QUERY_SPEED_SIZE = 26,  // 回复ros 		查询速度		协议总长度

    A_ROS_DOWN_QUERY_IMU_SIZE = 6, // ros下发 		查询IMU		    协议总长度
    A_ROS_UP_QUERY_IMU_SIZE = 12,  // 回复ros 		查询IMU		    协议总长度

    A_ROS_DOWN_QUERY_BATTERY_SIZE = 6, // ros下发 		查询电池		    协议总长度
    A_ROS_UP_QUERY_BATTERY_SIZE = 10,  // 回复ros 		查询电池		    协议总长度

    A_ROS_DOWN_QUERY_ODOM_SIZE = 6, // ros下发 		查询里程计		    协议总长度
    A_ROS_UP_QUERY_ODOM_SIZE = 26,  // 回复ros 		查询里程计		    协议总长度

    A_ROS_DOWN_QUERY_IMU2_SIZE = 6, // ros下发 		查询IMU2原始数据		    协议总长度
    A_ROS_UP_QUERY_IMU2_SIZE = 38,  // 回复ros 		查询IMU2原始数据		    协议总长度

    A_ROS_DOWN_QUERY_ULTRA_SIZE = 6, // ros下发 		查询超声波数据		    协议总长度
    A_ROS_UP_QUERY_ULTRA_SIZE = 10,  // 回复ros 		查询超声波数据		    协议总长度

    A_ROS_DOWN_QUERY_INFRA_SIZE = 6, // ros下发 		查询红外数据		    协议总长度
    A_ROS_UP_QUERY_INFRA_SIZE = 10,  // 回复ros 		查询红外数据            协议总长度

    A_ROS_DOWN_RESTART_SIZE = 6, // ros下发 		重启		    协议总长度
    A_ROS_UP_RESTART_SIZE = 6,   // 回复ros 		重启            协议总长度

} A_ROS_PROTOCOL_SIZE_ENUM;

// ros 的命令码
typedef enum _A_ROS_CMD
{

    A_ROS_DOWN_WHEEL_MOTION_CMD = 0X01, // 0X01 1		ros下发 		轮子舵机运动 			指令
    A_ROS_UP_WHEEL_MOTION_CMD = 0X2,    // 0X02 2		回复ros 	    轮子舵机运动 	        指令

    A_ROS_DOWN_QUERY_SPEED_CMD = 0X03, // 0X03 3		ros下发 		查询速度 			指令
    A_ROS_UP_QUERY_SPEED_CMD = 0X04,   // 0X04 4		回复ros 	    查询速度 	        指令

    A_ROS_DOWN_QUERY_IMU_CMD = 0X05, // 0X05 5		ros下发 		查询IMU 			指令
    A_ROS_UP_QUERY_IMU_CMD = 0X06,   // 0X06 6		回复ros 	    查询IMU 	        指令

    A_ROS_DOWN_QUERY_BATTERY_CMD = 0X07, // 0X07 7		ros下发 		查询电池 			指令
    A_ROS_UP_QUERY_BATTERY_CMD = 0X08,   // 0X08 8		回复ros 	    查询电池 	        指令

    A_ROS_DOWN_QUERY_ODOM_CMD = 0X09, // 0X09 9		ros下发 		查询里程计 			指令
    A_ROS_UP_QUERY_ODOM_CMD = 0X0A,   // 0X0A 10	回复ros 	    查询里程计 	        指令

    A_ROS_DOWN_QUERY_IMU2_CMD = 0X13, // 0X13 19	ros下发 		查询IMU2原始数据 			指令
    A_ROS_UP_QUERY_IMU2_CMD = 0X14,   // 0X14 20	回复ros 	    查询IMU2原始数据 	        指令

    A_ROS_DOWN_QUERY_ULTRA_CMD = 0XF5, // 0XF5 245	ros下发 		查询超声波距离 			指令
    A_ROS_UP_QUERY_ULTRA_CMD = 0XF6,   // 0XF6 246	回复ros 	    查询超声波距离 	        指令

    A_ROS_DOWN_QUERY_INFRA_CMD = 0XF7, // 0XF7 247	ros下发 		查询红外数据 			指令
    A_ROS_UP_QUERY_INFRA_CMD = 0XF8,   // 0XF8 248	回复ros 	    查询红外数据 	        指令

    A_ROS_DOWN_RESTART_CMD = 0XFD, // 0XFD 253	ros下发 		查询红外数据 			指令
    A_ROS_UP_RESTART_CMD = 0XFE,   // 0XFE 254	回复ros 	    查询红外数据 	        指令

} A_ROS_CMD;

void app_TimerTaskUsartRecvRos(void);
void app_TimerTaskRecvRosDataHandle(void);
void app_TimerTaskRecvRosDataControl(vu8 cmd);
void app_TimerTaskRecvRosDataHook(void);

void app_TimerRecvRosSend(A_ROS_PROTOCOL_SIZE_ENUM Size, A_ROS_CMD Cmd, vu8 *Data);

#endif
