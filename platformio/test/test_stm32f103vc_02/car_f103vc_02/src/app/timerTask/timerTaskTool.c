#include "timerTaskTool.h"

//*******************************************************************
// 函  数：void app_TimerTaskToolObstacleFlag(void)
// 功  能：障碍检测，超声波和红外，检测到就要刹车
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskToolObstacleFlag(void)
{

    // 检测前先置为没有障碍
    A_AppObj.Infra1Flag = B_FALSE;
    A_AppObj.Infra2Flag = B_FALSE;
    A_AppObj.Infra3Flag = B_FALSE;
    A_AppObj.UltrasonicJustFlag = B_FALSE;
    A_AppObj.UltrasonicBackFlag = B_FALSE;

    // 前超声波遇到障碍，不能向前走了
    if (B_UltraObj.Distance1 <= B_ULTRA_CM_MAX)
    {
        // A_AppObj.UltrasonicJustFlag = B_TRUE;
        // printf("Ultra_Just_true...\n");
    }

    // 后超声波遇到障碍，不能向后走了
    if (B_UltraObj.Distance2 <= B_ULTRA_CM_MAX)
    {
        // A_AppObj.UltrasonicBackFlag = B_TRUE;
        // printf("Ultra_Back_true...\n");
    }

    // 前边红外悬空，不能向前走了
    if (B_InfraredObj.En1 == B_NULL || B_InfraredObj.En2 == B_NULL)
    {
        // A_AppObj.Infra1Flag = B_TRUE;
        // A_AppObj.Infra2Flag = B_TRUE;
        // printf("Infra_Just_true...\n");
    }

    // 后边红外悬空，不能向后走了
    if (B_InfraredObj.En3 == B_NULL)
    {
        // A_AppObj.Infra3Flag = B_TRUE;
        // printf("Infra_Back_true...\n");
    }
}

//*******************************************************************
// 函  数：vu8 app_TimerRecvCRC8MAXIM(vu8 *data, vu32 len)
// 功  能：校验协议数据
// 参  数：data		要crc校验的数据
// 				 len		协议数据的长度，协议中除了crc位，其他的数据都进行校验
// 返回值：校验后的crc位
// 备  注：vu8 tempbuffer[15]={0x5A, 0x0C, 0x01, 0x01, 0x01, 0xF4, 0x01, 0xF4, 0x00, 0x00, 0x00, 0x00  };
//					tempbuffer[11] = app_ToolsCRC8MAXIM(tempbuffer, 11);
//					校验后位  tempbuffer[11] 为 E4
//*******************************************************************
vu8 app_TimerRecvCRC8MAXIM(vu8 *data, vu32 len)
{
    unsigned char crc = 0;
    unsigned char i;

    while (len--)
    {
        crc ^= *data++;
        for (i = 0; i < 8; i++)
        {
            if (crc & 0x01)
                crc = (crc >> 1) ^ 0x8C;
            else
                crc >>= 1;
        }
    }
    return crc;
}
