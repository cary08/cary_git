#include "timerWheel.h"

//*******************************************************************
// 函  数：void app_TimerTaskWheel(void)
// 功  能：轮子控制定时器
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void app_TimerTaskWheel(void)
{

    if (app_TimerTaskIsDelayEnd(A_TIMER_TASK_WHEEL) == B_TRUE)
    {
        // 继续开启延时
        app_TimerTaskWait(A_TIMER_TASK_WHEEL, A_TIMER_TASK_WHEEL_MS, B_NULL);

        // 获取单位时间内的编码器值 并 乘以方向系数
        B_WheelObj.LeftEncodeVal = bsp_WheelEncoderRead(B_ENCODER_LEFT) * B_WHEEL_LEFT_ENCODE_DIRECTION;
        B_WheelObj.RightEncodeVal = bsp_WheelEncoderRead(B_ENCODER_RIGHT) * B_WHEEL_RIGHT_ENCODE_DIRECTION;

        // 转/秒 = 单位时间内编码器值 / 编码器内轴一圈的计数值 * 时间系数(1s/单位时间)
        B_WheelObj.LeftTurnSecond = (float)B_WheelObj.LeftEncodeVal / B_WHEEL_ENCODE_VAL * B_WHEEL_DETECTION_TIME;
        B_WheelObj.RightTurnSecond = (float)B_WheelObj.RightEncodeVal / B_WHEEL_ENCODE_VAL * B_WHEEL_DETECTION_TIME;

        // 速度m/s = 内轴速度(转/秒) / 减速比 * 轮子周长(米)
        B_WheelObj.LeftSpeed = B_WheelObj.LeftTurnSecond / B_WHEEL_REDUCTION_RATIO * B_WHEEL_PERIMETER;
        B_WheelObj.RightSpeed = B_WheelObj.RightTurnSecond / B_WHEEL_REDUCTION_RATIO * B_WHEEL_PERIMETER;

        // 同时计算出 线速度 角速度
        B_WheelObj.SpeedX = (B_WheelObj.LeftSpeed + B_WheelObj.RightSpeed) / 2.0;
        B_WheelObj.SpeedZ = (B_WheelObj.RightSpeed - B_WheelObj.LeftSpeed) / B_WHEEL_SPACING;

        // 判断是否刹车，掉线、避障, 设置速度为0。没有掉线和障碍，正常行驶
        app_TimerWheelStopFlag();

        // 速度为0要 初始化 pid
        if (B_WheelObj.SetLeftSpeed == 0.0)
        {
            bsp_PIDPositionMotorAParamInit(); // 初始化A电机pid
        }
        if (B_WheelObj.SetRightSpeed == 0.0)
        {
            bsp_PIDPositionMotorBParamInit(); // 初始化B电机pid
        }

        // printf("setLeftSpeed:%.2f,setRightSpeed:%.2f\n", B_WheelObj.SetLeftSpeed, B_WheelObj.SetRightSpeed);

        // 设置两个轮子的速度值函数
        bsp_WheelLeftSetTargetSpeed(B_WheelObj.SetLeftSpeed);
        bsp_WheelRightSetTargetSpeed(B_WheelObj.SetRightSpeed);

        // 用实时速度值通过pid获取pwm值
        float leftPwm = bsp_WheelLeftPidGetPwm(B_WheelObj.LeftSpeed);
        float rightPwm = bsp_WheelRightPidGetPwm(B_WheelObj.RightSpeed);

        // 设置两个轮子的 转向 和 pwm值
        bsp_WheelSetDirectionPwm(leftPwm, rightPwm);
    }
}

//*******************************************************************
// 函  数：void app_TimerWheelStopFlag(void)
// 功  能：轮子是否需要刹车，停止行走
// 参  数：无
//
// 返回值：0轮子正常行驶，不需要刹车。  1轮子需要刹车，停止行走
// 备  注：无
//*******************************************************************
void app_TimerWheelStopFlag(void)
{
    if ((A_AppObj.OnLineFlag == B_FALSE ||
         A_AppObj.UltrasonicJustFlag == B_TRUE || A_AppObj.UltrasonicBackFlag == B_TRUE ||
         A_AppObj.Infra1Flag == B_TRUE || A_AppObj.Infra2Flag == B_TRUE || A_AppObj.Infra3Flag == B_TRUE) &&
        (B_WheelObj.SetLeftSpeed != B_NULL || B_WheelObj.SetRightSpeed != B_NULL))
    {
        printf("OnLine:%d,UltraJust:%d,UltraBack:%d,Infra1:%d,Infra2:%d,Infra3:%d,leftSpeed:%.2f,rightSpeed:%.2f\n",
               A_AppObj.OnLineFlag, A_AppObj.UltrasonicJustFlag, A_AppObj.UltrasonicJustFlag,
               A_AppObj.Infra1Flag, A_AppObj.Infra2Flag, A_AppObj.Infra3Flag, B_WheelObj.SetLeftSpeed, B_WheelObj.SetRightSpeed);

        // 掉线 速度设置为 0
        if (A_AppObj.OnLineFlag == B_FALSE)
        {
            B_WheelObj.SetLeftSpeed = B_NULL;
            B_WheelObj.SetRightSpeed = B_NULL;
        }

        // 向前走 速度设置为 0
        if ((A_AppObj.UltrasonicJustFlag == B_TRUE || A_AppObj.Infra1Flag == B_TRUE || A_AppObj.Infra2Flag == B_TRUE) &&
            (B_WheelObj.SetLeftSpeed > B_NULL || B_WheelObj.SetRightSpeed > B_NULL))
        {
            B_WheelObj.SetLeftSpeed = B_NULL;
            B_WheelObj.SetRightSpeed = B_NULL;
        }

        // 向后走 速度设置为 0
        if ((A_AppObj.UltrasonicBackFlag == B_TRUE || A_AppObj.Infra3Flag == B_TRUE) &&
            (B_WheelObj.SetLeftSpeed < B_NULL || B_WheelObj.SetRightSpeed < B_NULL))
        {
            B_WheelObj.SetLeftSpeed = B_NULL;
            B_WheelObj.SetRightSpeed = B_NULL;
        }
    }
}