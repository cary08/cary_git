#include "speak.h"

//*******************************************************************
// 函  数：void bsp_SpeakInit(void)
// 功  能：蜂鸣器 gpio 初始化
// 参  数：无
//
// 返回值：当前计时的毫秒值 的个数
// 备  注：无
//*******************************************************************
void bsp_SpeakInit(void)
{
    SPEAK_GPIO_CLK_ENABLE(); // 使能时钟
    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.Pin = SPEAK_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP; // 推挽输出
    GPIO_InitStruct.Pull = GPIO_PULLUP;         // 上拉
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(SPEAK_GPIO_PORT, &GPIO_InitStruct);
}
