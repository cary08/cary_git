#ifndef _SPEAK_H_
#define _SPEAK_H_
#include "../bsp.h"

#define SPEAK_PIN GPIO_PIN_15
#define SPEAK_GPIO_PORT GPIOD
#define SPEAK_GPIO_CLK_ENABLE() __HAL_RCC_GPIOD_CLK_ENABLE()

void bsp_SpeakInit(void);

#endif
