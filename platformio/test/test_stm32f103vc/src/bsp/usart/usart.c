#include "usart.h"

UART_HandleTypeDef Uart2Handle; // 串口句柄

//*******************************************************************
// 函  数：void bsp_Usar2tInit(vu32 Baud)
// 功  能：初始化串口2
// 参  数：无
//
// 返回值：无
// 备  注：初始化串口2的功能后，然后再去实现 HAL_UART_MspInit 初始化基本硬件，开时钟、gpio、开中断、dma 等
//*******************************************************************
void bsp_Usar2tInit(vu32 Baud)
{

    Uart2Handle.Instance = USART2;
    Uart2Handle.Init.BaudRate = Baud;
    Uart2Handle.Init.WordLength = UART_WORDLENGTH_8B;
    Uart2Handle.Init.StopBits = UART_STOPBITS_1;
    Uart2Handle.Init.Parity = UART_PARITY_NONE;
    Uart2Handle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    Uart2Handle.Init.Mode = UART_MODE_TX_RX;

    HAL_UART_Init(&Uart2Handle); // 这里面调用了 HAL_UART_MspInit 函数，这就是hal库的特点

    // 使能串口接收中断，这个用cubemx生成不出来，需要查看hal库文件说明，去找到这个手动开启
    // UART_IT_RXNE 意思是接收非空中断
    __HAL_UART_ENABLE_IT(&Uart2Handle, UART_IT_RXNE);
}

//*******************************************************************
// 函  数：void HAL_UART_MspInit(UART_HandleTypeDef *Uart2Handle)
// 功  能：开时钟、初始化gpio、中断配置
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void HAL_UART_MspInit(UART_HandleTypeDef *Uart2Handle)
{

    GPIO_InitTypeDef GPIO_InitStruct = {0};
    if (Uart2Handle->Instance == USART2)
    {
        /* USER CODE BEGIN USART2_MspInit 0 */

        /* USER CODE END USART2_MspInit 0 */
        /* USART2 clock enable */
        __HAL_RCC_USART2_CLK_ENABLE();

        __HAL_RCC_GPIOA_CLK_ENABLE();
        /**USART2 GPIO Configuration
        PA2     ------> USART2_TX
        PA3     ------> USART2_RX
        */
        GPIO_InitStruct.Pin = GPIO_PIN_2;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        GPIO_InitStruct.Pin = GPIO_PIN_3;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        /* USART2 interrupt Init */
        HAL_NVIC_SetPriority(USART2_IRQn, 0, 0); // 优先级设置
        HAL_NVIC_EnableIRQ(USART2_IRQn);         // 开启中断通道
        /* USER CODE BEGIN USART2_MspInit 1 */

        /* USER CODE END USART2_MspInit 1 */
    }
}

//*******************************************************************
// 函  数：int fputc(int ch, FILE *f)
// 功  能：用于重定向printf
// 参  数：无
//
// 返回值：无
// 备  注：printf 调用 fputc，然后fputc 调用 发送串口2 函数。 为啥进不来呢？
//          因为编译器的缘故，这个所以要用定义宏的形式，应对各种编译器
//          keil5 用的 CC_ARM 这个编译器，所以之前一直都是重写fputc
//*******************************************************************
// int fputc(int ch, FILE *f)
// {
//     // 发送一个字节数据到串口2
//     HAL_UART_Transmit(&Uart2Handle, (uint8_t *)&ch, 1, 1000);
//     return (ch);
// }
#if defined(__GNUC__)
int _write(int fd, char *ptr, int len)
{
    // 最大给100ms的超时时间，len表示发送的字节长度
    HAL_UART_Transmit(&Uart2Handle, (uint8_t *)ptr, len, 100);
    return len;
}
#elif defined(__ICCARM__)
size_t __write(int handle, const unsigned char *buffer, size_t size)
{
    HAL_UART_Transmit(&huart1, (uint8_t *)buffer, size, HAL_MAX_DELAY);
    return size;
}
#elif defined(__CC_ARM)
int fputc(int ch, FILE *f)
{
    HAL_UART_Transmit(&huart1, (uint8_t *)&ch, 1, HAL_MAX_DELAY);
    return ch;
}
#endif

//*******************************************************************
// 函  数：void USART2_IRQHandler(void)
// 功  能：usart2中断入口函数
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void USART2_IRQHandler(void)
{
    uint8_t ch = 0;
    // 判断串口是否接收到数据
    if (__HAL_UART_GET_FLAG(&Uart2Handle, UART_FLAG_RXNE) != RESET)
    {
        // 有数据，将DR寄存器中的数据读取到 ch 中
        ch = (uint16_t)READ_REG(Uart2Handle.Instance->DR);

        // 这个是将读出的数据，写到DR寄存器中，这个DR是有两个，一个是放读到数据的DR一个是放写出数据的DR
        // 这个就是将读到的数据，写到写出DR寄存器中。
        WRITE_REG(Uart2Handle.Instance->DR, ch);
    }
}
