#include "delay.h"

static vu32 TimeMs = 0;
static vu32 TimeMsNum = 0;
static vu32 WaitMs = 0;

//*******************************************************************
// 函  数：void bsp_DelaySysTickInit(void)
// 功  能：滴答定时器初始化函数
// 参  数：无
//
// 返回值：无
// 备  注：命名风格还是用之前的，不用hal库的命名风格，就是中间不要下划线
//*******************************************************************
void bsp_DelaySysTickInit(void)
{
    // 滴答定时器递减的数值，减到0就中断
    // SystemCoreClock / 1000    = 72000    1ms中断一次
    // SystemCoreClock / 100000  = 720      10us中断一次
    // SystemCoreClock / 1000000 = 72       1us中断一次
    if (HAL_SYSTICK_Config(SystemCoreClock / 1000))
    {
        // error
        while (1)
            ;
    }
}

//*******************************************************************
// 函  数：void bsp_DelayMs(vu32 Ms)
// 功  能：阻塞延时
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void bsp_DelayMs(vu32 Ms)
{
    WaitMs = Ms;
    while (WaitMs != 0)
        ;
}

//*******************************************************************
// 函  数：vu32 bsp_DelayGetMs(void)
// 功  能：获取从开机到当前滴答计时器的总毫秒值
// 参  数：无
//
// 返回值：当前计时的毫秒值
// 备  注：无
//*******************************************************************
vu32 bsp_DelayGetMs(void)
{
    return TimeMs;
}

//*******************************************************************
// 函  数：vu32 bsp_DelayGetMsNum(void)
// 功  能：获取从开机到当前滴答计时器的总毫秒值 的个数
// 参  数：无
//
// 返回值：当前计时的毫秒值 的个数
// 备  注：无
//*******************************************************************
vu32 bsp_DelayGetMsNum(void)
{
    return TimeMsNum;
}

//*******************************************************************
// 函  数：vu8 bsp_DelayIsTimeOut(vu32 oldNum, vu32 oldMs, vu32 outMs)
// 功  能：查看是否延时完毕
// 参  数：oldNum   延时前的延时毫秒个数
//          oldMs   延时前的延时毫秒值
//          outMs   延时的毫秒值
// 返回值：0延时没有完成，1延时结束
// 备  注：无
//*******************************************************************
vu8 bsp_DelayIsTimeOut(vu32 oldNum, vu32 oldMs, vu32 outMs)
{
    // 计数值相等的情况下
    if (oldNum == TimeMsNum)
    {
        // 新的毫秒值，减去 旧的毫秒值，大于超时的毫秒值，那么就是已经超时了
        if ((TimeMs - oldMs) > outMs)
        {
            return B_TRUE; // 已超时
        }
    }

    // 旧的计数值小于新的计数值，没有大于的情况
    if (oldNum < TimeMsNum)
    {
        // 用最大值减去旧的毫秒值再加上新的毫秒值，就是已经延时了的毫秒值，然后对比超时毫秒值，如果大于说明超时
        if (B_DELAY_TIME_MS_MAX - oldMs + TimeMs > outMs)
        {
            return B_TRUE; // 已超时
        }
    }

    return B_FALSE; // 未超时
}

//*******************************************************************
// 函  数：void SysTick_Handler(void)
// 功  能：这个函数名在启动文件.s中为滴答定时器的中断入口名，不能改名
// 参  数：无
//
// 返回值：无
// 备  注：无
//*******************************************************************
void SysTick_Handler(void)
{

    TimeMs++;
    if (TimeMs == B_DELAY_TIME_MS_MAX)
    {
        TimeMsNum++;
    }

    // 用于阻塞延时
    if (WaitMs > B_NULL)
    {
        WaitMs--;
    }
}
