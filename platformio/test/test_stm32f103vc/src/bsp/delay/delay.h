#ifndef _DELAY_H_
#define _DELAY_H_
#include "../bsp.h"

#define B_DELAY_TIME_MS_MAX 0XFFFFFFFF

void bsp_DelaySysTickInit(void);
void bsp_DelayMs(vu32 Ms);
vu32 bsp_DelayGetMs(void);
vu32 bsp_DelayGetMsNum(void);
vu8 bsp_DelayIsTimeOut(vu32 oldNum, vu32 oldMs, vu32 outMs);

#endif
