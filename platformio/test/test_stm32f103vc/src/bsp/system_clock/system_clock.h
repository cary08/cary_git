#ifndef _SYSTEM_CLOCK_H_
#define _SYSTEM_CLOCK_H_
#include "../bsp.h"

void _Error_Handler(char *, int);
#define Error_Handler() _Error_Handler(__FILE__, __LINE__)

void SystemClock_Config(void);

#endif
