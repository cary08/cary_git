#include "app.h"

int main(void)
{
    SystemClock_Config(); // 初始化时钟，72M

    // 中断优先级分组
    HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_2);

    bsp_DelaySysTickInit(); // 滴答定时器初始化
    bsp_SpeakInit();        // 蜂鸣器初始化
    bsp_Usar2tInit(115200); // 串口初始化

    bsp_DelayMs(500);

    printf("start_中文\n");

    vu32 testMs = bsp_DelayGetMs();
    vu32 testMsNum = bsp_DelayGetMsNum();
    vu32 waitMs = 1000;

    while (1)
    {
        if (bsp_DelayIsTimeOut(testMsNum, testMs, waitMs) == B_TRUE)
        {
            printf("ms1000___\n");
            // HAL_GPIO_TogglePin(SPEAK_GPIO_PORT, SPEAK_PIN);
            testMs = bsp_DelayGetMs();
            testMsNum = bsp_DelayGetMsNum();
        }
    }
}
