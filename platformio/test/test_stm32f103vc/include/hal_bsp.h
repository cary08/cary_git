#ifndef _HAL_BSP_H_
#define _HAL_BSP_H_
#include "stm32f1xx_hal.h"

typedef __IO uint32_t vu32;
typedef __IO uint16_t vu16;
typedef __IO uint8_t vu8;

typedef __I uint32_t vuc32;
typedef __I uint16_t vuc16;
typedef __I uint8_t vuc8;

#define B_NULL 0
#define B_TRUE 1
#define B_FALSE 0

#endif