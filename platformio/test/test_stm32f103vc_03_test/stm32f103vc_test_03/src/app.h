#ifndef _APP_H_
#define _APP_H_
#include "stm32f1xx_hal.h"
#include "stdio.h"

#include "tim.h"
#include "usart.h"
#include "stm32f1xx_it.h"

#define jy901s_usart_tx_Pin GPIO_PIN_2
#define jy901s_usart_tx_GPIO_Port GPIOA
#define jy901s_usart_rx_Pin GPIO_PIN_3
#define jy901s_usart_rx_GPIO_Port GPIOA
#define encoder_left_a_Pin GPIO_PIN_6
#define encoder_left_a_GPIO_Port GPIOA
#define encoder_left_b_Pin GPIO_PIN_7
#define encoder_left_b_GPIO_Port GPIOA
#define jy01_left_Pin GPIO_PIN_4
#define jy01_left_GPIO_Port GPIOC
#define pc_usart_tx_Pin GPIO_PIN_8
#define pc_usart_tx_GPIO_Port GPIOD
#define pc_usart_rx_Pin GPIO_PIN_9
#define pc_usart_rx_GPIO_Port GPIOD
#define Infra1_Pin GPIO_PIN_10
#define Infra1_GPIO_Port GPIOD
#define Infra2_Pin GPIO_PIN_11
#define Infra2_GPIO_Port GPIOD
#define Infra3_Pin GPIO_PIN_12
#define Infra3_GPIO_Port GPIOD
#define Infra4_Pin GPIO_PIN_13
#define Infra4_GPIO_Port GPIOD
#define speak_Pin GPIO_PIN_15
#define speak_GPIO_Port GPIOD
#define jy01_left_speed_Pin GPIO_PIN_6
#define jy01_left_speed_GPIO_Port GPIOC
#define jy01_right_speed_Pin GPIO_PIN_7
#define jy01_right_speed_GPIO_Port GPIOC
#define jy01_left_brake_Pin GPIO_PIN_8
#define jy01_left_brake_GPIO_Port GPIOC
#define jy01_right_brake_Pin GPIO_PIN_9
#define jy01_right_brake_GPIO_Port GPIOC
#define ultra_read_Pin GPIO_PIN_8
#define ultra_read_GPIO_Port GPIOA
#define ultra_read_EXTI_IRQn EXTI9_5_IRQn
#define ros_usart_tx_Pin GPIO_PIN_9
#define ros_usart_tx_GPIO_Port GPIOA
#define ros_usart_rx_Pin GPIO_PIN_10
#define ros_usart_rx_GPIO_Port GPIOA
#define ultra_out_echo2_a1_Pin GPIO_PIN_11
#define ultra_out_echo2_a1_GPIO_Port GPIOA
#define ultra_out_echo2_a2_Pin GPIO_PIN_12
#define ultra_out_echo2_a2_GPIO_Port GPIOA
#define encoder_right_b_Pin GPIO_PIN_15
#define encoder_right_b_GPIO_Port GPIOA
#define ultra_out_trig1_Pin GPIO_PIN_10
#define ultra_out_trig1_GPIO_Port GPIOC
#define ultra_out_trig2_Pin GPIO_PIN_2
#define ultra_out_trig2_GPIO_Port GPIOD
#define ultra_out_trig3_Pin GPIO_PIN_3
#define ultra_out_trig3_GPIO_Port GPIOD
#define ultra_out_trig4_Pin GPIO_PIN_5
#define ultra_out_trig4_GPIO_Port GPIOD
#define ultra_out_trig5_Pin GPIO_PIN_6
#define ultra_out_trig5_GPIO_Port GPIOD
#define encoder_right_a_Pin GPIO_PIN_3
#define encoder_right_a_GPIO_Port GPIOB
#define jy01_right_Pin GPIO_PIN_4
#define jy01_right_GPIO_Port GPIOB
#define ultra_out_trig6_Pin GPIO_PIN_5
#define ultra_out_trig6_GPIO_Port GPIOB
#define ultra_out_trig7_Pin GPIO_PIN_6
#define ultra_out_trig7_GPIO_Port GPIOB
#define ultra_out_trig8_Pin GPIO_PIN_7
#define ultra_out_trig8_GPIO_Port GPIOB
#define ultra_out_echo2_a0_Pin GPIO_PIN_8
#define ultra_out_echo2_a0_GPIO_Port GPIOB

void Error_Handler(void);

void SystemClock_Config(void);

void HAL_MspInit(void);
#endif