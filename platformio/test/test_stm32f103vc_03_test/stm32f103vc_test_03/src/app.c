#include "app.h"

int main(void)
{
    HAL_Init();
    HAL_MspInit();

    SystemClock_Config();
    MX_USART1_UART_Init();
    MX_USART2_UART_Init();
    MX_USART3_UART_Init();
    // MX_TIM3_Init();
    // MX_TIM2_Init();

    // MX_TIM8_Init();

    HAL_UART_Receive_IT(&huart1, uart_ros_buff, 1);
    HAL_UART_Receive_IT(&huart2, uart_pc_buff, 1);
    // __HAL_UART_ENABLE_IT(&huart2, UART_IT_RXNE);
    // __HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);

    // __HAL_UART_ENABLE_IT(&huart3, UART_IT_RXNE);
    // __HAL_UART_ENABLE_IT(&huart3, UART_IT_IDLE);

    HAL_UART_DMAStop(&huart3);
    __HAL_UART_ENABLE_IT(&huart3, UART_IT_RXNE);
    __HAL_UART_ENABLE_IT(&huart3, UART_IT_IDLE);
    HAL_UART_Receive_DMA(&huart3, uart_jy901s_buff, 100);

    // HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);
    // HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);

    // HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_1);
    // HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_2);
    // HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_3);
    // HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_4);

    while (1)
    {
        // __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_1, 1000); // left
        // __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_2, 1000); // right

        // printf("encoder_left:%ld\n", __HAL_TIM_GET_COUNTER(&htim3));
        // printf("encoder_right:%ld\n", __HAL_TIM_GET_COUNTER(&htim2));

        // __HAL_TIM_SET_COUNTER(&htim2, 0);
        // __HAL_TIM_SET_COUNTER(&htim3, 0);

        HAL_Delay(1000);
    }
}

void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

    /** Initializes the RCC Oscillators according to the specified parameters
     * in the RCC_OscInitTypeDef structure.
     */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI | RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.LSIState = RCC_LSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
    /** Initializes the CPU, AHB and APB buses clocks
     */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
    {
        Error_Handler();
    }
}

/* USER CODE BEGIN 4 */

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if (GPIO_Pin == ultra_read_Pin)
    {
        //		if(HAL_GPIO_ReadPin(ultra_read_GPIO_Port,ultra_read_Pin) == GPIO_PIN_SET)
        //		{
        //			HAL_GPIO_WritePin(speak_GPIO_Port, speak_Pin, GPIO_PIN_SET);
        //		}
        //		else if(HAL_GPIO_ReadPin(ultra_read_GPIO_Port,ultra_read_Pin) == GPIO_PIN_RESET)
        //		{
        //			HAL_GPIO_WritePin(speak_GPIO_Port, speak_Pin, GPIO_PIN_RESET);
        //		}
    }
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    __disable_irq();
    while (1)
    {
    }
    /* USER CODE END Error_Handler_Debug */
}

void HAL_MspInit(void)
{
    __HAL_RCC_AFIO_CLK_ENABLE();
    __HAL_RCC_PWR_CLK_ENABLE();

    __HAL_AFIO_REMAP_SWJ_NOJTAG();
}
