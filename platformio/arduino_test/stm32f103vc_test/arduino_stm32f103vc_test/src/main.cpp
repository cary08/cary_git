#include <Arduino.h>

HardwareSerial Serial1(PA_10, PA_9);  // RX  TX
HardwareSerial Serial3(PD_11, PB_10); // RX  TX  PD_9, PD_8
HardwareTimer myPwm(TIM8);
HardwareTimer myEncoderRight(TIM2);
HardwareTimer myEncoderLeft(TIM3);

void setup()
{

  pinMode(PD_15, OUTPUT);
  Serial1.begin(115200);
  Serial1.println(111);

  Serial2.begin(115200);
  Serial2.println(2);

  Serial3.setTx(PD_8);
  Serial3.setRx(PD_9);
  Serial3.begin(9600);

  // pwm 参数3是 pwm值，0最小，100最大
  myPwm.setPWM(1, PC_6, 7200, 20, 0, 0);
  myPwm.setPWM(2, PC_7, 7200, 20, 0, 0);
  // myPwm.setPWM(3, PC_8, 7200, 100, 0, 0);
  // myPwm.setPWM(4, PC_9, 7200, 100, 0, 0);

  // 输入捕获试试效果
  myEncoderRight.setMode(1, TIMER_INPUT_FREQ_DUTY_MEASUREMENT, PA_15);
}

void loop()
{
  // digitalWrite(PD_15, HIGH);
  // delay(1000);
  // digitalWrite(PD_15, LOW);
  // delay(1000);
  // uint8_t buff[100] = {0};
  // int count = Serial3.readBytes(buff, 1);

  // Serial2.printf("%02X ", buff[0]);

  uint32_t encoderRight = 0;
  // encoderRight = myEncoderRight.getCaptureCompare(1);
  // encoderRight = myEncoderRight.getCount();
  encoderRight = myEncoderRight.getOverflow();
  printf("encoder_right:%d\n", encoderRight);
  delay(1000);

  // Serial2.printf("%02X ", Serial3.read());
}
