#include <stdio.h>
void main()
{
    int j, a[] = {1, 3, 5, 7, 9, 11, 13, 15}, *p = a + 5;
    for (j = 4; j > 0; j--)
    {
        switch (j)
        {
        case 1:
        case 2:
            printf("%d \n", *p);
            printf("---1---\n");
            break;
        case 3:
            printf("%d \n", *(--p));
            printf("---2---\n");
        case 4:
            printf("%d \n", *(--p));
            printf("---3---\n");
        }
    }
}
