/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "iwdg.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
int ultraReadFlag = 0;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint32_t testNum = 0;
uint32_t testNum2 = 0;
uint32_t testNum3 = 0;
uint8_t ultState = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM5_Init();
  MX_TIM8_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
//  MX_IWDG_Init();
  /* USER CODE BEGIN 2 */

	// user init ...
	// 1.start time2	encode_right
	HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);
	// 2.start time3	encode_left
	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);
	// 3.start time5	ultra ,is not it
//	HAL_TIM_Base_Start_IT(&htim5);
	// 4.start time8	wheel_speed_brake			__HAL_TIM_SET_CONPARE(__HANDLE_, __CHANNEL__, __COMPARE__)
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_4);
	// 5.start usart
//	HAL_UART_Receive_IT(&huart1,uart_ros_buff,1);
//	HAL_UART_Receive_IT(&huart2,uart_pc_buff,1);

	__HAL_UART_ENABLE_IT(&huart1,UART_IT_RXNE);
	__HAL_UART_ENABLE_IT(&huart1,UART_IT_IDLE);

	__HAL_UART_ENABLE_IT(&huart2,UART_IT_RXNE);
	__HAL_UART_ENABLE_IT(&huart2,UART_IT_IDLE);

//	__HAL_UART_ENABLE_IT(&huart3,UART_IT_RXNE);
//	__HAL_UART_ENABLE_IT(&huart3,UART_IT_IDLE);

 //????DMA??
 HAL_UART_DMAStop(&huart3);
// //????1????
 __HAL_UART_ENABLE_IT(&huart3,UART_IT_RXNE);
// //????1????
 __HAL_UART_ENABLE_IT(&huart3,UART_IT_IDLE);
// //????DMA??
// __HAL_DMA_ENABLE_IT(&hdma_usart3_rx,DMA_IT_TC);
 HAL_UART_Receive_DMA(&huart3,uart_jy901s_buff,100);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	
	// test init...
//	HAL_Delay(200);
	HAL_GPIO_WritePin(speak_GPIO_Port, speak_Pin, GPIO_PIN_SET);
//	int i = 0;
//	for(i=0; i<100000; i++)
//	{
//		i=i;
//	}
	HAL_Delay(200);
	HAL_GPIO_WritePin(speak_GPIO_Port, speak_Pin, GPIO_PIN_RESET);
	
	printf("start\n");
	uint8_t buff[10] = {0,1,2,3,4,5,6,7,8,9};
	HAL_UART_Transmit(&huart1, (uint8_t *)buff, 10, HAL_MAX_DELAY);
	
  while (1)
  {
//		HAL_GPIO_TogglePin(speak_GPIO_Port, speak_Pin);
//		printf("1000ms........\n");
//		HAL_Delay(1000);
		
		
		// test 
		testNum++;
		if(testNum > 10000000)
		{
		
			// wheel just
			HAL_GPIO_WritePin(jy01_left_GPIO_Port, jy01_left_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(jy01_right_GPIO_Port, jy01_right_Pin, GPIO_PIN_SET);
			
			// wheel back
//			HAL_GPIO_WritePin(jy01_left_GPIO_Port, jy01_left_Pin, GPIO_PIN_SET);
//			HAL_GPIO_WritePin(jy01_right_GPIO_Port, jy01_right_Pin, GPIO_PIN_RESET);
			
			
			// wheel speed_pwm
			__HAL_TIM_SET_COMPARE(&htim8,TIM_CHANNEL_1, 1000);		// left
			__HAL_TIM_SET_COMPARE(&htim8,TIM_CHANNEL_2, 1000);		// right
			
			// wheel get_encoder
			printf("encoder_right:%d\n", __HAL_TIM_GET_COUNTER(&htim2));
			printf("encoder_left:%d\n", __HAL_TIM_GET_COUNTER(&htim3));
			
			// wheel clear_encoder 
			__HAL_TIM_SET_COUNTER(&htim2, 0);
			__HAL_TIM_SET_COUNTER(&htim3, 0);
			
			// wheel brake_pwm
	//		HAL_Delay(4000);
	//		__HAL_TIM_SET_COMPARE(&htim8,TIM_CHANNEL_3, 1000);
	//		__HAL_TIM_SET_COMPARE(&htim8,TIM_CHANNEL_4, 1000);
	//		HAL_Delay(1000);
			
			// infra
			printf("infra_1:%d\n",HAL_GPIO_ReadPin(Infra1_GPIO_Port, Infra1_Pin));
			printf("infra_2:%d\n",HAL_GPIO_ReadPin(Infra2_GPIO_Port, Infra2_Pin));
			printf("infra_3:%d\n",HAL_GPIO_ReadPin(Infra3_GPIO_Port, Infra3_Pin));
			printf("infra_4:%d\n",HAL_GPIO_ReadPin(Infra4_GPIO_Port, Infra4_Pin));
			testNum = 0;
		}
//		
//		// ultra
//		int count = 0;
//		switch(ultState)
//		{
//			case 0:
//			{
////				printf("ultra_case_0_start_\n");
//				// jieshou kaiguan
//				HAL_GPIO_WritePin( ultra_out_echo2_a0_GPIO_Port,  ultra_out_echo2_a0_Pin, GPIO_PIN_SET);
//				HAL_GPIO_WritePin( ultra_out_echo2_a1_GPIO_Port,  ultra_out_echo2_a1_Pin, GPIO_PIN_RESET);
//				HAL_GPIO_WritePin( ultra_out_echo2_a2_GPIO_Port,  ultra_out_echo2_a2_Pin, GPIO_PIN_SET);
//				
//				// FaShe  gaodianping
//				HAL_GPIO_WritePin( ultra_out_trig1_GPIO_Port,  ultra_out_trig1_Pin, GPIO_PIN_SET);
//				
//				// qingling dakai dingshiqi
//				__HAL_TIM_SetCounter(&htim5,0);
//				HAL_TIM_Base_Start(&htim5);

//				// dengdai >10us
//				while( __HAL_TIM_GetCounter(&htim5)< 20);

//				ultState = 1;
////				printf("ultra_case_0_end_\n");
//				break;
//			}
//			case 1:
//			{

//				// guanbi FaShe
//				HAL_GPIO_WritePin( ultra_out_trig1_GPIO_Port,  ultra_out_trig1_Pin, GPIO_PIN_RESET);
//				
//				// guanbi qingkong dingshiqi 
//				__HAL_TIM_SetCounter(&htim5,0);
//				HAL_TIM_Base_Stop(&htim5);
//				
//				ultState = 2;

//				break;
//			}
//			case 2:
//			{

//				// dengdai JieShou gaodianping
//				if(HAL_GPIO_ReadPin(ultra_read_GPIO_Port,ultra_read_Pin) == GPIO_PIN_SET)
//				{
//					// gaodianping le
//					// kaiqi dingshiqi
//					HAL_TIM_Base_Start(&htim5);
//					// zhuangtai zhiyi
//					ultState = 3;
//				}

//				break;
//			}
//			case 3:
//			{

//				// dengdai JieShou didianping
//				if(HAL_GPIO_ReadPin(ultra_read_GPIO_Port,ultra_read_Pin) == GPIO_PIN_RESET)
//				{
//					// jisuan juli zhi
//					count = __HAL_TIM_GetCounter(&htim5);
//					float juli = count / 5.8;
//					
//					// guanbi qingling dingshiqi
//					__HAL_TIM_SetCounter(&htim5,0);
//					HAL_TIM_Base_Start(&htim5);
//					
//					
//					printf("ult_mm:%.2f\n",juli);
//					ultState = 4;
//				}

//				break;
//			}
//			case 4:
//			{

//				// yanshi 50ms
//				count = __HAL_TIM_GetCounter(&htim5);
//				if(count > 50000)
//				{
//					__HAL_TIM_SetCounter(&htim5,0);
//					HAL_TIM_Base_Stop(&htim5);
//					ultState = 0;
//				}

//				break;
//			}
//		}
//		
		
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == ultra_read_Pin)
	{
//		if(HAL_GPIO_ReadPin(ultra_read_GPIO_Port,ultra_read_Pin) == GPIO_PIN_SET)
//		{
//			HAL_GPIO_WritePin(speak_GPIO_Port, speak_Pin, GPIO_PIN_SET);
//		}
//		else if(HAL_GPIO_ReadPin(ultra_read_GPIO_Port,ultra_read_Pin) == GPIO_PIN_RESET)
//		{
//			HAL_GPIO_WritePin(speak_GPIO_Port, speak_Pin, GPIO_PIN_RESET);
//		}
	}
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
