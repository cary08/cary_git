/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_TIM5_Init();
  MX_TIM8_Init();
  MX_USART1_UART_Init();
  MX_USART3_UART_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */

//	__HAL_TIM_SET_COUNTER(&htim2, 0);
//	__HAL_TIM_SET_COUNTER(&htim3, 0);

//	__HAL_TIM_CLEAR_IT(&htim2, TIM_IT_UPDATE);
//	__HAL_TIM_CLEAR_IT(&htim3, TIM_IT_UPDATE);
//	
//	__HAL_TIM_ENABLE_IT(&htim2, TIM_IT_UPDATE);
//	__HAL_TIM_ENABLE_IT(&htim3, TIM_IT_UPDATE);

//	__HAL_TIM_URS_ENABLE(&htim2);
//	__HAL_TIM_URS_ENABLE(&htim3);

	HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);

//    __HAL_UART_ENABLE_IT(&huart1, UART_IT_RXNE);
		HAL_UART_Receive_IT(&huart1,data_buff,1);
//		__HAL_UART_ENABLE_IT(&huart2, UART_IT_RXNE);
		HAL_UART_Receive_IT(&huart2,data_buff,1);
//		__HAL_UART_ENABLE_IT(&huart3, UART_IT_RXNE);
		HAL_UART_Receive_IT(&huart3,data_buff,100);
__HAL_UART_ENABLE_IT(&huart3, UART_IT_IDLE);
HAL_UART_Receive_DMA(&huart3, (uint8_t *)data_buff, 100);
//HAL_DMA_Start(&hdma_usart3_rx, uint32_t SrcAddress, uint32_t DstAddress, uint32_t DataLength)
    printf("start\n");
//		uint8_t send[10] = {1,2,3,4,5,6,7,8,9,0};
//		HAL_StatusTypeDef state = HAL_UART_Transmit_DMA(&huart1,(uint8_t *)send, 10);
//		printf("state:%d\n",state);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
    while (1)
    {
        //		HAL_GPIO_TogglePin(SPEAK_GPIO_Port, SPEAK_Pin);
//        TIM_SetTIM8Compare1(2000);
//			TIM_SetTIM8Compare2(2000);
//			printf("tim3_encode_main:%d\n",__HAL_TIM_GET_COUNTER(&htim3));
//			printf("tim2_encode_main:%d\n",__HAL_TIM_GET_COUNTER(&htim2));
//			__HAL_TIM_SET_COUNTER(&htim3, 0);
//			__HAL_TIM_SET_COUNTER(&htim2, 0);
			
			if(RxFlag == 1)
			{
				RxFlag = 0;
		// ???????,??????? ????????DMA????????????
		RecCount = 100 - __HAL_DMA_GET_COUNTER(&hdma_usart3_rx);
				printf("RecCount=%d\n", RecCount);
		// ??DMA?????????????PC
//		HAL_UART_Transmit_DMA(&huart1,(uint8_t *)data_buff, RecCount);
				int i=0;
				printf("data_buff\n");
				for(i=0; i<RecCount; i++)
				{
					printf(" %d",data_buff[i]);
				}
				printf("\n");
		RecCount = 0;
		// ??DMA Disable,??DMA??,??????????????????DMA??
		// ?????????DMA??,??????????,?????DMA??
		__HAL_DMA_DISABLE(&hdma_usart3_rx);
			}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
//        HAL_Delay(800);
    }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
// int fputc(int ch, FILE *f)
//{
//     // ???????????2
//     HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 1000);
//     return (ch);
// }
#if defined(__GNUC__)
int _write(int fd, char *ptr, int len)
{
    // ???100ms?????,len?????????
    HAL_UART_Transmit(&huart1, (uint8_t *)ptr, len, 100);
    return len;
}
#elif defined(__ICCARM__)
size_t __write(int handle, const unsigned char *buffer, size_t size)
{
    HAL_UART_Transmit(&huart1, (uint8_t *)buffer, size, HAL_MAX_DELAY);
    return size;
}
#elif defined(__CC_ARM)
int fputc(int ch, FILE *f)
{
    HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 100);
    return ch;
}
#endif
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    __disable_irq();
    while (1)
    {
    }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
