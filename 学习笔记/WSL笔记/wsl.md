# Windows11 WSL使用 ubuntu

- 参考连接
  - 超详细Windows10/Windows11 子系统（WSL2）安装Ubuntu20.04（带桌面环境）
    > https://voycn.com/article/chaoxiangxiwindows10windows11-zixitongwsl2anzhuangubuntu2004daizhuomianhuanjing

  - 排查适用于 Linux 的 Windows 子系统问题
    > https://docs.microsoft.com/zh-cn/windows/wsl/troubleshooting

  - 为win10的linux子系统搭载图形界面(WSL安装桌面)
    > https://www.cnblogs.com/liangxuran/p/14274847.html

### 1. 开启Windows11的WSL功能

- 打开控制面板
- 程序和功能
- 勾选启用或关闭Windows功能
- 勾选虚拟机平台
- 点击确定，点击重启

### 2. 下载Linux内核更新包 并 安装
> https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi


### 3. 将WSL2设置为默认版本
- 打开PowerShell，然后在安装新的 Linux 发行版时运行以下命令，将 WSL 2 设置为默认版本：
  > wsl --set-default-version 2

### 4. 在 Microsoft Store 中下载并安装linux发行版 ubuntu

- 打开Microsoft Store
- 搜索 《ubuntu》 进行下载
- 打开下载完成的 ubuntu
- 设置账号密码

### 5. 在此电脑中可以看到 ubuntu 可以进行文件交互
 ![](img/1.png)

### 6. 更新源

- 备份源列表
    > sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak
- 命令行打开sources.list文件
    > sudo vim /etc/apt/sources.list
- 修改sources.list文件
    > sudo vim /etc/apt/sources.list
- 将源文件内容全部注释，并添加以下内容任意源

  - 清华源
  
    deb http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
    deb-src http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse

    deb http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
    deb-src http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse

    deb http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
    deb-src http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse

    deb http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
    deb-src http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse

    deb http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
    deb-src http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse

  - 阿里源

    deb http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
    deb-src http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse

    deb http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
    deb-src http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse

    deb http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
    deb-src http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse

    deb http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
    deb-src http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse

    deb http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
    deb-src http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse

- 更新源
    > sudo apt-get update

- 更新软件，然后你就可以感受到更换国内源之后的飞速提升了
    > sudo apt-get dist-upgrade
    > sudo apt-get upgrade

### 7. 安装好用的终端 Windows Terminal
- 在 Microsoft Corporation 中搜索 Windows Terminal 下载
- 设置为默认终端，点击下拉箭头-》设置-》默认终端应用程序
### 8. 安装桌面环境
> Windows子系统下安装的分发版Ubuntu是默认不带有桌面环境的，为了能够使用桌面环境，我们需要手动安装。桌面环境有xfce4、gnome等。

#### - 安装gnome桌面环境，和Ubuntu官方版本的桌面环境相似。 (未完成)
> 这个安装太慢！（并且未实现）
- \# 安装gnome桌面环境
    > sudo apt-get install ubuntu-desktop
- \# 安装相关工具
    > sudo apt-get install gnome-tweak-tool
- 安装systemctl
    >  Windows子系统下安装的分发版Ubuntu默认不支持systemctl命令，然而systemctl在原生版Ubuntu中是非常重要的，在这里手动安装。
    - 在Ubuntu命令行中依次执行如下命令：
      - git clone https://github.com/DamionGans/ubuntu-wsl2-systemd-script.git
      - cd ubuntu-wsl2-systemd-script/
      - bash ubuntu-wsl2-systemd-script.sh
    - 到这里进行不下去了，git根本下载不下来。

#### - 安装xfce4桌面环境（已完成）
1. win11中安装VcXsrv
   > https://sourceforge.net/projects/vcxsrv/
2. wsl终端中安装xfce4
   - #!/bin/bash
   - \# this is bash command
   - sudo apt-get update #更新源
   - sudo apt-get install xfce4 xfce4-terminal #安装xfce4桌面
   - echo -e "\n##DISPLAY Configuration" >> ~/.bashrc #配置声明
   - echo "export DISPLAY=127.0.0.1:0.0" >> ~/.bashrc #添加配置
   - source ~/.bashrc #配置生效
   - #run xfce4
   - startxfce4 #如何运行xfce4
3. 使用
   - 在win10中打开VcXsrv，以开启Xserver监听wsl，选择 One window
   - 下一步，下一步然后勾选 Disable access control
   - 然后会打开一个窗口，等着就行了。
4. 配置监听
   - 打开windows终端，Powershell运行 ipconfig
   - 查看虚拟平台的地址（WSL）：vEthernet，这个地址每次打开都会变。
5. 配置.bashrc 
   - 中的最后追加一行
   > export DISPLAY=172.17.208.1:0
   - 其中 172.17.208.1 每次开机都会变都要改
   - 保存并 source ~/.bashrc 使配置生效
6. 启动图形界面
   - ubuntu 执行 startxfce4
   - 然后在 windows中就会监听到界面了




