## linux目录结构

- bin 常用

    存放最经常使用的命令
- sbin

    存放系统管理员使用的系统管理程序 
- home 常用

    存放普通用户的主目录，在linux中每个用户都有一个自己的目录，一般该目录名是以用户的账号命名
- root 常用

    系统管理员的用户主目录
- lib 

    系统开机所需要最基本的动态连接共享库，其作用类似于windows里的dll文件，几乎所有的应用程序都需要用到这些共享库
- lost+found

    这个目录一般情况下是空的，当系统非法关机后，这里就存放了一些文件
- etc 常用

    所有的系统管理所需要的配置文件和子目录，比如安装mysql数据库的 my.conf文件
- usr 常用

    这是一个非常重要的目录，用户的很多应用程序和文件都放在这个目录下，类似于windows下的program files目录
- boot 常用

    存放的是启动linux时使用的一些核心文件，包括一些连接文件以及镜像文件
- proc 不能动

    这个目录是一个虚拟的目录，他是系统内存的映射，访问这个目录来获取系统信息
- srv 不能动

    service缩写，该目录存放一些服务启动之后需要提取的数据
- sys 不能动

    这是linux2.6内核的一个很大的变化，该目录下安装了2.6内核中新出现的一个文件系统sysfs
- tmp

    这个目录存放一些临时文件 
- dev

    类似于windows的设备管理器，把所有的硬件用文件的形式存储
- media 常用

    linux系统会自动识别一些设备，例如u盘，光驱等等，当识别后，linux会把识别的设备挂载到这个目录下
- mnt 常用

    系统提供该目录是为了让用户临时挂载别的文件系统的，我们可以将外部的存储挂载在 /mnt/上，然后进入该目录就可以查看里面的内容了。
- opt

    这是给主机额外安装软件所摆放的目录，如安装oracle数据库就可放在该目录下，默认为空。

    主要是存放，安装文件的。
- usr/local 常用

    这是另一个给主机额外软件安装的目录，一般是通过编译源码的方式安装的程序
- var 常用

    这个目录中存放着在不断扩充着的东西，习惯将经常被修改的目录放在这个目录下，包括各种日志文件
- selinux

    selinux是一种安全子系统，他能控制程序只能访问特定文件，有三种工作模式，可以自行设置



### 用户组

- /etc/passwd 文件 用户user的配置文件，记录用户的各种信息 每行的含义

    用户名:口令:用户标识:组标识号:注释性描述:主目录:登录shell



- /etc/shadow 文件 口令配置文件 每行的含义

    登录名:加密口令:最后一次修改时间:最小时间间隔:最大时间间隔:警告时间:不活动时间:失效时间:标志
- /etc/group 文件 组group的配置文件，记录linux包含的组的信息 每行含义

    组名:口令:组标识号:组内用户列表





### 指定运行级别



- 0：关机
- 1：单用户【找回丢失密码】
- 2：多用户状态没有网络服务
- 3：多用户状态有网络服务
- 4：系统未使用保留给用户
- 5：图形界面
- 6：系统重启
- 常用级别是3和5，也可以指定默认运行级别





- 应用实例

    init[0123456] 通过init来切换不同的运行级别，也可以修改默认运行级别





### 找回root密码、



- 进入到运行级别3，init 3
- 进入系统前按e键，进入开机前的配置界面
- 找到linux16开头的那一行，将光标移动到最后边，然后空格 然后输入 init=/bin/sh ， 这就表示进入单用户模式
- 然后按 ctrl+x 进入单用户模式
- 重启后进入到系统后，在光标闪烁的位置输入：mount -o remount,rw /  ， 然后按回车
- 然后输入 passwd 然后回车，输入两次密码
- 然后输入：touch /.autorelabel 然后回车
- 然后输入：exec /sbin/init 回车后等待比较长的时间，不是死机，会自动重启。

### 帮助指令



- man 获得帮助信息

    基本语法：man [命令或配置文件]
- help 指令

    基本语法：help 命令



### 文件目录类指令

- less 分屏查看文件内容，与 more指令类似

    不同之处在于，more是加载整个文件后在显示，而less是查看哪个地方就去加载哪个地方

    空格键：向下翻动一页

    pagedown：向下翻动一页

    pageup：向上翻动一页

    /要查询的字符串：n：向下查找，N：向上查找

    ?要查询的字符串：n：向下查找，N：向上查找

    q：退出less
- echo 输出内容到控制台

    输出环境变量：echo $PATH
- head 显示文件的开头部分内容，默认显示文件前10行

    head -n 5 这个 5 代表显示前5行，可以任意行数
- tail 输出文件内容的尾部，默认10行

    -f 实时追踪该文档的所有更新，很有用，抓包，监听
- ln -s [原文件或目录] [软连接名]



### crond 任务调度

- 设置任务调度文件 /etc/crontab
- 设置个人任务调度，执行 crontab -e 命令
- 接着输入任务到调度文件
- 如：*/1 * * * * ls -l /etc/ > /tmp/to.txt 命令

    意思是说，每小时每分钟执行 ls -l /etc/ > /tmp/to.txt 命令
- 参数细节说明
- 5个占位符说明
- 第一个* 一小时当中的第几分钟  范围  0-59
- 第一个* 一天当中的第几小时  范围  0-23
- 第一个* 一个月当中的第几天  范围  1-31
- 第一个* 一年当中的第几月  范围  1-12
- 第一个* 一周当中的星期几  范围  0-7（0和7都代表星期日）



- 特殊符号说明
- * 代表任何时间，比如第一个*，就代表一小时中每分钟都执行一次的意思
- , 代表不连续的时间，比如 0 8,12,16 * * * 命令，就代表在每天的8点0分，12点0分，16点0分都执行一次命令
- - 代表连续的时间范围，比如 0 5 * * 1-6 命令，代表在周一到周六的凌晨5点0分执行命令
- */n 代表每隔多久执行一次，比如 */10 * * * * 命令，代表每隔10分钟就执行一遍命令
- 
- 特定时间执行任务案例
- 45 22 * * * 命令 在22点45分执行命令
- 0 17 * * 1 命令  每周 1 的17点0分执行命令
- 0 5 1,15 * * 每月1日和15日的凌晨5点0分执行命令
- */10 4 * * * 命令，每天凌晨4点每隔10分执行命令
- 0 0 1,15 * 1 命令，每月1号和15号，和每周一，凌晨0点零分执行命令，周几和几号最好不要同时出现
- 脚本步骤
    - 写脚本
    - 赋权限
    - 写定时语句



- crond 相关指令
- crontab-r 终止任务调度，相当于删除写的定时语句
- crontab -l 列出当前有哪些任务调度
- service crond restart 重启任务调度





### at定时任务



- 基本介绍
- at命令是一次性定时计划任务，at的守护进程atd会以后台模式运行，检查作业队列来运行
- 默认情况下，atd守护进程每60秒检查作业队列，有作业时，会检查作业运行时间，如果时间与当前时间匹配，则运行此作业
- at命令是一次性定时计划任务，执行完一次任务后不再执行此任务了
- 在使用at命令的时候，一定要保证atd进程的启动，可以使用相关指令来查看
- ps -ef 显示当前所有在运行的进程
- ps -ef | grep atd 查看是否有atd进程在运行
- 
- at命令格式 
- at 【选项】 【时间】
- ctrl + d 表示命令输入结束，不是 回车。。。
- 
- at命令选项
- -m 当指定的任务被完成后，将给用户发送邮件，即使没有标准输出
- -I atq的别名，查询的意思（这个是 -爱 不是 -爱奥，Il 不分的字体最恶心了！！！）
- -d atrm的别名，删除的意思
- -v 显示任务将被执行的时间
- -c 打印任务的内容到标准输出
- -V 显示版本信息
- -q <队列> 使用指定的队列
- -f <文件> 从指定文件读入任务而不是从标准输入读入
- -t <时间参数> 以时间参数的形式提交要运行的任务
- 
- at时间定义
- at指定时间的方法
- 接受在当天的hh:mm(小时:分钟) 式的时间指定。假如该时间已过去，那么就放在第二天执行。例如：04:00
- 使用midnight（深夜），noon(中午)，teatime（饮茶时间，一般下午4点）等比较模糊的词语来指定时间
- 采用12小时计时制，即在时间后面加上AM（上午）或PM（下午）来说明是上午还是下午。例如12pm
- 指定命令执行的具体日期，指定格式为month day（月 日）或 mm/dd/yy (月/日/年)或 dd.mm.yy(日.月.年)，指定的日期必须跟在指定时间的后面。例如：04:00 2021-03-1
- 使用相对计时法。指定格式为：now + count time-units, now 就是当前时间，time-units是时间单位，这里能够是minutes（分钟）、hours（小时）、days（天）、weeks(星期)。count是时间数量，几天，几小时。例如：now + 5 minutes
- 直接使用today（今天）、tomorrow（明天）来指定完成命令的时间



- 案例1
- 2天后的下午5点执行 /bin/ls /home 
- at 5pm + 2days  然后回车
- 输入要执行的指令 /bin/ls /home 然后两次ctrl+d 

    [cary@cary ~]$ at 5pm + 2 days
at> /bin/ls /home<EOT>
job 1 at Thu Apr  7 17:00:00 2022






- 案例2
- atq查看有没有执行的工作任务

    [cary@cary ~]$ atq
1  Thu Apr  7 17:00:00 2022 a cary






- 案例3
- 明天17点钟，输出时间到指定文件内，比如 /home/cary/test/date100.log

    [cary@cary ~]$ at 5pm tomorrow 
at> date > /home/cary/test/date100.log<EOT>
job 2 at Thu Apr  7 09:50:00 2022



- 案例4
- 2分钟后，将时间信息输入到 /home/cary/test/date200.log

    [cary@cary ~]$ at now + 2 minutes
at> date > /home/cary/test/date200.log<EOT>
job 4 at Tue Apr  5 16:55:00 2022






- 案例5
- atrm 编号 ，进行删除

    [cary@cary test]$ atq
2  Thu Apr  7 09:50:00 2022 a cary
1  Thu Apr  7 17:00:00 2022 a cary
[cary@cary test]$ atrm 1
[cary@cary test]$ atq
2  Thu Apr  7 09:50:00 2022 a cary






### linux分区



- 原理介绍
- linux来说无论几个分区，分给哪一目录使用，它归根结底就只有一个目录，一个独立且唯一的文件结构，linux中每个分区都是用来组成整个文件系统的一部分
- linux采用了一种叫“载入”的处理方法，它的整个文件系统中包含了一整套的文件和目录，且将一个分区和一个目录联系起来。这时要载入一个分区将使它的存储空间在一个目录下获得
- 硬盘说明、
- linux硬盘分IDE硬盘和SCSI硬盘，目前基本上是SCSI硬盘
- 对于 IDE硬盘，驱动器标识符为 hdx~，其中hd表明分区所在设备的类型，这里是指IDE硬盘了。x为盘号（a为基本盘，b为基本从属盘，c为辅助主盘，d为辅助从属盘），~代表分区，前四个分区用数字1到4表示，他们是主分区或扩展分区，从5开始就是逻辑分区，例，hda3表示为第一个IDE硬盘上的第三个主分区或扩展分区，hdb2表示为第二个ide硬盘上的第二个主分区或扩展分区。
- 对于 SCSI 硬盘则标识为 sdx~ ，SCSI 硬盘是用 sd 来表示分区所在设备的类型的，其余则和IDE硬盘的表示方法一样。
- 
- 查看所有设备挂载情况
- 命令：lsblk 或者 lsblk -f
- 加 -f 信息更加详细





- 挂载的经典案例
- 说明，以增加一块硬盘来熟悉磁盘的相关指令和深入理解磁盘分区、挂载、卸载的概念
- 如何增加一块硬盘
- 1.虚拟机添加硬盘
- 2.分区
- 3.格式化
- 4.挂载
- 5.设置可以自动挂载
- 
- 分区
- 分区命令 fdisk /dev/sdb
- 开始对 /sdb 分区
- m 显示命令列表
- p 显示磁盘分区 同 fdisk -l
- n新增分区
- d 删除分区
- w 写入并退出
- 说明：开始分区后输入n，新增分区，然后选择p，分区类型为主分区。两次回车默认剩余全部空间。最后输入w写入分区并退出，若不保存退出输入q。

    [cary@cary ~]$ sudo fdisk /dev/sdb 

    欢迎使用 fdisk (util-linux 2.23.2)。

    更改将停留在内存中，直到您决定将更改写入磁盘。

    使用写入命令前请三思。

    Device does not contain a recognized partition table

    使用磁盘标识符 0x66939792 创建新的 DOS 磁盘标签。

    命令(输入 m 获取帮助)：m

    命令操作

      a   toggle a bootable flag

      b   edit bsd disklabel

      c   toggle the dos compatibility flag

      d   delete a partition

      g   create a new empty GPT partition table

      G   create an IRIX (SGI) partition table

      l   list known partition types

      m   print this menu

      n   add a new partition

      o   create a new empty DOS partition table

      p   print the partition table

      q   quit without saving changes

      s   create a new empty Sun disklabel

      t   change a partition's system id

      u   change display/entry units

      v   verify the partition table

      w   write table to disk and exit

      x   extra functionality (experts only)

    命令(输入 m 获取帮助)：n

    Partition type:

      p   primary (0 primary, 0 extended, 4 free)

      e   extended

    Select (default p): p

    分区号 (1-4，默认 1)：1

    起始 扇区 (2048-2097151，默认为 2048)：

    将使用默认值 2048

    Last 扇区, +扇区 or +size{K,M,G} (2048-2097151，默认为 2097151)：

    将使用默认值 2097151

    分区 1 已设置为 Linux 类型，大小设为 1023 MiB

    命令(输入 m 获取帮助)：w

    The partition table has been altered!

    Calling ioctl() to re-read partition table.

    正在同步磁盘。



- 给分区格式化
- 分区命令 mkfs -t ext4 /dev/sdb1 其中，ext4是分区类型
- 格式化后才能挂载
- 
- 挂载
- mount 设备名称 挂载目录
- 注意：使用命令行挂载重启后会失效
- 例如：mount /dev/sdb1 /home/cary/testDisk
- 卸载：umount 设备名称 挂载目录
- 例如：umount /dev/sdb1 /home/cary/testDisk 或 umount /home/cary/testDisk 





- 永久挂载
- 修改 /etc/fstab 实现挂载，这个文件也存放着其他目录的挂载到硬盘的语句
- 添加一行
- /dev/sdb1          /home/cary/testDisk          ext4      defaults         0 0
- 添加完成后 执行 mount -a 即刻生效



### 磁盘情况查询

- 查询系统整体磁盘使用情况
- 基本语法
- df -h
- 应用实例
- 查询系统整体磁盘使用情况  df -h
- 
- 查询指定目录的磁盘占用情况
- du -h /目录
- 默认为当前目录
- -s 指定目录占用大小汇总
- -h 带计量单位
- -a 含文件
- —max-depth=1 子目录深度
- -c 列出明细的同时，增加汇总值
- 
- 应用实例
- 查询 /opt 目录的磁盘占用情况，深度为1
- du -h --max-depth=1 /opt
- 
- 同时查询带文件
- du -ha --max-depth=1 /opt
- 
- 同时汇总
- du -hac --max-depth=1 /opt
- 
- 



### 磁盘情况 - 工作实用指令



- 统计/opt文件夹下文件的个数
- ls -l /opt | grep "^-" 这个是只筛选出以-开头的，是个正则表达式，这样就是普通文件了
- ls -l /opt | grep "^-" | wc -l 这个wc是统计数据的
- 
- 统计/opt文件夹下目录的个数
- ls -l /opt | grep "^d" 这个是只筛选出以d开头的，是个正则表达式，这样就是文件夹了
- ls -l /opt | grep "^d" | wc -l 这个wc是统计数据的
- 
- 统计/opt文件夹下文件的个数，包括子文件夹里面的
- ls -lR /opt | grep "^-" | wc -l    必须是大写的R
- 
- 统计/opt文件夹下目录的个数，包括子文件夹里面的
- ls -lR /opt | grep "^d" | wc -l
- 
- 以树状显示目录结构
- 先安装 sudo yum install tree.x86_64
- tree /home/





### linux网络环境配置



- 修改配置文件来指定ip，并可以连接到外网（程序员推荐）
- 编辑 vi /etc/sysconfig/network-scripts/ifcfg-ens33
- 这个ens33 文件就是网卡文件
- 文件说明
- 这里面没有mac地址

    TYPE="Ethernet"        # 网络类型（通常是 Ethernet）
PROXY_METHOD="none"
BROWSER_ONLY="no"

    # ip配置方法[none|static|bootp|dhcp](引导时不适用协议|静态分配ip|bootp协议|dhcp协议)
BOOTPROTO="dhcp"      
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="yes"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_FAILURE_FATAL="no"
IPV6_ADDR_GEN_MODE="stable-privacy"
NAME="ens33" 
UUID="6ef4a453-33d7-4a96-8034-57a408342d02"      # 随机id
DEVICE="ens33"         # 接口名（设备，网卡）
ONBOOT="yes"           # 系统启动的时候网络接口是否有效（yes/no）

    

    // 下边是加的

    // 这个是mac地址，但是配置文件中居然没有

    HWADDR = 00:11:22:33:44:55

    //  ip地址，静态分配时手动指定 不能有空格，完全按照此格式写

    IPADDR=192.168.73.130

    // 网关，静态分配时手动指定

    GATEWAY=192.168.73.2

    // 域名解析器，静态分配时手动指定

    DNS1=192.168.73.2



- 重启网络服务
- service network restart



### 设置主机名和hosts映射

- 设置主机名
- 为了方便记忆，可以给linux系统设置主机名，也可以根据需要修改主机名
- 指令hostname：查看主机名
- 修改文件在 /etc/hostname 指定
- 修改后，重启生效
- 
- 设置hosts映射
- 思考：如何通过 主机名能够找到（比如ping）某个linux系统？
- windows
- 在 c:\Windows\System32\drivers\etc\hosts 文件指定即可
- 案例：192.168.73.130 hspedu1000
- linux
- 在 /etc/hosts 文件指定
- 案例：192.168.73.130 Cary
- 
- 主机名解析过程分析（Hosts、DNS）
- Hosts是什么
- 一个文本文件，用来记录ip和Hostname（主机名）的映射关系
- DNS
- dns就是 Domain Name System的缩写，翻译过来就是域名系统
- 是互联网上作为域名和ip地址相互映射的一个分布式数据库





### 进程基本介绍



- 在linux中，每个执行的程序都称为一个进程，每一个进程都分配一个id号，pid，进程号
- 每个进程都可以以两种方式存在的。前台与后台，所谓前台进程就是用户目前的屏幕上可以进行操作的。后台进程则是实际在操作，但由于屏幕上无法看到的进程，通常使用后台方式执行。
- 一般系统的服务都是以后台进程的方式存在，而且都会常驻在系统中，直到关机才结束。





### 显示系统执行的进程



- 基本介绍
- ps命令是用来查看目前系统中，有哪些正在执行，以及他们执行的状况，可以不加任何参数
- 
- 字段
- pid    进程识别号
- tty   终端机号
- time    此进程所占用cpu时间
- cmd    正在执行的命令或进程名
- 
- 选项
- -a 显示当前终端的所有进程信息
- -u 以用户的格式显示进程信息
- -x 显示后台进程运行的参数





### 显示系统执行的进程



- 应用实例
- 要求：以全格式显示当前所有的进程，查看进程的父进程。
- ps -ef 是以全格式显示当前所有的进程
- -e 显示所有进程， -f 全格式
- ps -ef | grep xxx
- 是BSD风格，啥意思？
- UID：用户ID
- PID：进程ID
- PPID：父进程ID、
- C：CPU用于计算执行优先级的因子，数值越大，表明进程是CPU密集型运算，执行优先级会降低，数值越小，表明进程是I/O密集型运算，执行优先级会提高
- STIME：进程启动的时间
- TTY：完整的终端名称
- TIME：CPU时间
- CMD：启动进程所用的命令和参数



### 终止进程 kill 和 killall

- 介绍
- 若是某个进程执行一半需要停止，使用 kill命令来完成
- 基本语法
- kill 【选项】 进程号 （通过进程号杀死进程）
- killall 进程名称  （通过进程名称杀死进程，也支持通配符，这在系统因负载过大而变得很慢时有用）
- 
- 常用选项
- -9   表示强迫进程立即停止，防止有些进程屏蔽普通kill指令。
- 最佳实践
- 案例1：踢掉某个非法登录用户
- 通过 ps -aux | grep sshd 找到cary用户的登录进程，然后 kill 1797 杀死进程，1797就是进程号
- 案例2：终止远程登录服务sshd，在适当时候再次重启sshd服务
- kill 1062 这个1062就是远程登录服务sshd，然后xshell就无法登录了
- 重启sshd服务，/bin/systemctl start sshd.service
- 案例3：终止多个gedit
- killall gedit
- 案例4：强制杀掉一个终端
- keill -9 (bash的进程号)



### 查看进程树 pstree

- 基本语法
- pstree 【选项】    可以更直观的查看进程信息
- -p 显示进程号
- -u 显示用户名



### 服务（service）管理

- 介绍
- 服务（service）本质就是进程，但是是运行在后台的，通常都会监听某个端口，等待其他程序的请求，比如（mysqld，sshd 防火墙等），因此我们又成为守护进程，是linux中非常重要的只是
- service 管理指令
- service 服务名 【start | stop | restart | status】
- 在centos7后，很多服务不再使用 service，而是systemctl（后边专门讲）
- 只查看service 指令管理的服务，是在 /etc/init.d 查看
- ls -l /etc/init.d 
- service 管理指令案例
- 请使用service 指令，查看、关闭、启动 network 
- service network status
- service network stop
- service network start
- 
- 使用 setup 命令查看所有的服务名
- 输入 setup 回车，弹出菜单，选择 系统服务 回车
- 带星号的自启动
- 没有带型号的需要手动启动
- 星号位置，输入星号设置为自启动，输入空格取消自启动





### 服务管理



- 服务的运行级别（runlevel）
- linux系统有7种运行级别（runlevel）：常用的是级别3和5
- 0：系统停机状态，系统默认运行级别不能设为0，否则不能正常启动
- 1：单用户工作状态，root权限， 用于系统维护，禁止远程登录
- 2：多用户状态（没有NFS,相当于网络邻居），不支持网络
- 3：完全的多用户状态（有NFS），登陆后进入控制台命令模式
- 4：系统未使用，保留
- 5：X11控制台，登录后进入图形GUI模式
- 6：系统正常关闭并重启，默认运行级别不能设为6，否则不能正常启动
- 开机流程说明
- 开机→bios→/boot→systemd进程1→运行级别→运行级对应的服务
- 
- 设置运行级别
- /etc/initab
- 
- chkconfig指令
- 介绍
- 通过chkconfig命令可以设置某个服务在各个运行级别的 启动/关闭
- chkconfig指令管理的服务在/etc/init.d 查看
- chkconfig基本语法
- 查看服务 chkconfig —list 【|grep xxx】
- chkconfig 服务名 —list
- // 设置某个服务在运行级别5设置自启动或关闭
- chkconfig —level 5 服务名 on/off
- 重启才能生效
- 
- systemctl 管理指令
- 基本语法 systemctl 【start | stop | restart | status】 服务名
- systemctl 指令管理的服务在 /usr/lib/systemd/system 查看
- 
- systemctl 设置服务的自启动状态
- systemctl list-unit-files 【|grep 服务名】 查看服务开机启动状态
- systemctl enable 服务名 设置服务开机启动
- systemctl disable 服务名 关闭服务开机启动
- systemctl is-enabled 服务名 查询某个服务是否是自启动的
- 应用案例
- 查看当前防火墙的状况，关闭防火墙和重启防火墙
- 细节讨论
- 关闭或者启用防火墙后，立即生效，telnet 测试 某个端口即可
- 这种方式只是临时生效，当重启系统后，还是回归以前对服务的设置
- 如果希望设置某个服务自启动或关闭永久生效，要使用 systemctl 【enable|disable】 服务名



- 打开或者关闭指定端口
- 正真的生产环境，往往需要将防火墙打开，但问题来了，如果我们把防火墙打开，那么外部请求数据包就不能跟服务器监听端口通讯，这时，需要打开指定的端口。比如 80、22、8080，这个又怎么做呢？
- firewall指令、
- 协议怎么看？
- netstat -anp | more 然后可以看到111端口是tcp协议
- 打开端口：firewall-cmd —permanent —add-port=端口号/协议
- 关闭端口：firewall-cmd —permanent —remove-port=端口号/协议
- 重新载入才能生效，firewall-cmd —reload
- 查询端口是否开放：firewall-cmd —query-port=端口/协议
- 应用案例
- 启用防火墙，测试111端口是否能telnet（远程登录）
- telnet 192.168.73.130 111    连接不上
- 开放111端口
- firewall-cmd --permanent --add-port=111/tcp
- sudo firewall-cmd --reload
- 再次关闭111端口
- firewall-cmd --permanent --remove-port=111/tcp
- sudo firewall-cmd --reload





### 动态监控进程



- 介绍
- top与ps命令很相似。他们都是用来显示正在执行的进程，top与ps最大的不同之处，在于top在执行一段时间可以更新正在运行的进程。
- 基本语法
- top 【选项】
- -d 秒数，指定top命令每隔几秒更新。默认是3秒
- -i 使top不显示任何闲置或者僵死进程
- -p 通过指定监控进程id来仅仅监控某个进程的状态。
- 交互操作
- p 以cpu使用率排序，默认是此项
- M 以内存的使用率排序
- N 以pid排序
- q 退出top
- u 输入u，在输入用户名 回车 即可 
- k 输入k，在输入进程号 回车 
- 
- 



### 监控网络状态



- 查看系统网络情况netstat
- 基本语法 
- netstat 【选项】
- 选项说明
- -an 按一定顺序排序输出
- -p 显示哪个进程在调用
- 一般用  netstat -anp | more
- 应用案例
- 请查看服务名为sshd的服务信息
- 一般用  netstat -anp | grep sshd
- 检测主机连接命令ping
- 是一种网络检测工具，它主要是用于检测远程主机是否正常，或是两部主机间的网线或网卡故障





### rpm包管理



- 介绍
- rpm用于互联网下载包的打包格式及安装工具，它包含在某些linux分发版中，它生成具有rpm扩展名的文件。rmp是 RedHat Package Manager 红帽软件包管理 的缩写，类似 windows的setup.exe，这一文件格式名称虽然打上了RedHat的标志，但理念是通用的。
- linux的分发版都有采用 ，可以算是公认的行业标准了。
- rpm 包的简单查询指令
- 查询已安装的rpm列表 rpm -qa | grep xx
- rpm包名基本格式、
- 一个rpm包名：firefox-60.2.2-1.el7.centos.x86_64
- 名称：firefox
- 版本号：60.2.2-1
- 适用操作系统：el7.centos.x86_64   表示centos7的64位操作系统
- 如果是 i686、i386表示32位系统，noarch表明通用。 
- 
- rpm -qi 软件包名           查询软件包信息
- ql 软件包名                   查询软件包中的文件夹，安装在哪里了
- qf 文件全路径名             查询文件所属软件包
- 卸载rpm包
- 基本语法
- rpm -e 包名称
- 如果其他软件依赖于要卸载的软件包，卸载会发生错误
- 增加参数 —nodeps 可以强制删除软件包，但不建议这样做。
- 例如   rpm -e —nodeps foo
- 安装
- 基本语法
- rpm -ivh 包全路径名
- 参数
- i      install安装
- v      verbose 提示
- h     hash进度条
- 



### yum



- 介绍
- yum是一个shell前端软件包管理器。基于rpm包管理，能够从指定的服务器自动下载rpm包并且安装，可以自动处理依赖性关系，并且一次安装所有依赖的软件包。
- yum的基本指令
- 查询yun服务器是否有需要安装的软件
- yum list | grep xx     
- 安装指定的yum包
- yum install xxx
- 





### 配置JDK8



- 安装步骤
- mkdir /opt/jdk
- 将 ideaiu     tomcat    mysql    jdk8    的linux版本通过xftp6上传到/opt/jdk下
- 解压    tar -zxvf jdk8.tar.gz
- mkdir /usr/local/java
- mv /opt/jdk/jdk1.8.0_261 /usr/local/java
- 配置环境变量的配置文件
- vim /etc/profile
- export JAVA_HOME=/usr/local/java/jdk1.8.0_261
- export PATH=$JAVA_HOME/bin:$PATH
- source /etc/profile               这句是让文件生效
- 测试是否安装成功
- 编写一个Hello.java    正确执行就行





### tomcat的安装



- 解压到 /opt/tomcat
- 进入 bin 执行 ./startup.sh
- 开放端口8080，执行 
- sudo firewall-cmd --permanent --add-port=8080/tcp
- sudo firewall-cmd --reload
- 测试是否安装成功
- 在windows或linux 的浏览器中访问下边连接
- 192.168.73.130:8080



### idea安装

- 解压到 /usr/local/idea 
- 进入文件夹中找到 bin/idea.sh 去执行就行了





### mysql5.7安装（最稳定）



- 安装的步骤和文档
- 
    1. 新建文件夹/opt/mysql，并cd进去
    2. 运行wget [http://dev.mysql.com/get/mysql-5.7.26-1.el7.x86_64.rpm-bundle.tar，下载mysql安装包](http://dev.mysql.com/get/mysql-5.7.26-1.el7.x86_64.rpm-bundle.tar，下载mysql安装包)

    PS：centos7.6自带的类mysql数据库是mariadb，会跟mysql冲突，要先删除。

    1. 运行tar -xvf mysql-5.7.26-1.el7.x86_64.rpm-bundle.tar
    2. 运行rpm -qa|grep mari，查询mariadb相关安装包
    3. 运行rpm -e --nodeps mariadb-libs，卸载
    4. 然后开始真正安装mysql，依次运行以下几条

    rpm -ivh mysql-community-common-5.7.26-1.el7.x86_64.rpm

    rpm -ivh mysql-community-libs-5.7.26-1.el7.x86_64.rpm

    rpm -ivh mysql-community-client-5.7.26-1.el7.x86_64.rpm

    rpm -ivh mysql-community-server-5.7.26-1.el7.x86_64.rpm

    1. 运行systemctl start mysqld.service，启动mysql
    2. 然后开始设置root用户密码

    Mysql自动给root用户设置随机密码，运行grep "password" /var/log/mysqld.log可看到当前密码

      [cary@Cary mysql]$ sudo grep "password" /var/log/mysqld.log 
2022-04-09T03:37:47.818166Z 1 [Note] A temporary password is generated for root@localhost: s-lcNZa5=/wv

      标红的字体位置就是密码

    1. 运行mysql -u root -p，用root用户登录，提示输入密码可用上述的，可以成功登陆进入mysql命令行
    2. 设置root密码，对于个人开发环境，如果要设比较简单的密码（**生产环境服务器要设复杂密码**），可以运行
    3. 先设置密码策略，默认是1，这里设置为0，就是要求最低，可以设置简单密码

        set global validate_password_policy=0;
    1. set password for 'root'@'localhost' =password('qq123456');

        这个密码也要加字母
    1. 运行flush privileges;使密码设置生效



### linux之大数据定制篇 

### shell编程

- 为什么要学习shell编程
- linux运维工程师在进行服务器集群管理时，需要编写shell程序来进行服务器管理
- 对于javaee 和 python程序员来说，工作的需要，你的老大要求你编写一些shell脚本进行程序或者是服务器的维护，比如编写一个定时备份数据库的脚本
- 对于大数据程序员来说，需要编写shell程序来管理集群
- shell是什么
- shell是一个命令行解释器，它为用户提供了一个向linux内核发送请求以便运行程序的界面系统级程序，用户可以用shell来启动、挂起、停止甚至是编写一些程序。





### shell脚本的执行方式



- 脚本格式要求
- 脚本以 #!/bin/bash 开头
- 脚本需要有可执行权限
- 编写第一个shell脚本
- 需求说明：创建一个shell脚本，输出hello world
- 脚本的常用执行方式
- 方式一：输入脚本的绝对路径或相对路径
- 方式二：sh 脚本                      （这种不用赋予执行权限）
- 

### shell 的变量



- shell变量的介绍
- linux shell中的变量分为 系统变量 和 用户自定义变量
- 系统变量
- $HOME、$PWD、$SHELL、$USER等等，
- 用法，比如 echo $HOME 等等
- 显示当前shell中所有的变量：set
- 
- shell变量的定义
- 基本语法
- 定义变量：变量名=值         注意：不能有空格，严格按照此格式来
- 撤销变量：unset 变量
- 声明静态变量：readonly变量，注意：不能unset 
- 
- 快速入门
- 案例1：定义变量A
- 案例2：撤销变量A
- 案例3：声明静态的变量B=2，不能unset
- readonly B=2
- 案例4：可把变量提升为全局环境变量，可供其他shell程序使用
- 
- shell变量的定义
- 定义变量的规则
- 变量名称可以由字母、数字和下划线组成，但是不能以数字开头。
- 等号两侧不能有空格
- 变量名称一般习惯为大写
- 
- 将命令的返回值赋给变量
- A=`date`      就是把 date 命令的结果，给 A 变量
- A=$(date)    也可以这样写





### 设置环境变量



- 基本语法
- export 变量名=变量值                 （将shell变量输出为环境变量/全局变量）
- source 配置文件                           （让修改后的配置文件立即生效）
- echo $变量名                                （查询环境变量的值）
- 
- 快速入门
- 在 /etc/profile 文件中定义 TOMCAT_HOME 环境变量
- 查看环境变量 TOMCAT_HOME 的值
- 在另外一个shell程序中使用 TOMCAT_HOME
- 注意：在输出 TOMCAT_HOME 环境变量前，需要让其生效
- source /etc/profile



- 多行注释
- :<<!
- 注释的内容
- !
- 其中   :<<!  和 ! 一定独自占一行





### 位置参数变量



- 介绍
- 当我们执行一个shell脚本时，如果向脚本中传入参数。
- 比如：./myshell.sh 100 200
- 100 和 200 就是向 myshell 中传入的参数值
- 
- 基本语法
- $n     
- n为数字，$0代表命令本身，$1-9代表第一到第九个参数，十以上的参数需要用大括号包含，如${10}
- $*
- 代表命令行中所有的参数，$*把所有的参数看成一个整体
- $@  
- 这个也是代表命令行中所有的参数，不过$@把每个参数区分对待
- $#
- 这个变量代表命令行中所有参数的个数
- 
- 位置参数变量
- 案例：编写一个shell脚本position.sh 在脚本中获取到命令行的各个参数信息
- 说白了，这个位置参数变量，就相当于c语言 main函数中的参数
- 



### 预定义变量



- 基本介绍
- 就是shell设计者事先已经定义好的变量，可以直接在shell脚本中使用
- 基本语法
- $$
- 当前进程的进程号
- $!
- 后台运行的最后一个进程的进程号
- /home/cary/test/shtestsh01.sh &          
- 后边加个地址符& 就说明以后台的方式运行这个脚本
- $?
- 最后一次执行的命令的返回状态。如果这个变量的值为0，证明上一个命令正确执行，如果这个变量的值为非0（具体是哪个数，由命令自己来决定），则证明上一个命令不正确了。
- 案例：在一个shell脚本中简单使用一下预定义变量



### 运算符

- 基本介绍
- 学习如何在shell中进行各种运算操作
- 基本语法
- "$((运算式))"   或   "$[运算式]"      或者     expr   m + n 
- 注意 expr 运算符间要有空格，如果想将运算式结果赋给某个变量，那么表达式要用反引号引起来 ``
- expr m - n
- expr \*,/,%    乘，除，取余
- 应用实例
- 案例1：计算(2+3)*4 的值
- echo $[(2 + 3)*4]           说明这种方式加不加空格都可以
- 案例2：请求出命令行的两个参数[整数]的和
- expr $1 + $2





## 条件判断



- 判断语句
- 基本语法
- [ 这里写条件 ]           注意条件与中括号之间是有空格的
- 非空返回 true   可使用$? 验证，0为true，非0为false
- 应用实例
- [ hspEdu ]       返回true
- [  ]                   返回false
- [ condition ] && echo OK || echo notok                条件满足，执行后边的语句
- 
- 判断语句
- 常用判断条件
- = 字符串比较
- 两个整数比较
- -lt      小于
- -le      小于等于
- -eq     等于
- -gt       大于
- -ge      大于等于
- -ne      不等于
- 按照文件权限进行判断
- -r       有读的权限
- -w      有写的权限
- -x        有执行的权限、
- 按照文件类型进行判断
- -f      文件存在并且是一个常规文件
- -e     文件存在
- -d     文件存在并且是一个目录
- 应用实例
- 案例1："ok"是否等于"ok"

    if [ "ok" = "ok" ]

    then

      echo "yes ==="

    else

      echo "not ==="

    fi
- 案例2：23是否大于22

    if [ 23 -ge 22 ]

    then

      echo "23>22,yes"

    else

      echo "23>22,no"

    fi
- 案例3：/home/cary/test/shtest01.sh  此文件是否存在

    if [ -f /home/cary/test/shtest01.sh ]

    then

      echo "[shtest01.sh](http://shtest01.sh) yes"

    else

      echo "[shtest01.sh](http://shtest01.sh) no"

    fi





### 流程控制



- if [ 条件判断 ]
- then
- 代码
- elif [ 条件判断 ]
- then
- 代码
- else
- 代码
- fi
- 
- case语句
- case $变量名 in
- "值1")
- 如果变量的值等于值1，则执行程序1
- ;;
- "值2")
- 如果变量的值等于值2，则执行程序2
- ;;
- *)
- 如果变量的值都不是以上的值，则执行此程序
- ;;
- esac
- 案例1：当命令参数是1时，输出周一，2时，输出周二，其它输出other
- 
- for循环
- 基本语法1
- for 变量 in 值1 值2 值3 ...
- do
- 程序
- done
- 案例1：打印命令行输入的参数，这里可以看出$* 和 $@ 的区别

    for A in $*

    do

      A1=$[A1+A]

      echo "A=$A"

    done

    echo "A_SUM=$A1"

    for B in "$@"

    do

      B1=$[B1+B]

      echo "B=$B"

    done

    echo "B_SUM=$B1"
- 基本语法2
- for (( 初始值;循环控制条件;变量变化))
- do
- 程序
- done
- 案例2：从1加到100的值输出显示

    for (( B=1;B < 101;B=$[B+1] ))

    do

      C=$[C+B]

      echo "C=$C"

    done
- 
- while循环
- 基本语法1
- while [ 条件判断式 ]
- do
- 程序
- done
- 案例1：从命令行输入一个数n，统计从1+..+n的值是多少

    A=1

    B=0

    while [ $A -le $1 ]

    do

      B=$[B+A]

      A=$[A+1]

    done

    echo "B=$B"





### read 读取控制台输入



- 基本语法
- read 选项 参数
- 选项
- -p 指定读取值时的提示符
- -t 指定读取值时等待的时间秒，如果没有在指定的时间内输入，就不去等待了
- 参数
- 变量：指定读取值的变量名
- 案例1：读取控制台输入一个num值
  
  read -p "pless input one num:" NUM1

  echo "-->$NUM1" 

- 案例2：读取控制台输入一个num值，在10秒内输入

    read -t 10 -p "pless 10s input one num:" NUM2

    echo "-->$NUM2"




