
# 搭建FTP服务器

## 1. 安装
    
    // 安装vsftpd
    sudo apt-get install vsftpd
    
    // 查看版本
    vsftpd -v  

## 2. 新建一个ftp目录

    // 在用户目录中新建ftp目录
    mkdir ftp
    
    // 在ftp目录中新建uploadFile目录
    mkdir uploadFile
    
    // 修改目录权限
    chmod 777 -R uploadFile

### 3. 修改 vsftpd.conf 文件

    sudo gedit /etc/vsftpd.conf

    // 在打开的文件后边追加，具体啥意思，能猜个差不多
    // 有网址解释：https://www.cnblogs.com/YangJieCheng/p/8252577.html
    anonymous_enable=YES
    anon_root=/home/cary/ftp
    no_anon_password=YES
    write_enable=YES
    anon_upload_enable=YES
    anon_mkdir_write_enable=YES

    // 重新加载 vsftpd 配置文件
    sudo /etc/init.d/vsftpd restart

### 4. linux测试

    ftp 127.0.0.1
    
    // 输入账号密码
    
    // 出现 ftp> 就说明成功了

### 5. ubuntu 与 windows 交互

    // 先查看ubuntu的ip地址，要先安装net_tools才能使用此命令
    ifconfig

    // 复制ip地址，在windows中打开cmd，执行命令
    ftp 192.168.179.130

    // 输入账号密码

    // 将当前文件夹中的某文件上传到ftp服务器
    put test.txt

    // 然后去ubuntu的 uploadFile 目录中查看是否有上传的文件

    // 然后在ubunut中的随便一个目录下执行
    ftp 127.0.0.1
    
    // 输入用户名和密码
    
    // 然后输入获取刚才上传的文件
    get test.txt
    
    // 也就是说，ubuntu又是服务器又是客户端，就算windows上传到ubuntu所在的服务器中了
    // 如果ubuntu想用此文件，还需要get获取才行

    // 退出ftp
    exit

### 6. 重启或开启ftp服务

    // 后续如果windows连接不上ftp服务了，说明ftp服务没开，要重启一下就好了
    sudo service vsftpd restart


# 查看 ubuntu 系统版本号

    cat /etc/issue

# ubuntu 20.04 安装搜狗输入法

1. 安装 Fcitx 输入框架
   
    // 相关的依赖库和框架都会自动安装上
    sudo apt install fcitx-bin
    sudo apt-get install fcitx-table

2. 下载 ubuntu版本搜狗输入法
   
3. 进入相应的下载目录进行安装
   
    sudo dpkg -i sougoupinyin.dep

4. 遇到问题或提示

    // 如果安装错误，运行此命令删除安装，重新开始
    sudo apt -fix-broken install

    // 遇到 dpkg: error processing package sogoupinyin (–install): 
    // dependency problems – leaving unconfigured
    // 输入
    sudo apt-get install -f

    // 中间有提示 输入 Y

    // 出现安装错误可以使用此命令，来检查修复依赖
    sudo apt-get install -f

5. 安装完成后重启系统

6. 进行相关设置 
   
    settings->Region&language->Manage Installed Languages
    // 然后在弹出来的对话框中 选择输入框架为 fcitx
    // 然后点击 Apply System-Wide 应用到全局

7. 重启

    // 重启后在右上角有个键盘标志，点击进入选择
    // Configure Current Input Method
    // 进入Input Method 界面后，选择+号
    // 进入到 Add input method 界面，将下面的 Only Show Current Language 点掉后
    // 在搜索栏搜索搜狗拼音，选中之后进行添加
    // 搜狗里面设置 shift 切换中英文

    // 到这里就完了，下面是卸载了

8. 根据需要，卸载ibus
   
    sudo apt-get remove ibus

    // 清除ibus配置
    sudo apt-get purge ibus

9. 根据需要，卸载顶部面板任务栏上的键盘指示

    sudo apt-get remove indicator-keyboard

10. 搜索搜狗，进行卸载
    
    // 搜索
    sudo dpkg -l so*

    // 卸载
    sudo apt-get purge sogoupinyin