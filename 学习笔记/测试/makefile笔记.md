
# Makefile笔记

## GCC常用选项

- 懒得写，直接截图
  - ![""](img/01.jpg)
  - ![](img/02.jpg)

## Linux下GCC制作静态库

    首先将所有源文件都编译成目标文件：
    gcc -c *.c

    *.c表示所有以.c结尾的文件，也即所有的源文件。执行完该命令，会发现 test 目录中多了三个目标文件，分别是 add.o、sub.o 和 div.o。

    然后把所有目标文件打包成静态库文件：
    ar rcs libtest.a *.o

    *.o表示所有以.o结尾的文件，也即所有的目标文件。执行完该命令，发现 test 目录中多了一个静态库文件 libtest.a，大功告成。

    下面是完整的生成命令：

    [c.biancheng.net ~]$ cd test

    [c.biancheng.net test]$ gcc -c *.c

    [c.biancheng.net test]$ ar rcs libtest.a *.o

## Linux下使用静态库

    此时 math 目录中文件结构如下所示：
    |-- include
    |   `-- test.h
    |-- lib
    |   `-- libtest.a
    `-- src
        `-- main.c

    在 main.c 中，可以像下面这样使用 libtest.a 中的函数：
    #include <stdio.h>
    #include "test.h"  //必须引入头文件
    int main(void)
    {
        int m, n;
        printf("Input two numbers: ");
        scanf("%d %d", &m, &n);
        printf("%d+%d=%d\n", m, n, add(m, n));
        printf("%d-%d=%d\n", m, n, sub(m, n));
        printf("%d÷%d=%d\n", m, n, div(m, n));
        return 0;
    }
    在编译 main.c 的时候，我们需要使用-I（大写的字母i）选项指明头文件的包含路径，使用-L选项指明静态库的包含路径，使用-l（小写字母L）选项指明静态库的名字。所以，main.c 的完整编译命令为：
    gcc src/main.c -I include/ -L lib/ -l test -o math.out

    注意，使用-l选项指明静态库的名字时，既不需要lib前缀，也不需要.a后缀，只能写 test，GCC 会自动加上前缀和后缀。

    打开 math 目录，发现多了一个 math.out 可执行文件，使用./math.out命令就可以运行 math.out 进行数学计算。

    完整的使用命令如下所示：
    [c.biancheng.net ~]$ cd math
    [c.biancheng.net math]$ gcc src/main.c -I include/ -L lib/ -l test -o math.out
    [c.biancheng.net math]$ ./math.out
    Input two numbers: 27 9↙
    27+9=36
    27-9=18
    27÷9=3

## 常用规则
- 在命令最前面加上@：不显示命令执行
- 在命令最前面加上-：失败继续执行
- @和-可以写一起，组合命令
- 伪目标 .PHONY: clean cleanall